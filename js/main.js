$(document).ready(function(e) {
//    $(".city-dropdown").selectbox({
//        onChange: function (val, inst) {
//            window.location.href = 'site/index?id=' + val;
//        },
//        effect: "slide"
//    })

    $("#personal-link").click(function(event){
        event.stopPropagation();
        var menu = $("ul.personal-menu");
        if (menu.is(":visible"))
            menu.slideUp();
        else
            menu.slideDown();
    })

    $("html").click(function(){
        var menu = $("ul.personal-menu");
        if (menu.is(":visible"))
            menu.slideUp();
    })

    $("#invitebox form").submit(function(){
        var url = $(this).attr("action");
        var param = $("input#invitee").val();
        $.ajax({
            url: url,
            type: "post",
            data: {invitee: param},
            success: function(response) {
                $("#invitebox .newsletter-dialog-content").html(response.message);
            }
        })
        return false;
    })

    $("#newsletterbox form").submit(function(){
        var url = $(this).attr("action");
        var param = $("input#email").val();
        $.ajax({
            url: url,
            type: "post",
            data: {email: param},
            success: function(response) {
                $("#newsletterbox  .newsletter-dialog-content").html(response.message);
            }
        })
        return false;
    })

    $(".placeholder-nl form").submit(function(){
        var url = $(this).attr("action");
        var param = $("input#email").val();
        $.ajax({
            url: url,
            type: "post",
            data: {email: param},
            success: function(data) {
                if (data.status == true)
                    $(".placeholder-nl").html(data.message);
                else
                    $(".nl-error").html(data.message);
            }
        })
        return false;
    })

});