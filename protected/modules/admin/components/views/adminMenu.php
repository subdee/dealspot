<?php foreach ($this->items as $item) : ?>

<div class="menu-item">
    <div class="menu-header">
        <span><?php echo CHtml::link($item['label'], $item['url']); ?></span>
    </div>
    <div class="menu-choices">
        <?php foreach ($item['items'] as $subItem) : ?>
            <?php if ($subItem['visible']) : ?>
                <div class="menu-subitem">
                    <span><?php echo CHtml::link($subItem['label'], $subItem['url']); ?></span>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>

<?php endforeach; ?>