<?php

/**
 * Specificaitons:
 *
 * Must support:
 * label (optional [value callback]) default=none
 * url (optional) default=none
 * realUrl (optional) default=_url(this->url)
 * image (optional) default=none
 * visible (optional condition [boolean|expression]) default=true
 * enabled (optional condition [boolean|expression]) default=true
 * active (optional condition [boolean|expression]) NOTE: maybe this must be automatic. default=false. if active then force visible true
 * showItems (optional condition [boolean|expression]) default=true
 * subitems
 */
class AdminMenu extends CWidget {

    /**
     * @var array The menu items to actually render.
     */
    private $_menu;

    /**
     * @var int The level of the menu to show.
     */
    public $showLevel = -1;

    /**
     * @var string
     */
    public $cssClassActive = 'selected';

    /**
     * @var string
     */
    public $cssClassDisabled = 'disabled';

    /**
     * @var string
     */
    public $cssClassLiExpression = null;

    /**
     * @var string
     */
    public $cssClassUl = 'menu';

    /**
     *
     */
    public function init() {
        $this->_menu = array(
            array(
                'label' => _t('admin', 'dashboard'),
                'url' => _aurl('main/index'),
//                'visible' => Utils::isLoggedIn() && _user()->hasRole(Role::MANAGER | Role::RESET),
                'items' => array(
            )),
            array(
                'label' => _t('admin', 'users'),
                'url' => _aurl('users/index'),
                'items' => array(
                    array(
                        'label' => _t('admin', 'all users'),
                        'url' => _aurl('users/index'),
                    ),
                    array(
                        'label' => _t('admin', 'all newsletter users'),
                        'url' => _aurl('users/newsletters'),
                    ),
                    array(
                        'label' => _t('admin', 'add user'),
                        'url' => _aurl('users/add'),
                    ),
                    array(
                        'label' => _t('admin', 'add newsletter user'),
                        'url' => _aurl('users/addNewsletter'),
                    ),
                    array(
                        'label' => _t('admin', 'adjust user points'),
                        'url' => _aurl('users/adjustPoints'),
                    ),
            )),
            array(
                'label' => _t('admin', 'transactions'),
                'url' => _aurl('transactions/index'),
                'items' => array(
                    array(
                        'label' => _t('admin', 'completed transactions'),
                        'url' => _aurl('transactions/index'),
                    ),
                    array(
                        'label' => _t('admin', 'pending transactions'),
                        'url' => _aurl('transactions/pending'),
                    ),
            )),
            array(
                'label' => _t('admin', 'coupons'),
                'url' => _aurl('coupons/index'),
                'items' => array(
                    array(
                        'label' => _t('admin', 'all coupons'),
                        'url' => _aurl('coupons/index'),
                    ),
                    array(
                        'label' => _t('admin', 'new coupon'),
                        'url' => _aurl('coupons/add'),
                    ),
                    array(
                        'label' => _t('admin', 'send test coupon'),
                        'url' => _aurl('#'),
                        'active' => false,
                    ),
            )),
            array(
                'label' => _t('admin', 'deals'),
                'url' => _aurl('deals/index'),
                'items' => array(
                    array(
                        'label' => _t('admin', 'all deals'),
                        'url' => _aurl('deals/index'),
                    ),
                    array(
                        'label' => _t('admin', 'add deals'),
                        'url' => _aurl('deals/add'),
                    ),
                    array(
                        'label' => _t('admin', 'send test newsletter'),
                        'url' => _aurl('#'),
                        'active' => false,
                    ),
            )),
            array(
                'label' => _t('admin', 'categories'),
                'url' => _aurl('categories/index'),
                'items' => array(
                    array(
                        'label' => _t('admin', 'all categories'),
                        'url' => _aurl('categories/index'),
                    ),
                    array(
                        'label' => _t('admin', 'add category'),
                        'url' => _aurl('categories/add'),
                    ),
                    array(
                        'label' => _t('admin', 'all subcategories'),
                        'url' => _aurl('categories/sub'),
                    ),
                    array(
                        'label' => _t('admin', 'add subcategory'),
                        'url' => _aurl('categories/subAdd'),
                    ),
            )),
            array(
                'label' => _t('admin', 'companies'),
                'url' => _aurl('companies/index'),
                'items' => array(
                    array(
                        'label' => _t('admin', 'all companies'),
                        'url' => _aurl('companies/index'),
                    ),
                    array(
                        'label' => _t('admin', 'add company'),
                        'url' => _aurl('companies/add'),
                    ),
            )),
            array(
                'label' => _t('admin', 'cities'),
                'url' => _aurl('cities/index'),
                'items' => array(
                    array(
                        'label' => _t('admin', 'all cities'),
                        'url' => _aurl('cities/index'),
                    ),
                    array(
                        'label' => _t('admin', 'add city'),
                        'url' => _aurl('cities/add'),
                    ),
            )),
            array(
                'label' => _t('admin', 'settings'),
                'url' => _aurl('settings/index'),
                'items' => array(
                    array(
                        'label' => _t('admin', 'logout'),
                        'url' => _aurl('main/logout'),
                    ),
            )),
        );

        $this->_activateActions($this->_menu, _app()->controller->route);
    }

    /**
     *
     */
    public function run() {
        $this->render('adminMenu');
    }

    /**
     * @return array The menu items.
     */
    public function getItems() {
        return $this->_menu;
    }

    /**
     * @param array $item The menu item to push at the end of the list.
     */
    public function pushItem($item) {
        $this->_menu[] = $item;
    }

    /**
     *
     * @param type $items
     * @param type $level
     * @param type $current
     */
    private function _getLevel($items, $level, $current = 0) {

        if ($level == $current)
            return $items;

        // For level greater than 1, we must render the submenu of the active parent item.
        foreach ($items as $item) {

            // Is item active?
            if (!$this->is($item, 'active'))
                continue;

            // Has item children?
            if (!isset($item['items']))
                continue;

            return $this->_getLevel($item['items'], $level, $current + 1);
        }

        return null;
    }

    /**
     * Sets as active items that either have at least one active child or their url matches exactly.
     * @param string $route
     * @param int $level
     * @return boolean True if at least one action was activated
     */
    private function _activateActions(&$items, $route, $level = 0) {

        $foundActiveAction = false;
        foreach ($items as &$item) {

            // Default enabled
            if (!isset($item['enabled']))
                $item['enabled'] = true;
            else {
                if (is_string($item['enabled']))
                    $item['enabled'] = eval("return {$item['enabled']};");
            }

            // Default show children
            if (!isset($item['showItems']))
                $item['showItems'] = true;
            else {
                if (is_string($item['showItems']))
                    $item['showItems'] = eval("return {$item['showItems']};");
            }

            // Default active
            if (!isset($item['active']))
                $item['active'] = false;
            else {
                if (is_string($item['active']))
                    $item['active'] = eval("return {$item['active']};");

                if (!isset($item['visible'])) {
                    $item['visible'] = $item['active'];
                }
            }

            // Set visible
            if (!isset($item['visible']))
                $item['visible'] = true;
            else {
                if (is_string($item['visible']))
                    $item['visible'] = eval("return {$item['visible']};");
            }

            // Set item class
            $item['cssClassLi'] = '';
            if ($this->cssClassLiExpression !== null)
                $item['cssClassLi'] = eval("return {$this->cssClassLiExpression};");

            if (!$item['active']) {

                if (!$item['enabled']) {
                    $item['cssClassLi'] .= ' ' . $this->cssClassDisabled;
                    continue;
                }

                // If item has not url, cannot be active.
                if (!isset($item['url']))
                    continue;

                // Visit item's children.
                $foundActiveChild = false;
                if (isset($item['items'])) {
                    $foundActiveChild = $this->_activateActions($item['items'], $route, $level + 1);
                }

                // Activate
                if ($foundActiveChild) {
                    $item['active'] = true;
                } else {
                    if ($item['url'] == $route) {
                        $item['active'] = true;
                        $foundActiveAction = true;
                    }
                }

                // Generate real url for anchor
                $item['realUrl'] = _url($item['url']);
            } else {
                $foundActiveAction = true;
            }

            if ($item['active']) {
                $item['cssClassLi'] .= ' ' . $this->cssClassActive;
            }
        }

        return $foundActiveAction;
    }

    /**
     *
     * @param array $item
     * @param string $attribute
     * @return boolean
     */
    public function is($item, $attribute) {
        return isset($item[$attribute]) && $item[$attribute] === true;
    }

}
?>
