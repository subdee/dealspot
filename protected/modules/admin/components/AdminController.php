<?php

class AdminController extends Controller {

    public $layout = 'admin';

    public function init() {
        parent::init();

        // Override error handler
        _app()->errorHandler->errorAction = 'admin/main/error';
    }

}