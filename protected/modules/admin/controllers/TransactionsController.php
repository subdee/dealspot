<?php
class TransactionsController extends AdminController {

    /**
     * Declares class-based actions.
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'expression' => 'User::hasRole(Role::ADMIN | Role::DEV)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
                'redirect' => array('admin/main/login'),
            ),
        );
    }

    public function actionIndex() {
        $trans = new Transaction('search');
        if (isset($_GET['Transaction']))
            $trans->attributes = $_GET['Transaction'];

        $trans->status = Transaction::COMPLETED;

        $this->render('index', array('trans' => $trans));
    }

    public function actionPending() {
        $trans = new Transaction('search');

        if (isset($_GET['Transaction']))
            $trans->attributes = $_GET['Transaction'];

        $trans->status = Transaction::VERIFIED;

        $this->render('pending', array('trans' => $trans));
    }

    public function actionProcess($id) {
        $trans = Transaction::model()->findByPk($id);
        if ($trans)
            $trans->complete();

        $this->redirect(_aurl('transactions/pending'));
    }

}