<?php

class DealsController extends AdminController {

    /**
     * Declares class-based actions.
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'expression' => 'User::hasRole(Role::ADMIN | Role::DEV)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
                'redirect' => array('admin/main/login'),
            ),
        );
    }

    public function actionIndex() {
        $deals = new Deal('search');

        $this->render('index', array('deals' => $deals));
    }

    public function actionAdd() {
        $this->_form(new Deal, new DealDetails);
    }

    public function actionUpdate($id) {
        $this->_form(Deal::model()->findByPk($id), DealDetails::model()->findByPk($id));
    }

    public function actionDelete($id) {
        if (Deal::model()->deleteByPk($id))
            return true;
        else
            return false;
    }

    public function actionActivate($id, $activate = true) {
        Deal::model()->updateByPk($id, array('is_active' => $activate));

        $this->redirect(_aurl('deals/index'));
    }

    private function _form(Deal $deal, DealDetails $dealDetails) {
        $form = new CForm('admin.views.forms.deal');
        $form['deal']->model = $deal;
        $form['dealDetails']->model = $dealDetails;

        if ($form->submitted('save') && $form->validate()) {
            $tempImage = $deal->image;
            $tempSideImage = $deal->side_image;
            $tempSmallImage = $deal->small_image;

            $deal = $form['deal']->model;
            $dealDetails = $form['dealDetails']->model;

            $image = CUploadedFile::getInstance($deal, 'image_file');
            $sideImage = CUploadedFile::getInstance($deal, 'side_image_file');

            $deal->image = $tempImage;
            $deal->side_image = $tempSideImage;
            $deal->small_image = $tempSmallImage;

            if ($deal->save(false)) {
                $dealDetails->deal_id = $deal->id;
                if ($dealDetails->save(false)) {
                    if ($image) {
                        if ($image->saveAs(Utils::createFileName('deal', $deal->id, $image->extensionName, 'deals'))) {
                            $deal->image = Utils::createFileName('deal', $deal->id, $image->extensionName);
                            Utils::ImageResize(Utils::createFileName('deal', $deal->id, $image->extensionName, 'deals'), 698, 347, false, Utils::createFileName('deal', $deal->id, $image->extensionName, 'deals'));
                        }
                    }
                    if ($sideImage) {
                        if ($sideImage->saveAs(Utils::createFileName('deal_side', $deal->id, $sideImage->extensionName, 'deals'))) {
                            $deal->side_image = Utils::createFileName('deal_side', $deal->id, $sideImage->extensionName);
                            Utils::ImageResize(Utils::createFileName('deal_side', $deal->id, $sideImage->extensionName, 'deals'), 390, 283, false, Utils::createFileName('deal_side', $deal->id, $sideImage->extensionName, 'deals'));
                            $deal->small_image = Utils::createFileName('deal_small', $deal->id, $sideImage->extensionName);
                            Utils::ImageResize(Utils::createFileName('deal_side', $deal->id, $sideImage->extensionName, 'deals'), 250, false, true, Utils::createFileName('deal_small', $deal->id, $sideImage->extensionName, 'deals'), false);
                        }
                    }
                    if ($deal->save(false))
                        $this->redirect(_aurl('deals/index'));
                }
            }
        }

        $this->render('add', array('form' => $form));
    }

    public function actionDetails($id) {
        $trans = new Transaction('search');
        if (isset($_GET['Transaction']))
            $trans->attributes = $_GET['Transaction'];
        $trans->deal_id = $id;
        $trans->status = Transaction::COMPLETED;

        $this->render('details', array('trans' => $trans));
    }

}