<?php

class CategoriesController extends AdminController {

    /**
     * Declares class-based actions.
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'expression' => 'User::hasRole(Role::ADMIN | Role::DEV)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
                'redirect' => array('admin/main/login'),
            ),
        );
    }

    public function actionIndex() {
        $categories = new Category('search');

        $this->render('index', array('categories' => $categories));
    }

    public function actionAdd() {
        $this->_form(new Category);
    }

    public function actionUpdate($id) {
        $this->_form(Category::model()->findByPk($id));
    }

    public function actionDelete($id) {
        if (Category::model()->deleteByPk($id))
            return true;
        else
            return false;
    }

    public function actionSub() {
        $subcategories = new Subcategory('search');

        $this->render('sub', array('subcategories' => $subcategories));
    }

    public function actionSubAdd() {
        $this->_subform(new Subcategory);
    }

    public function actionSubUpdate($id) {
        $this->_subform(Subcategory::model()->findByPk($id));
    }

    public function actionSubDelete($id) {
        if (Subcategory::model()->deleteByPk($id))
            return true;
        else
            return false;
    }

    private function _form(Category $category) {
        $form = new CForm('admin.views.forms.category');
        $form['category']->model = $category;

        if ($form->submitted('save') && $form->validate()) {
            $category = $form['category']->model;

            $image = CUploadedFile::getInstance($category, 'image_file');

            if ($category->save(false)) {
                if ($image) {
                    if ($image->saveAs(Utils::createFileName('category', $category->id, $image->extensionName, 'categories'))) {
                        $category->image = Utils::createFileName('category', $category->id, $image->extensionName);
                        Utils::ImageResize(Utils::createFileName('category', $category->id, $image->extensionName, 'categories'), 
                                18, false, true, 
                                Utils::createFileName('category', $category->id, $image->extensionName, 'categories'));
                    }
                }
                if ($category->save(false))
                    $this->redirect(_aurl('categories/index'));
            }
        }

        $this->render('add', array('form' => $form));
    }

    private function _subform(Subcategory $subcategory) {
        $form = new CForm('admin.views.forms.subcategory');
        $form['subcategory']->model = $subcategory;

        if ($form->submitted('save') && $form->validate()) {
            $subcategory = $form['subcategory']->model;
            if ($subcategory->save(false))
                $this->redirect(_aurl('categories/sub'));
        }

        $this->render('subadd', array('form' => $form));
    }

}