<?php

class CouponsController extends AdminController {

    /**
     * Declares class-based actions.
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'expression' => 'User::hasRole(Role::ADMIN | Role::DEV)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
                'redirect' => array('admin/main/login'),
            ),
        );
    }

    public function actionIndex($id = false) {
        $coupons = new Coupon('search');
        if ($id)
            $coupons->transaction_id = $id;

        $this->render('index', array('coupons' => $coupons));
    }

    public function actionAdd() {
        $form = new CForm('admin.views.forms.coupon');
        $form['coupon']->model = new Coupon;

        if ($form->submitted('save') && $form->validate()) {
            $city = $form['city']->model;
            if ($city->save(false))
                $this->redirect(_aurl('cities/index'));
        }

        $this->render('add', array('form' => $form));
    }

    public function actionSend($id) {
        $coupon = Coupon::model()->findByPk($id);
        if ($coupon)
            $coupon->send();

        $this->redirect(_aurl('coupons/index'));
    }

    public function actionActivate($id, $activate = true) {
        Coupon::model()->updateByPk($id, array('is_active' => $activate, 'date_activated' => date(DATE_ISO8601)));

        $this->redirect(_aurl('coupons/index'));
    }

}