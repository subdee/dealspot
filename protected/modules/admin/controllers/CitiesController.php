<?php

class CitiesController extends AdminController {

    /**
     * Declares class-based actions.
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'expression' => 'User::hasRole(Role::ADMIN | Role::DEV)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
                'redirect' => array('admin/main/login'),
            ),
        );
    }

    public function actionIndex() {
        $cities = new City('search');

        $this->render('index', array('cities' => $cities));
    }

    public function actionAdd() {
        $this->_form(new City);
    }

    public function actionUpdate($id) {
        $this->_form(City::model()->findByPk($id));
    }

    public function actionDelete($id) {
        if (City::model()->deleteByPk($id))
            return true;
        else
            return false;
    }

    private function _form(City $city) {
        $form = new CForm('admin.views.forms.city');
        $form['city']->model = $city;

        if ($form->submitted('save') && $form->validate()) {
            $city = $form['city']->model;
            if ($city->save(false))
                $this->redirect(_aurl('cities/index'));
        }

        $this->render('add', array('form' => $form));
    }

}