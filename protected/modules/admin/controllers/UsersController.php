<?php
class UsersController extends AdminController {

    /**
     * Declares class-based actions.
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'expression' => 'User::hasRole(Role::ADMIN | Role::DEV)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
                'redirect' => array('admin/main/login'),
            ),
        );
    }

    public function actionIndex() {
        $users = new User('search');

        $this->render('index', array('users' => $users));
    }

    public function actionAdd() {
        $this->_form(new User, new UserDetails);
    }

    public function actionUpdate($id) {
        $this->_form(User::model()->findByPk($id), UserDetails::model()->findByPk($id));
    }

    public function actionDelete($id) {
        if (User::model()->deleteByPk($id))
            return true;
        else
            return false;
    }

    public function actionNewsletters() {
        $users = new NewsletterUsers('search');

        $this->render('newsletters', array('users' => $users));
    }

    public function actionAddNewsletter() {
        $user = new NewsletterUsers;

        if (isset($_POST['email'])) {
            $user->email = $_POST['email'];
            if ($user->save())
                $this->redirect(_aurl('users/newsletters'));
        }
        $this->render('addNewsletter', array('user' => $user));
    }

    public function actionDeleteNewsletter($email) {
        if (NewsletterUsers::model()->deleteByPk($email))
            return true;
        else
            return false;
    }

    public function actionAdjustPoints() {
        if (isset($_POST['points']) && isset($_POST['user_id'])) {
            $user = User::model()->findByPk($_POST['user_id']);
            if ($user) {
                $user->details->points = $_POST['points'];
                $user->details->save();
            }
        }

        $this->render('adjust', array('users' => User::model()->findAll()));
    }

    public function actionGetUserPoints($user_id) {
        if (Utils::isAjax()) {
            $user = User::model()->findByPk($user_id);
            if ($user)
                echo CHtml::textField('points', $user->details->points);
        }
    }

    private function _form(User $user, UserDetails $userDetails) {
        $form = new CForm('admin.views.forms.user');
        $form['user']->model = $user;
        $form['userDetails']->model = $userDetails;

        if ($form->submitted('save')) {
            $user = $form['user']->model;
            $userDetails = $form['userDetails']->model;

            if (!$user->registration_date || $user->registration_date == '0000-00-00 00:00:00')
                $user->registration_date = date(DATE_ISO8601);

            if ($user->role != Role::CLIENT)
                $user->company_id = null;

            if ($user->save(false)) {
                $userDetails->user_id = $user->id;
                if ($userDetails->save(false))
                    $this->redirect(_aurl('users/index'));
            }
        }

        $this->render('add', array('form' => $form));
    }

}
