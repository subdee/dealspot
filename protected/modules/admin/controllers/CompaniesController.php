<?php

class CompaniesController extends AdminController {

    /**
     * Declares class-based actions.
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'expression' => 'User::hasRole(Role::ADMIN | Role::DEV)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
                'redirect' => array('admin/main/login'),
            ),
        );
    }

    public function actionIndex() {
        $companies = new Company('search');

        $this->render('index', array('companies' => $companies));
    }

    public function actionAdd() {
        $this->_form(new Company);
    }

    public function actionUpdate($id) {
        $this->_form(Company::model()->findByPk($id));
    }

    public function actionDelete($id) {
        if (Company::model()->deleteByPk($id))
            return true;
        else
            return false;
    }

    private function _form(Company $company) {
        $form = new CForm('admin.views.forms.company');
        $form['company']->model = $company;

        if ($form->submitted('save') && $form->validate()) {
            $company = $form['company']->model;
            $oldImage = $company->image;

            $image = CUploadedFile::getInstance($company, 'image_file');

            if ($company->save(false)) {
                if ($image) {
                    if ($image->saveAs(Utils::createFileName('company', $company->id, $image->extensionName, 'companies'))) {
                        $company->image = Utils::createFileName('company', $company->id, $image->extensionName);
                        Utils::ImageResize(Utils::createFileName('company', $company->id, $image->extensionName, 'companies'), 274, 150, false, Utils::createFileName('company', $company->id, $image->extensionName, 'companies'));
                    } else {
                        $company->image = $oldImage;
                    }
                }
                if ($company->save())
                    $this->redirect(_aurl('companies/index'));
            }
        }

        $this->render('add', array('form' => $form));
    }

}