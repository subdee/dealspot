<?php

class MainController extends AdminController {

    /**
     * Declares class-based actions.
     */
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('login'),
                'users' => array('*'),
            ),
            array('allow',
                'expression' => 'User::hasRole(Role::ADMIN | Role::DEV)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
                'redirect' => array('admin/main/login'),
            ),
        );
    }

    public function actionIndex($days = 30) {
        $data = new stdclass;
        $regUsersPerDate = User::getRegisteredUsersPerDate($days);
        $data->totalRegUsers = User::model()->count();
        $data->totalNewsUsers = NewsletterUsers::model()->count();
        $data->totalDeals = Deal::model()->count();
        $data->totalActiveDeals = Deal::model()->count('valid_to > now() AND is_active');
        $incomePerDay = Transaction::getSalesByDate($days);
        $salesByCategory = Transaction::getSalesByCategory();
        $salesByMethod = Transaction::getSalesByPaymentMethod();

	$data->salesByCategory = $data->salesByPaymentMethod = array();
        $data->regUsersData = $data->incomeData = $this->getArrayWithLastXDates($days);

        foreach ($regUsersPerDate as $regUser)
            $data->regUsersData[$regUser['reg_date']] = (int) $regUser['cnt'];

        foreach ($incomePerDay as $income)
            $data->incomeData[$income['trans_date']] = (float) $income['total'];

        foreach ($salesByCategory as $sale)
            $data->salesByCategory[$sale['category']] = (int) $sale['cnt'];

        foreach ($salesByMethod as $saleM)
            $data->salesByPaymentMethod[PaymentMethod::text($saleM['method'])] = (int) $saleM['cnt'];

        $this->render('index', array('data' => $data));
    }

    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login(true))
//                $this->redirect(Yii::app()->user->returnUrl);
                $this->redirect(_aurl('main/index'));
        }
        $this->render('login', array('model' => $model));
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(_aurl('main/index'));
    }

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    private function getArrayWithLastXDates($days) {
        $now = new DateTime;
        $dates = array($now->format('d-m') => 0);
        for ($i = 1; $i < $days; ++$i) {
            $dates[$now->sub(new DateInterval('P1D'))->format('d-m')] = 0;
        }

        return array_reverse($dates);
    }

}
