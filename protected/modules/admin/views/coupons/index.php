<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $coupons->search(),
    'filter' => $coupons,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'name' => 'transaction.deal.company.name',
        ),
        array(
            'name' => 'transaction.deal.name',
        ),
        array(
            'name' => 'name',
        ),
        array(
            'name' => 'coupon_code',
        ),
        array(
            'name' => 'transaction.transaction_date',
            'value' => 'Utils::date($data->transaction->transaction_date)',
        ),
        array(
            'type' => 'raw',
            'name' => 'sent_on',
            'value' => '$data->sent_on ? Utils::date($data->sent_on) : CHtml::link(_t("admin", "send coupon"), _aurl("coupons/send", array("id" => $data->id)))',
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}{activate}{deactivate}',
            'deleteButtonImageUrl' => Utils::adminImageUrl('delete.png'),
            'buttons' => array(
                'activate' => array(
                    'label' => _t('admin', 'activate coupon'),
                    'url' => '_aurl("coupons/activate", array("id" => $data->id))',
                    'imageUrl' => Utils::adminImageUrl('activate.png'),
                    'visible' => '!$data->is_active',
                ),
                'deactivate' => array(
                    'label' => _t('admin', 'deactivate coupon'),
                    'url' => '_aurl("coupons/activate", array("id" => $data->id, "activate" => false))',
                    'imageUrl' => Utils::adminImageUrl('remove.png'),
                    'visible' => '$data->is_active',
                ),
            ),
        ),
    ),
));