<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $companies->search(),
    'filter' => $companies,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'name' => 'name',
        ),
        array(
            'name' => 'email',
        ),
        array(
            'name' => 'phone',
            'filter' => false,
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'deleteButtonImageUrl' => Utils::adminImageUrl('delete.png'),
            'updateButtonImageUrl' => Utils::adminImageUrl('edit.png'),
        ),
    ),
));