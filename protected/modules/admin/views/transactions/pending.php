<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $trans->search(),
    'filter' => $trans,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'name' => 'user_username',
            'value' => '$data->user->username',
        ),
        array(
            'name' => 'deal.name',
        ),
        array(
            'name' => 'amount',
            'value' => 'Utils::currency($data->amount)',
            'filter' => false,
        ),
        array(
            'name' => 'payment_method',
            'value' => 'PaymentMethod::text($data->payment_method)',
            'filter' => PaymentMethod::getArray(),
        ),
        array(
            'name' => 'transaction_date',
            'value' => 'Utils::date($data->transaction_date)',
            'filter' => false,
        ),
        array(
            'header' => _t('admin', 'coupons'),
            'value' => 'count($data->coupons)',
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{process}',
            'buttons' => array(
                'process' => array(
                    'label' => _t('admin', 'process transaction'),
                    'url' => '_aurl("transactions/process", array("id" => $data->id))',
                    'imageUrl' => Utils::adminImageUrl('process.png'),
                ),
            ),
        ),
    ),
));