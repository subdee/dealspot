<div><?php echo _t('admin', 'total registered users'); ?>: <?php echo $data->totalRegUsers; ?></div>
<div><?php echo _t('admin', 'total newsletter users'); ?>: <?php echo $data->totalNewsUsers; ?></div>
<div id="chart-users" class="chart"></div>
<div><?php echo _t('admin', 'total deals'); ?>: <?php echo $data->totalDeals; ?></div>
<div><?php echo _t('admin', 'total active deals'); ?>: <?php echo $data->totalActiveDeals; ?></div>
<div id="chart-sales" class="chart"></div>
<div id="chart-sales-category" class="chart"></div>
<div id="chart-sales-method" class="chart"></div


<?php
$this->widget('ext.highcharts.HighchartsWidget', array(
    'options' => array(
        'credits' => false,
        'chart' => array(
            'defaultSeriesType' => 'column',
            'renderTo' => "chart-sales",
        ),
        'title' => array('text' => _t('admin', 'sales per period')),
        'xAxis' => array(
            'categories' => array_keys($data->incomeData),
            'labels' => array(
                'rotation' => 45,
                'y' => 20,
                'x' => 10,
            ),
        ),
        'yAxis' => array(
            'title' => array('text' => _t('admin', 'sales')),
            'labels' => array('formatter' => 'js:function() {
                console.log(this.value);
                    return "€" + Highcharts.numberFormat(this.value, 2, ",");
                }'),
        ),
        'legend' => array(
            'enabled' => false,
        ),
        'series' => array(
            array('name' => _t('admin', 'sales'), 'data' => array_values($data->incomeData)),
        ),
        'plotOptions' => array(
            'bar' => array(
                'dataLabels' => array(
                    'enabled' => true
                )
            )
        )
    )
));

$this->widget('ext.highcharts.HighchartsWidget', array(
    'options' => array(
        'credits' => false,
        'chart' => array(
            'defaultSeriesType' => 'column',
            'renderTo' => "chart-users",
        ),
        'title' => array('text' => _t('admin', 'registered users')),
        'xAxis' => array(
            'categories' => array_keys($data->regUsersData),
            'labels' => array(
                'rotation' => 45,
                'y' => 20,
                'x' => 10,
            ),
        ),
        'yAxis' => array(
            'title' => array('text' => _t('admin', 'no of users')),
            'allowDecimals' => false,
        ),
        'legend' => array(
            'enabled' => false,
        ),
        'series' => array(
            array('name' => _t('admin', 'users'), 'data' => array_values($data->regUsersData)),
        ),
        'plotOptions' => array(
            'bar' => array(
                'dataLabels' => array(
                    'enabled' => true
                )
            )
        )
    )
));

$this->widget('ext.highcharts.HighchartsWidget', array(
    'options' => array(
        'credits' => false,
        'chart' => array(
            'defaultSeriesType' => 'column',
            'renderTo' => "chart-sales-category",
        ),
        'title' => array('text' => _t('admin', 'sales per category')),
        'xAxis' => array(
            'categories' => array_keys($data->salesByCategory),
        ),
        'yAxis' => array(
            'title' => array('text' => _t('admin', 'no of sales')),
            'allowDecimals' => false,
        ),
        'legend' => array(
            'enabled' => false,
        ),
        'series' => array(
            array('name' => _t('admin', 'sales'), 'data' => array_values($data->salesByCategory)),
        ),
        'plotOptions' => array(
            'bar' => array(
                'dataLabels' => array(
                    'enabled' => true
                )
            )
        )
    )
));

$this->widget('ext.highcharts.HighchartsWidget', array(
    'options' => array(
        'credits' => false,
        'chart' => array(
            'defaultSeriesType' => 'column',
            'renderTo' => "chart-sales-method",
        ),
        'title' => array('text' => _t('admin', 'sales per payment method')),
        'xAxis' => array(
            'categories' => array_keys($data->salesByPaymentMethod),
        ),
        'yAxis' => array(
            'title' => array('text' => _t('admin', 'no of sales')),
            'allowDecimals' => false,
        ),
        'legend' => array(
            'enabled' => false,
        ),
        'series' => array(
            array('name' => _t('admin', 'sales'), 'data' => array_values($data->salesByPaymentMethod)),
        ),
        'plotOptions' => array(
            'bar' => array(
                'dataLabels' => array(
                    'enabled' => true
                )
            )
        )
    )
));