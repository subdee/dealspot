<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $categories->search(),
    'filter' => $categories,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'name' => 'name',
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'deleteButtonImageUrl' => Utils::adminImageUrl('delete.png'),
            'updateButtonImageUrl' => Utils::adminImageUrl('edit.png'),
        ),
    ),
));