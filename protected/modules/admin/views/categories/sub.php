<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $subcategories->search(),
    'filter' => $subcategories,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'name' => 'name',
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'deleteButtonImageUrl' => Utils::adminImageUrl('delete.png'),
            'deleteButtonUrl' => '_aurl("categories/subDelete", array("id" => $data->id))',
            'updateButtonImageUrl' => Utils::adminImageUrl('edit.png'),
            'updateButtonUrl' => '_aurl("categories/subUpdate", array("id" => $data->id))',
        ),
    ),
));