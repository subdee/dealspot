<div class="form">
    <?php echo $form; ?>
</div>
<script>
    $(function(){
        $(".field_company_id").hide();
        $("#User_role").change(function(){
            if ($(this).val() == <?php echo Role::CLIENT; ?>) {
                $(".field_company_id").slideDown();
            } else {
                $(".field_company_id").slideUp();
            }
        })
    })
</script>