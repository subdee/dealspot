<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $users->search(),
    'filter' => $users,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'name' => 'email',
        ),
        array(
            'name' => 'last_sent',
            'filter' => false,
            'value' => '$data->last_sent ? : "' . _t('admin', 'n/a') . '"',
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'deleteButtonImageUrl' => Utils::adminImageUrl('delete.png'),
            'deleteButtonUrl' => '_aUrl("users/deleteNewsletter", array("email" => $data->email))',
        ),
    ),
));