<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $users->search(),
    'filter' => $users,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'name' => 'username',
        ),
        array(
            'name' => 'email',
        ),
        array(
            'name' => 'registration_date',
            'filter' => false,
            'value' => '$data->registration_date ? : "' . _t('admin', 'n/a') . '"',
        ),
        array(
            'header' => _t('admin', 'administrator'),
            'value' => '$data->isAdmin() ? "'. _t('admin', "yes") .'" : "' . _t('admin', "no") . '"',
        ),
        array(
            'header' => _t('admin', 'company manager'),
            'value' => '$data->managesCompany() ? "'. _t('admin', "yes") .'" : "' . _t('admin', "no") . '"',
        ),
        array(
            'name' => 'details.points',
            'value' => '$data->details->points ? : 0',
            'htmlOptions' => array('class' => 'text-right'),
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'deleteButtonImageUrl' => Utils::adminImageUrl('delete.png'),
            'updateButtonImageUrl' => Utils::adminImageUrl('edit.png'),
        ),
    ),
));