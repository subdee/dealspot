<div class="form">
    <?php echo CHtml::form(); ?>

    <div class="row">
        <?php echo CHtml::dropDownList('user_id', '', CHtml::listData($users, 'id', 'username'), array(
            'prompt' => _t('admin', 'select a user'),
            'ajax' => array(
                'type' => 'GET',
                'url' => _aurl('users/getUserPoints'),
                'update' => '.points-slider',
            ),
        )); ?>
    </div>

    <div class="row">
        <div class="points-slider">
            <?php echo CHtml::textField('points'); ?>
        </div>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton(_t('admin', 'save')); ?>
    </div>
</div>