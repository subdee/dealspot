<div class="form">
<?php echo CHtml::form(); ?>

    <div class="row">
        <?php echo CHtml::label(_t('admin', 'email'), 'email'); ?>
        <?php echo CHtml::textField('email'); ?>
    </div>

    <div class="buttons">
        <?php echo Chtml::submitButton(_t('admin', 'Save')); ?>
    </div>
<?php echo CHtml::endForm(); ?>
</div>