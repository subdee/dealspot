<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $deals->search(),
    'filter' => $deals,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'name' => 'name',
            'type' => 'raw',
            'value' => 'CHtml::link($data->name, _aurl("deals/details", array("id" => $data->id)))'
        ),
        array(
            'name' => 'company.name',
        ),
        array(
            'name' => 'city.name',
        ),
        array(
            'name' => 'valid_to',
            'filter' => false,
            'value' => '$data->valid_to ? : "' . _t('admin', 'n/a') . '"',
        ),
        array(
            'name' => 'coupon_price',
            'value' => 'Utils::currency($data->coupon_price)',
            'htmlOptions' => array('class' => 'text-right'),
            'filter' => false,
        ),
        array(
            'header' => _t('admin', 'coupons bought'),
            'value' => '$data->coupons_bought',
            'htmlOptions' => array('class' => 'text-right'),
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}{activate}{deactivate}',
            'deleteButtonImageUrl' => Utils::adminImageUrl('delete.png'),
            'updateButtonImageUrl' => Utils::adminImageUrl('edit.png'),
            'buttons' => array(
                'activate' => array(
                    'label' => _t('admin', 'activate deal'),
                    'url' => '_aurl("deals/activate", array("id" => $data->id))',
                    'imageUrl' => Utils::adminImageUrl('activate.png'),
                    'visible' => '!$data->is_active',
                ),
                'deactivate' => array(
                    'label' => _t('admin', 'deactivate deal'),
                    'url' => '_aurl("deals/activate", array("id" => $data->id, "activate" => false))',
                    'imageUrl' => Utils::adminImageUrl('remove.png'),
                    'visible' => '$data->is_active',
                ),
            ),
        ),
    ),
));