<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $trans->search(),
    'filter' => $trans,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'name' => 'user.details.fullname',
        ),
        array(
            'name' => 'user.email',
        ),
        array(
            'name' => 'amount',
            'value' => 'Utils::currency($data->amount)',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'filter' => false,
        ),
        array(
            'name' => 'payment_method',
            'value' => 'PaymentMethod::text($data->payment_method)',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'filter' => PaymentMethod::getArray(),
        ),
        array(
            'name' => 'transaction_date',
            'value' => 'Utils::date($data->transaction_date)',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'filter' => false,
        ),
        array(
            'header' => 'coupons',
            'type' => 'raw',
            'value' => 'CHtml::link(count($data->coupons), _aurl("coupons/index", array("id" => $data->id)))',
            'htmlOptions' => array('style' => 'text-align: right;'),
        ),
    ),
));