<?php
return array(
    'attributes' => array(
        'id' => 'categoryForm',
        'enctype' => 'multipart/form-data'
    ),
    'elements' => array(
        'category' => array(
            'type' => 'form',
            'elements' => array(
                'name' => array(
                    'type' => 'text',
                    'maxlength' => 50,
                ),
                'image_file' => array(
                    'type' => 'file',
                ),
            ),
        ),
        '<hr/>',
    ),
    'buttons' => array(
        'save' => array(
            'type' => 'submit',
            'label' => _t('admin', 'save'),
        ),
    ),
);