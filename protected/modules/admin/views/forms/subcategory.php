<?php
return array(
    'elements' => array(
        'subcategory' => array(
            'type' => 'form',
            'elements' => array(
                'name' => array(
                    'type' => 'text',
                    'maxlength' => 50,
                ),
                'category_id' => array(
                    'type' => 'dropdownlist',
                    'items' => CHtml::listData(Category::model()->findAll(), 'id', 'name'),
                ),
                'tag' => array(
                    'type' => 'text',
                    'maxlength' => 255,
                ),
            ),
        ),
        '<hr/>',
    ),
    'buttons' => array(
        'save' => array(
            'type' => 'submit',
            'label' => _t('admin', 'save'),
        ),
    ),
);