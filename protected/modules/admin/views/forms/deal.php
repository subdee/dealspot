<?php
return array(
    'attributes' => array(
        'id' => 'dealForm',
        'enctype' => 'multipart/form-data'
    ),
    'elements' => array(
        'deal' => array(
            'type' => 'form',
            'elements' => array(
                'name' => array(
                    'type' => 'text',
                ),
                'valid_from' => array(
                    'type' => 'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                    'attributes' => array(
                        'options' => array(
                            'showAnim' => 'fold',
                            'minDate' => date('Y-m-d'),
                            'hourGrid' => 4,
                            'dateFormat' => 'yy-mm-dd',
                            'timeFormat' => 'hh:mm:ss',
                            'changeMonth' => true,
                            'changeYear' => true,
                        ),
                        'language' => _app()->language,
                        'htmlOptions' => array(
                            'style' => 'height: 20px;'
                        ),
                    ),
                ),
                'valid_to' => array(
                    'type' => 'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                    'attributes' => array(
                        'options' => array(
                            'showAnim' => 'fold',
                            'minDate' => date('Y-m-d'),
                            'hourGrid' => 4,
                            'dateFormat' => 'yy-mm-dd',
                            'timeFormat' => 'hh:mm:ss',
                            'changeMonth' => true,
                            'changeYear' => true,
                        ),
                        'language' => _app()->language,
                        'htmlOptions' => array(
                            'style' => 'height: 20px;'
                        ),
                    ),
                ),
                'coupon_valid_to' => array(
                    'type' => 'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                    'attributes' => array(
                        'options' => array(
                            'showAnim' => 'fold',
                            'minDate' => date('Y-m-d'),
                            'hourGrid' => 4,
                            'dateFormat' => 'yy-mm-dd',
                            'timeFormat' => 'hh:mm:ss',
                            'changeMonth' => true,
                            'changeYear' => true,
                        ),
                        'language' => _app()->language,
                        'htmlOptions' => array(
                            'style' => 'height: 20px;'
                        ),
                    ),
                ),
                'original_price' => array(
                    'type' => 'text',
                ),
                'coupon_price' => array(
                    'type' => 'text',
                ),
                'min_required' => array(
                    'type' => 'text',
                ),
                'max_coupons_user' => array(
                    'type' => 'text',
                ),
                'available_coupons' => array(
                    'type' => 'text',
                ),
                'image_file' => array(
                    'type' => 'file',
                ),
                'side_image_file' => array(
                    'type' => 'file',
                ),
                '<hr />',
                'company_id' => array(
                    'type' => 'dropdownlist',
                    'items' => CHtml::listData(Company::model()->findAll(), 'id', 'name'),
                ),
                'city_id' => array(
                    'type' => 'dropdownlist',
                    'items' => CHtml::listData(City::model()->findAll(), 'id', 'name'),
                ),
                'subcategory_id' => array(
                    'type' => 'dropdownlist',
                    'items' => Category::model()->getTree(),
                    'prompt' => _t('admin', 'form.select_subcategory'),
                ),
            ),
        ),
        '<hr/>',
        'dealDetails' => array(
            'type' => 'form',
            'elements' => array(
                'subtitle' => array(
                    'type' => 'text',
                    'maxlength' => 100,
                ),
                'terms_of_use' => array(
                    'type' => 'ext.ckeditor.TheCKEditorWidget',
                    'attributes' => array(
                        'height' => '400px',
                        'width' => '100%',
                        'toolbarSet' => 'Basic',
                        'ckeditor' => Yii::app()->basePath . '/../ckeditor/ckeditor.php',
                        'ckBasePath' => Yii::app()->baseUrl . '/ckeditor/',
                        'css' => Yii::app()->baseUrl . '/css/index.css',
                        'config' => array(
                            'entities' => false,
                        ),
                    ),
                ),
                'coupon_instructions' => array(
                    'type' => 'ext.ckeditor.TheCKEditorWidget',
                    'attributes' => array(
                        'height' => '400px',
                        'width' => '100%',
                        'toolbarSet' => 'Basic',
                        'ckeditor' => Yii::app()->basePath . '/../ckeditor/ckeditor.php',
                        'ckBasePath' => Yii::app()->baseUrl . '/ckeditor/',
                        'css' => Yii::app()->baseUrl . '/css/index.css',
                        'config' => array(
                            'entities' => false,
                        ),
                    ),
                ),
            ),
        ),
    ),
    'buttons' => array(
        'save' => array(
            'type' => 'submit',
            'label' => _t('admin', 'save'),
        ),
    ),
);