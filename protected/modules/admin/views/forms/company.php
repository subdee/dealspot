<?php
return array(
    'attributes' => array(
        'id' => 'dealForm',
        'enctype' => 'multipart/form-data'
    ),
    'elements' => array(
        'company' => array(
            'type' => 'form',
            'elements' => array(
                'name' => array(
                    'type' => 'text',
                    'maxlength' => 50,
                ),
                'email' => array(
                    'type' => 'text',
                    'maxlength' => 100,
                ),
                'website' => array(
                    'type' => 'text',
                    'maxlength' => 100,
                ),
                'phone' => array(
                    'type' => 'CMaskedTextField',
                    'attributes' => array(
                        'mask' => '999 99 99 999',
                    ),
                ),
                'fax' => array(
                    'type' => 'CMaskedTextField',
                    'attributes' => array(
                        'mask' => '999 99 99 999',
                    ),
                ),
                'mobile' => array(
                    'type' => 'CMaskedTextField',
                    'attributes' => array(
                        'mask' => '999 99 99 999',
                    ),
                ),
                'description' => array(
                    'type' => 'ext.ckeditor.TheCKEditorWidget',
                    'attributes' => array(
                        'height' => '400px',
                        'width' => '100%',
                        'toolbarSet' => 'Basic',
                        'ckeditor' => Yii::app()->basePath . '/../ckeditor/ckeditor.php',
                        'ckBasePath' => Yii::app()->baseUrl . '/ckeditor/',
                        'css' => Yii::app()->baseUrl . '/css/index.css',
                        'config' => array(
                            'entities' => false,
                        ),
                    ),
                ),
                'address' => array(
                    'type' => 'textarea',
                ),
                'facebook' => array(
                    'type' => 'text',
                    'maxlength' => 100,
                ),
                'twitter' => array(
                    'type' => 'text',
                    'maxlength' => 100,
                ),
                'google_plus' => array(
                    'type' => 'text',
                    'maxlength' => 100,
                ),
                'payment_details' => array(
                    'type' => 'textarea',
                ),
                'image_file' => array(
                    'type' => 'file',
                ),
            ),
        ),
        '<hr/>',
    ),
    'buttons' => array(
        'save' => array(
            'type' => 'submit',
            'label' => _t('admin', 'save'),
        ),
    ),
);