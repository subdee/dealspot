<?php
return array(
    'elements' => array(
        'city' => array(
            'type' => 'form',
            'elements' => array(
                'name' => array(
                    'type' => 'text',
                    'maxlength' => 50,
                ),
            ),
        ),
        '<hr/>',
    ),
    'buttons' => array(
        'save' => array(
            'type' => 'submit',
            'label' => _t('admin', 'save'),
        ),
    ),
);