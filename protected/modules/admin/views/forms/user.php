<?php
return array(
    'elements' => array(
        'user' => array(
            'type' => 'form',
            'elements' => array(
                'username' => array(
                    'type' => 'text',
                    'maxlength' => 32,
                ),
                'password' => array(
                    'type' => 'password',
                ),
                'email' => array(
                    'type' => 'text',
                    'maxlength' => 50,
                ),
                'role' => array(
                    'type' => 'dropdownlist',
                    'items' => Role::getRoles(),
                ),
                'company_id' => array(
                    'type' => 'dropdownlist',
                    'items' => CHtml::listData(Company::model()->findAll(), 'id', 'name'),
                ),
            ),
        ),
        '<hr/>',
        'userDetails' => array(
            'type' => 'form',
            'elements' => array(
                'fullname' => array(
                    'type' => 'text',
                    'maxlength' => 100,
                ),
                'mobile' => array(
                    'type' => 'text',
                    'maxlength' => 20,
                ),
                'gender' => array(
                    'type' => 'dropdownlist',
                    'items' => Gender::getGenders(),
                    'prompt' => _t('admin', 'select gender'),
                ),
                'birthdate' => array(
                    'type' => 'zii.widgets.jui.CJuiDatePicker',
                    'attributes' => array(
                        'options' => array(
                            'showAnim' => 'fold',
                        ),
                        'htmlOptions' => array(
                            'style' => 'height: 20px;'
                        ),
                    ),
                ),
            ),
        ),
    ),
    'buttons' => array(
        'save' => array(
            'type' => 'submit',
            'label' => _t('user', 'save'),
        ),
    ),
);