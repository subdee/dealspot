<?php
return array(
    'attributes' => array(
        'id' => 'couponForm',
    ),
    'elements' => array(
        'coupon' => array(
            'type' => 'form',
            'elements' => array(
                'transaction_id' => array(
                    'type' => 'dropdownlist',
                    'items' => CHtml::listData(Transaction::model()->completed()->findAll(), 'id', 'details'),
                ),
                'name' => array(
                    'type' => 'text',
                ),
            ),
        ),
        '<hr/>',
    ),
    'buttons' => array(
        'save' => array(
            'type' => 'submit',
            'label' => _t('admin', 'save'),
        ),
    ),
);