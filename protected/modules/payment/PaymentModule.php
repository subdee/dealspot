<?php

class PaymentModule extends WebModule {

    public $autoinit;

    public function init() {
        $this->setImport(array(
            'payment.components.*',
            'payment.extensions.*',
        ));

        $this->setModules(array('payPal'));
    }
}
