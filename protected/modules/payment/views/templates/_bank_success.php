<p><?php echo _t('shop', 'you have chosen to make a bank payment'); ?></p>
<p><?php echo _t('shop', 'an email has been sent that details the bank information and instructions to complete bank payment'); ?></p>
<p style="font-weight: bold;"><?php echo _t('shop', 'Instructions'); ?>:</p>
<p><?php echo _t('shop', 'to receive your coupon you must'); ?>:</p>
<ol>
    <li><?php echo _t('shop', 'deposit the exact amount of the deal you have chosen, within three (3) business days to activate your coupon in time'); ?></li>
    <p style="font-size: 0.8em; color: #8A1F11; font-style: italic;"><?php echo _t('shop', 'in case activation does not complete within those days, your order will be removed automatically from our system'); ?></p>
    <li><?php echo _t('shop', 'include your username or email in the payment for reference'); ?></li>
    <li><?php echo _t('shop', 'when your deposit is complete and verification is done within the day, you will receive an email with the coupon number and print instructions'); ?></li>
</ol>
<div class="error">
    <?php echo CHtml::image(Utils::imageUrl('danger-icon.png'), '', array('style' => 'vertical-align:middle;')); ?>
    <?php echo _t('shop', 'your coupon will remain inactive until the deposit has been verified'); ?>
</div>
<p><?php echo _t('shop', 'deposit can be done at the following banks'); ?>:</p>
<p style="font-weight: bold;">
    <?php echo _t('shop', 'national bank: 234423423424234'); ?>
</p>