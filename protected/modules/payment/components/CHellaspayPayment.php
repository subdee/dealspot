<?php

class CHellaspayPayment extends CPayment {

    private $_url = 'https://www.vivapayments.com/';
    protected $username = '74d70ddd-11d0-4dce-b459-960358ec2b72';
    protected $password = 'operatorsippokratis';

    public function create(Cart $cart) {
        Yii::import('payment.extensions.HellasPay');

        $result = json_decode($this->send($cart->total));
        if ($result && !$result->ErrorCode) {
            $txnID = $result->OrderCode;
            $trans = Transaction::createNew($cart, PaymentMethod::HELLASPAY, array('txn_id' => $txnID));
            _app()->session['trans'] = $trans->id;
            _app()->controller->redirect($this->_url . 'web/newtransaction.aspx?ref=' . $txnID);
        }
        return false;
    }

    public function verify($data) {
        if (isset($data['s'])) {
            $hp = HellaspayTransaction::model()->find('txn_id = :txnID', array(':txnID' => $data['s']));
            if ($hp) {
                $response = array();
                $response['image'] = Utils::imageUrl('hellaspay.png');
                $response['text'] = $this->verifyText();
                $hp->transaction->verify();

                return $response;
            }
        }
        return false;
    }

    public function process($data) {
        parent::process();
        if ($this->trans) {
            $this->trans->deal_id = $this->cart->deal->id;
            if ($this->trans->complete()) {
                return $this->successText();
            }
        }
        return false;
    }

    public function cancel() {
        return Transaction::model()->findByPk(_app()->session['trans'])->cancel();
    }

    private function verifyText() {
        return _t('shop', 'hellaspay verify text');
    }

    private function successText() {
        return _app()->controller->renderPartial('payment.views.templates._hellaspay_success', null, true);
    }

    protected function send($amount) {
        $curl = curl_init($this->_url . 'api/orders');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, $this->username . ':' . $this->password);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, 'Amount=' . $amount * 100 . '&SourceCode=2865');

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }

}
