<?php

/**
 * Description of IPayment
 *
 * @author kthermos
 */
abstract class CPayment {
    
    protected $cart;
    protected $trans;

    public function create() {
        Yii::app()->controller->redirect(_url('shop/verify'));
    }

    abstract public function verify($data);

    public function process() {
        $this->trans = Transaction::model()->findByPk(_app()->session['trans']);
        $this->cart = unserialize(_app()->session['cart']);
        unset(_app()->session['cart']);
        unset(_app()->session['trans']);
        _app()->session['amount'] = $this->trans->amount;
        _app()->session['id'] = $this->trans->id;
    }
    
    public function afterProcess() {
        if ($this->trans->status == Transaction::COMPLETED)
            Utils::triggerEvent ('onSuccesfulPayment', $this);
    }
    
    abstract public function cancel();

}
