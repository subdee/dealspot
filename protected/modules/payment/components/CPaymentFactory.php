<?php

/**
 * Description of CPaymentFactory
 *
 * @author kthermos
 */
class CPaymentFactory {

    public function build($type = null) {
        if (!$type) {
            $t = Transaction::model()->findByPk(_app()->session['trans']);
            if ($t)
                $type = $t->payment_method;
        }
        switch ($type) {
            case PaymentMethod::PAYPAL:
                return new CPaypalPayment;
                break;
            case PaymentMethod::POINTS:
                return new CPointsPayment;
                break;
            case PaymentMethod::BANK:
                return new CBankPayment;
                break;
            case PaymentMethod::HELLASPAY:
                return new CHellaspayPayment;
                break;
            default:
                unset(_app()->session['trans']);
                return false;
                break;
        }
    }

}