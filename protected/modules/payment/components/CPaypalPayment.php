<?php

class CPaypalPayment extends CPayment {

    public function create(Cart $cart) {
        $trans = Transaction::createNew($cart, PaymentMethod::PAYPAL);
        _app()->session['trans'] = $trans->id;

        $amount = number_format($trans->amount, 2, '.', ',');
        $token = PayPalModule::sendExpressPayment($amount, $trans->paypal);

        _app()->controller->redirect(PayPalModule::getPaymentUrl($token));
    }

    public function verify($data) {
        $response = PayPalModule::verifyExpressPayment($data['token']);
        $response['image'] = Utils::imageUrl('paypal.png');
        $response['text'] = $this->verifyText();

        Transaction::model()->findByPk(_app()->session['trans'])->verify();

        return $response;
    }

    public function process($data) {
        $ids = PayPalModule::submitExpressPayment($data);
        parent::process();
        if ($this->trans) {
            $this->trans->deal_id = $this->cart->deal->id;

            if ($ids) {
                $this->trans->paypal->txn_id = $ids['txn_id'];
                $this->trans->paypal->correlation_id = $ids['cor_id'];
                $this->trans->complete();
            }

            if ($this->trans->save() && $this->trans->paypal->save())
                return $this->successText();
        }
        return false;
    }

    public function cancel() {
        return Transaction::model()->findByTokenOrTransaction($_GET['CORRELATIONID'])->cancel();
    }

    public function ipn($controller) {
        $ipn = new PPIpnAction($controller, "ipn");

        $ipn->onRequest = function($event) {
                    if (!isset($event->details["txn_id"])) {
                        $event->msg = "Missing txn_id";
                        Yii::log($event->msg, "warning", "payPal.controllers.DefaultController");
                        $event->sender->onFailure($event);
                        return;
                    }
                    if ($event->details['payment_status'] === 'Completed')
                        $event->sender->onSuccess($event);
                };

        $ipn->onSuccess = function($event) {
                    $transaction = Transaction::model()->findByTokenOrTransaction($event->details['txn_id']);
                    $transaction->is_valid = true;

                    $transaction->save();
                };

        $ipn->run();
    }

    private function verifyText() {
        return _t('shop', 'paypal verify text');
    }

    private function successText() {
        return _app()->controller->renderPartial('payment.views.templates._paypal_success', null, true);
    }

}
