<?php

class CBankPayment extends CPayment {

    public function create(Cart $cart) {
        $trans = Transaction::createNew($cart, PaymentMethod::BANK);
        _app()->session['trans'] = $trans->id;

        parent::create();
    }

    public function verify($data) {
        $response = array();
        $response['image'] = Utils::imageUrl('bank.png');
        $response['text'] = $this->verifyText();

        Transaction::model()->findByPk(_app()->session['trans'])->verify();

        return $response;
    }

    public function process($data) {
        parent::process();
        Utils::sendEmail(_profile()->email, _t('shop', 'subject for bank payment email'), $this->successText());
        return $this->successText();
    }

    public function cancel() {
        return Transaction::model()->findByPk(_app()->session['trans'])->cancel();
    }

    private function verifyText() {
        return _t('shop', 'bank verify text');
    }

    private function successText() {
        return _app()->controller->renderPartial('payment.views.templates._bank_success', null, true);
    }

}
