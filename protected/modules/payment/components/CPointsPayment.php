    <?php

class CPointsPayment extends CPayment {

    public function create(Cart $cart) {
        if (($cart->total * 100) <= _profile()->details->points) {
            $trans = Transaction::createNew($cart, PaymentMethod::POINTS);
            _app()->session['trans'] = $trans->id;

            parent::create();
        } else {
            _user()->setFlash('nopoints', _t('shop', 'You don\'t have enough points!'));
            _app()->controller->redirect(_url('shop/cart'));
        }
    }

    public function verify($data) {
        $response = array();
        $response['image'] = Utils::imageUrl('points.png');
        $response['text'] = $this->verifyText();

        Transaction::model()->findByPk(_app()->session['trans'])->verify();

        return $response;
    }

    public function process($data) {
        parent::process();
        if ($this->trans) {
            $this->trans->deal_id = $this->cart->deal->id;
            if ($this->trans->complete()) {
                $pt = new PointTransaction;
                $pt->user_id = _profile()->id;
                $pt->points = -($this->trans->amount * 100);
                $pt->reason = PointTransactionReason::DEAL;
                $pt->transaction_date = date(DATE_ISO8601);
                $pt->are_redeemable = true;
                $pt->save();
                
                _profile()->details->points -= ($this->trans->amount * 100);
                if (_profile()->details->save(false))
                    return $this->successText();
            }
        }
        return false;
    }

    public function cancel() {
        return Transaction::model()->findByPk(_app()->session['trans'])->cancel();
    }

    private function verifyText() {
        return _t('shop', 'points verify text');
    }

    private function successText() {
        return _app()->controller->renderPartial('payment.views.templates._points_success', null, true);
    }

}
