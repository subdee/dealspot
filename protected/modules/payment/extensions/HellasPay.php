<?php

class HellasPay {
  const MODE_PRODUCTION  = 1;
  const MODE_DEVELOPMENT = 2;

  const SERVICE_WEBCHECKOUT              = 0;
  const SERVICE_TAXCARD                  = 1;
  const SERVICE_WEBCHECKOUT_WITH_TAXCARD = 2;

  /**
   * The remote web service endpoint
   *
   * @var string
   */
  private $_endpoint;

  /**
   * The merchant id
   *
   * @var string
   */
  private $_merchantId;

  /**
   * The password
   *
   * @var string
   */
  private $_password;

  /**
   * The SOAP client
   *
   * @var SoapClient
   */
  protected $_soap;

  /**
   * Class constructor
   *
   * @param const $mode The mode of operation
   * @param string $merchantId The id of the merchant. Not required in development environment
   * @param string $password The password of the merchant. Not required in development environment
   *
   * @throws HellasPay_Exception
   *
   * @return HellasPay
   */
  public function __construct( $mode, $merchantId = null, $password = null ) {
    switch( $mode ) {
      case self::MODE_PRODUCTION:
        $this->_endpoint = 'https://www.hellaspay.gr/HellasPaySVC/service.svc?wsdl';

        if( !$merchantId || !$password )
          throw new HellasPay_Exception("Please enter the merchant id and password for production mode");

        $this->_merchantId = $merchantId;
        $this->_password   = $password;
      break;
      case self::MODE_DEVELOPMENT:
        //$ini = ini_set("soap.wsdl_cache_enabled","0");
        $this->_endpoint = 'http://demo.hellaspay.gr/HellasPaySVC/service.svc?wsdl';

        $this->_merchantId = ($merchantId) ? $merchantId : '0B586B1C-84AE-4259-8C93-DCDCB0266B86';  // Default Merchant ID and Password for demo environment
        $this->_password   = ($password)   ? $password   : '%^&YHNmju';
      break;
      default:
        throw new HellasPay_Exception("You must specify a valid operation mode (HellasPay::MODE_PRODUCTION / HellasPay::MODE_DEVELOPMENT)");
    }

    try {
      $this->_soap = new SoapClient($this->_endpoint, array(
        'location'      => str_replace("?wsdl", "", $this->_endpoint) . "/Basic",
        'trace'         => true,
        'exceptions'    => true,
        'soap_version'  => SOAP_1_1
      ));
    }
    catch( Exception $e ) {
      throw new HellasPay_Exception("Unable to connect to remote endpoint (" . $e->getMessage() . ")");
    }
  }

  public function getExposedFunctions() {
    return $this->_soap->__getFunctions();
  }

  public function getExposedTypes() {
    return $this->_soap->__getTypes();
  }

  /**
   * Create a new payment order
   *
   * @param float $amount The amount in euros to create the order for
   * @param const $service The service type used
   * @param string $language The language
   * @param array $extra Extra parameters. Please check the documentation
   *
   * @throws HellasPay_Exception
   *
   * @return in
   */
  public function createOrder( $amount, $service = self::SERVICE_WEBCHECKOUT, $language = 'el-GR', $extra = array() ) {
    if( $service < 0 || $service > 2 )
       throw new HellasPay_Exception("Invalid service type requested. (Accepted values are HellasPay::SERVICE_WEBCHECKOUT / HellasPay::SERVICE_TAXCARD / HellasPay::SERVICE_WEBCHECKOUT_WITH_TAXCARD)");

    if( !is_numeric($amount) )
      throw new HellasPay_Exception("Invalid amount specified ($amount). Please enter the amount in a float value");

    $amount = $amount * 100;

    $request = array(
      'ServiceId'   => $service,
      'MerchantId'  => $this->_merchantId,
      'Amount'      => $amount,
      'RequestLang' => $language,
      'Password'    => $this->_password
    );

    if( !empty($extra) )
      $request = array_merge($request, $extra);

    $result = null;

    try {
      $result = $this->_soap->CreateOrder(
        array(
          'oOpt' => $request
        )
      );
    }
    catch( SoapFault $e ) {
      throw new HellasPay_Exception("Unable to contact remote server (" . $e->getMessage() . ")");
    }
    catch( Exception $e ) {
      throw new HellasPay_Exception("Unable to create a new order (" . $e->getMessage() . ")");
    }

    if( $result && isset($result->CreateOrderResult) ) {
      if( $result->CreateOrderResult->ErrorCode != 0 )
        throw new HellasPay_Exception("Unable to create a new order (" . $result->CreateOrderResult->ErrorText . ")");

      return $result->CreateOrderResult->OrderCode;
    }
    else
      throw new HellasPay_Exception("Unable to contact remote server. Empty response");

    return 0;
  }

  /**
   * Get the completed transactions based on date or order code
   *
   * @param string $date The date to get the transactions of. Date format must be parsable by strtotime()
   * @param long $orderCode The order code to search by
   *
   * @throws HellasPay_Exception
   *
   * @return array|stdClass
   */
  public function getTransactions( $date = null, $orderCode = null ) {
    $request = array(
      'MerchantId' => $this->_merchantId,
      'Password'   => $this->_password
    );

    if( $date ) {
      if( !($time = strtotime($date)) )
        throw new HellasPay_Exception("Invalid date format. Please use a date format that can be parsed by strtotime(). See - http://php.net/manual/en/function.strtotime.php");

      $request['Date'] = date("c", $time);
    }

    if( $orderCode && is_numeric($orderCode) )
      $request['OrderCode'] = $orderCode;

    if( !$date && !$orderCode )
      throw new HellasPay_Exception("Please specify either the date or the order code to get transactions for");

    $result = null;

    try {
      $result = $this->_soap->GetTransactions(
        array(
          'sOpt' => $request
        )
      );
    }
    catch( SoapFault $e ) {
      throw new HellasPay_Exception("Unable to contact remote server (" . $e->getMessage() . ")");
    }
    catch( Exception $e ) {
      throw new HellasPay_Exception("Unable to retrieve a list of transactions (" . $e->getMessage() . ")");
    }

    if( $result && isset($result->GetTransactionsResult) ) {
      if( $result->GetTransactionsResult->ErrorCode != 0 )
        throw new HellasPay_Exception("Unable to retrieve transactions (" . $result->GetTransactionsResult->ErrorText . ")");

      return $result->GetTransactionsResult->Transactions->Transaction;
    }
    else
      throw new HellasPay_Exception("Unable to contact remote server. Empty response");

    return null;
  }

  /**
   * If a payment has already been completed and the client has confirmed to be charged again by the merchant you can use the doTransaction to charge the client again
   *
   * @param string $transactionId The transaction id
   * @param float $amount The amount to charge the client
   * @param const $service The service type used
   *
   * @throws HellasPay_Exception
   *
   * @return boolean
   */
  public function doTransaction( $transactionId, $amount, $service = self::SERVICE_WEBCHECKOUT ) {
    if( !is_numeric($amount) )
      throw new HellasPay_Exception("Amount must be numeric");

    $request = array(
      'MerchantId'    => $this->_merchantId,
      'Password'      => $this->_password,
      'TransactionId' => $transactionId,
      'Amount'        => $amount * 100,
      'ServiceId'     => $service
    );

    $result = null;

    try {
      $result = $this->_soap->DoTransaction(
        array(
          'tOpt' => $request
        )
      );
    }
    catch( SoapFault $e ) {
      throw new HellasPay_Exception("Unable to contact remote server (" . $e->getMessage() . ")");
    }
    catch( Exception $e ) {
      throw new HellasPay_Exception("Unable to complete a transaction (" . $e->getMessage() . ")");
    }

    if( $result && isset($result->DoTransactionResult) ) {
      if( $result->DoTransactionResult->ErrorCode != 0 )
        throw new HellasPay_Exception("Unable to complete a transaction (" . $result->DoTransactionResult->ErrorText . ")");

      return $result->DoTransactionResult;
    }
    else
      throw new HellasPay_Exception("Unable to contact remote server. Empty response");
  }

  /**
   * A merchant can cancel a transaction if it was made in the same day until 22:00 or partially/whole if it was made a previous day
   *
   * @param string $transactionId The id of the transaction
   * @param float $amount The amount to refund
   *
   * @throws HellasPay_Exception
   *
   * @return stdClass
   */
  public function doRefund( $transactionId, $amount ) {
    if( !is_numeric($amount) )
      throw new HellasPay_Exception("Amount must be numeric");

    $request = array(
      'MerchantId'    => $this->_merchantId,
      'Password'      => $this->_password,
      'TransactionId' => $transactionId,
      'Amount'        => $amount * 100
    );

    $result = null;

    try {
      $result = $this->_soap->DoRefund(
        array(
          'tOpt' => $request
        )
      );
    }
    catch( SoapFault $e ) {
      throw new HellasPay_Exception("Unable to contact remote server (" . $e->getMessage() . ")");
    }
    catch( Exception $e ) {
      throw new HellasPay_Exception("Unable to complete a refund (" . $e->getMessage() . ")");
    }

    if( $result && isset($result->DoRefundResult) ) {
      if( $result->DoRefundResult->ErrorCode != 0 )
        throw new HellasPay_Exception("Unable to complete a refund (" . $result->DoRefundResult->ErrorText . ")");

      return $result->DoRefundResult;
    }
    else
      throw new HellasPay_Exception("Unable to contact remote server. Empty response");
  }
}

class HellasPay_Exception extends Exception {}

