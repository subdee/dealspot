<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
    <div class="span-7 prepend-top append-bottom">
        <?php echo $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>