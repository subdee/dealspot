<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
    <div class="span-7 banner prepend-top append-bottom"><?php echo CHtml::image(Utils::imageUrl('banner.jpg')); ?></div>
</div>
<div class="container">
    <div class="span-2">
        <div><?php $this->widget('MainMenu'); ?></div>
        <div class="total-savings-box"><?php $this->widget('TotalSavingsBox'); ?></div>
        <div class="invite-box"><?php $this->widget('InviteBox'); ?></div>
        <div class="facebook-like-box">
            <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fdealspot.gr&amp;width=228&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=false&amp;appId=447665841933638" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:228px; height:258px;" allowTransparency="true"></iframe>
        </div>
    </div>
    <div class="span-5 append-bottom last">
        <div class="box"><?php echo $content; ?></div>
    </div>
</div>
<?php $this->endContent(); ?>