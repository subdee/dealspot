<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />

        <!-- external sources -->
        <?php // Utils::registerJsFile('jquery.selectbox-0.2', CClientScript::POS_BEGIN); ?>
        <?php // Utils::registerCssFile('jquery.selectbox', CClientScript::POS_BEGIN); ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>

        <!-- main -->
        <?php Utils::registerJsFile('main', CClientScript::POS_BEGIN); ?>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

        <!--GOOGLE ANALYTICS-->
        <script type="text/javascript">
            var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36729387-1']);
        _gaq.push(['_trackPageview']);
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
    </head>

    <body>
        <div class="user-bar-background">
            <div class="container">
                <div class="user-bar span-7">
                    <div class="prepend-4 span-1 friend-invite text-right">
                        <?php if (Utils::isLoggedIn()) : ?>
                            <?php echo CHtml::link(_t('main', 'invite a friend'), _url('user/invite'), array('onclick' => '$("#invitebox").dialog("open"); return false;')); ?>
                        <?php endif; ?>
                    </div>
                    <?php $this->widget('PersonalMenu'); ?>
                </div>
            </div>
        </div>
        <div class="header-bar-background">
            <div class="container">
                <div class="header-bar">
                    <div class="span-2 append-1 logo"><?php echo CHtml::link(CHtml::image(Utils::imageUrl('logo.png')), _url('site/index')); ?></div>
                    <div class="span-1 append-1 city-choice">
                        <?php
                        echo CHtml::dropDownList('city', '', CHtml::listData(
                                City::model()->findAll(), 'id', 'name'), array('onchange' => '', 'id' => 'city-dropdown'));
                        ?></div>
                    <div class="span-2 last newsletter-register text-right">
                        <?php if (!Utils::isLoggedIn()) : ?>
                            <?php echo CHtml::link(_t('main', 'register for newsletter'), _url('site/newsletter'), array('onclick' => '$("#newsletterbox").dialog("open"); return false;')); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $content; ?>
        <div class="footer-background prepend-top">
            <div class="container">
                <div class="footer">
                    <div class="span-2 append-1 logo"><?php echo CHtml::image(Utils::imageUrl('logo.png')); ?></div>
                    <div class="span-2 append-1 bottom-menu">
                        <table>
                            <tr>
                                <td><?php echo CHtml::link(_t('menu', 'about us'), '#'); ?></td>
                                <td><?php echo CHtml::link(_t('menu', 'faq'), _url('site/faq')); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo CHtml::link(_t('menu', 'contact'), _url('site/contact')); ?></td>
                                <td><?php echo CHtml::link(_t('menu', 'companies'), _url('site/companies')); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo CHtml::link(_t('menu', 'how it works'), _url('site/howItWorks')); ?></td>
                                <td><?php echo CHtml::link(_t('menu', 'terms of use'), _url('site/termsOfUse')); ?></td>
                            </tr>
                            <tr>
                                <td><?php // echo CHtml::link(_t('menu', 'jobs'), _url('site/jobs'));  ?></td>
                                <td><?php echo CHtml::link(_t('menu', 'privacy policy'), '#'); ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="span-1 last social-icons">
                        <div>
                            <?php echo CHtml::link(CHtml::image(Utils::imageUrl('fb_bottom.png')), 'http://www.facebook.com/Dealspot.gr', array('target' => '_blank')); ?>
                            <?php echo CHtml::link(CHtml::image(Utils::imageUrl('twi_bottom.png')), 'https://twitter.com/dealspotgr', array('target' => '_blank')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright small text-right">
            <?php
            echo _t('main', 'footer copyright by {operators}', array(
                '{operators}' => CHtml::tag('span', array(), CHtml::link(
                        CHtml::image(Utils::imageUrl('cube_operators.png')) . 'OPERATORS_', 'http://www.operators.gr/', array('style' => 'color:#0786C8', 'target' => '_blank')
                    )
                )));
            ?>
        </div>
        <script type="text/javascript" src="https://gr.linkwi.se/delivery/js/tl.js"></script>
    </body>
</html>
<?php
if (Utils::isLoggedIn()) :
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'invitebox',
        'themeUrl' => _bu('css'),
        'theme' => 'newsletterDialog',
        // additional javascript options for the dialog plugin
        'options' => array(
            'title' => _t('main', 'invite friend'),
            'autoOpen' => false,
            'modal' => true,
            'show' => 'fade',
            'hide' => 'fade',
            'width' => 700,
            'open' => 'js:function(event, ui) { $(".ui-widget-header").hide(); }',
        ),
    ));
    ?>
    <div class="newsletter-container">
        <div class="newsletter-dialog-header">
            <?php echo CHtml::image(Utils::imageUrl('logo.png')); ?>
        </div>
        <div class="newsletter-dialog-content">
            <h2><?php echo _t('main', 'invite your friends'); ?></h2>
            <p><?php echo _t('main', 'invite your friends now and when they make purchase 20, you get 200 points!'); ?></p>
            <div class="form">
                <?php echo CHtml::form(_url('user/invite')); ?>
                <div class="row">
                    <?php echo CHtml::label(_t('main', 'email:'), 'invitee'); ?>
                    <?php echo CHtml::textField('invitee'); ?>
                </div>
                <?php echo CHtml::submitButton(_t('main', 'invite')); ?>
                <?php echo CHtml::endForm(); ?>
            </div>
            <div class="cubes"><?php echo CHtml::image(Utils::imageUrl('coins.png')); ?></div>
        </div>
    </div>
    <div class="clear newsletter-footer">
        <?php echo CHtml::link(_t('main', 'click here to close this window'), '#', array('onclick' => '$("#invitebox").dialog("close");'));
        ?>
    </div>
    <?php $this->endWidget('zii.widgets.jui.CJuiDialog');
else :
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'newsletterbox',
        'themeUrl' => _bu('css'),
        'theme' => 'newsletterDialog',
        // additional javascript options for the dialog plugin
        'options' => array(
            'title' => _t('main', 'newsletter registration'),
            'autoOpen' => false,
            'modal' => true,
            'show' => 'fade',
            'hide' => 'fade',
            'width' => 700,
            'open' => 'js:function(event, ui) { $(".ui-widget-header").hide(); }',
        ),
    ));
    ?>
    <div class="newsletter-container">
        <div class="newsletter-dialog-header">
            <?php echo CHtml::image(Utils::imageUrl('logo.png')); ?>
        </div>
        <div class="newsletter-dialog-content">
            <h2><?php echo _t('main', 'newsletter registration'); ?></h2>
            <p><?php echo _t('main', 'register now to get updates on all deals with coupons up to 90%'); ?></p>
            <div class="form">
                <?php echo CHtml::form(_url('site/newsletter')); ?>
                <div class="row">
                    <?php echo CHtml::label(_t('main', 'email:'), 'email'); ?>
                    <?php echo CHtml::textField('email'); ?>
                </div>
                <?php echo CHtml::submitButton(_t('main', 'register')); ?>
                <?php echo CHtml::endForm(); ?>
            </div>
            <div class="cubes"><?php echo CHtml::image(Utils::imageUrl('cubes.png')); ?></div>
        </div>
    </div>
    <div class="clear newsletter-footer">
        <?php echo CHtml::link(_t('main', 'if you already subscribed to the newsletter, click here to close this window'), '#', array('onclick' => '$("#newsletterbox").dialog("close");'));
        ?>
    </div>
    <?php $this->endWidget('zii.widgets.jui.CJuiDialog');
endif; ?>