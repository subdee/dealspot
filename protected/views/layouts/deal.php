
<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
    <div class="span-7 banner prepend-top append-bottom">
        <?php echo CHtml::link('&larr;&nbsp;' . _t('deals', 'view all deals'), _url('site/index'), array('class' => 'a-deal-back')); ?>
    </div>
</div>
<div class="container">
    <?php echo $content; ?>
    <?php $this->widget('SidebarDeals'); ?>
</div>
<?php $this->endContent(); ?>