<div class="span-7 white-box">
    <div class="grey-box contact-boxes">
        <h2><?php echo _t('main', 'contact'); ?></h2>
        <div class="form">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'contact-form',
                    ));
            ?>

            <div style="float: right; font-size: 0.8em; font-style: italic;"><?php echo _t('main', 'fields with * are required'); ?></div>

            <div class="row">
                <?php echo $form->labelEx($contact, 'fullname'); ?>
                <?php echo $form->textField($contact, 'fullname'); ?>
                <?php echo $form->error($contact, 'fullname'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($contact, 'email'); ?>
                <?php echo $form->textField($contact, 'email'); ?>
                <?php echo $form->error($contact, 'email'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($contact, 'phone'); ?>
                <?php echo $form->textField($contact, 'phone'); ?>
                <?php echo $form->error($contact, 'phone'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($contact, 'reason'); ?>
                <?php echo $form->dropDownList($contact, 'reason', ContactForm::reasonsArray()); ?>
                <?php echo $form->error($contact, 'reason'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($contact, 'description'); ?>
                <?php echo $form->textArea($contact, 'description'); ?>
                <?php echo $form->error($contact, 'description'); ?>
            </div>
            <div class="login-button">
                <?php echo CHtml::image(Utils::imageUrl('send.png'), '', array('onclick' => '$("#contact-form").submit();')); ?>
            </div>
            <div class="clear"></div>
            <?php $this->endWidget(); ?>
        </div>
        <div class="right"><?php echo _t('main', 'for more info contact us at 123123123'); ?></div>
        <div class="clear"></div>
    </div>
    <div class="grey-box contact-boxes">
        <h2><?php echo _t('main', 'common questions'); ?></h2>
        <div class="white-box">1</div>
        <div class="white-box">2</div>
        <div class="white-box">3</div>
    </div>
</div>