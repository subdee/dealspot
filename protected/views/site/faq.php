<div class="span-7">
    <div class="white-box" id="faqs">
        <h2><?php echo _t('faq', 'faq'); ?></h2>
        <?php for($i = 1; $i <= 14; ++$i) : ?>
        <h3><?php echo $i; ?>. <?php echo _t('faq', 'faq question ' . $i); ?></h3>
        <div>
            <p><?php echo _t('faq', 'faq answer ' . $i); ?></p>
        </div>
        <?php endfor; ?>
    </div>
</div>
<script>
    $(function() {
        $('#faqs h3').each(function() {
            var tis = $(this), state = false, answer = tis.next('div').hide().css('height','auto').slideUp();
            tis.click(function() {
                state = !state;
                answer.slideToggle(state);
                tis.toggleClass('active',state);
            });
        });
    });
</script>