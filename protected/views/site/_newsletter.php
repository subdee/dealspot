<style type="text/css">
    body,td,th {
        color: #3C3C3C;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
    }
    a:link {
        color: #FFFFFF;
        text-decoration: none;
    }
    a:visited {
        color: #FFFFFF;
        text-decoration: none;
    }
    a:hover {
        text-decoration: none;
    }
    a:active {
        text-decoration: none;
    }

    .price{
        font-size:25px;
        font-weight: bold;
        padding-left:5px;
    }

    .discount{
        font-weight:bold;
        color:#646464;
    }
    .title{
        font-size:14px;
    }
    .discount1 {
        font-weight:bold;
        color:#646464;
    }
    .date{
        color:#FFFFFF;
    }
</style>
<table width="700" height="705" border="0" align="center" >
    <tr>
        <td width="595" height="701" valign="top" bgcolor="#3c3c3c">
            <table width="700" border="0">
                <tr>
                    <td width="607" bgcolor="#3c3c3c">
                        <table width="700" border="0">
                            <tr>
                                <td width="340">
                                    <div class="span-2 append-1 logo">
                                        <?php echo CHtml::link(CHtml::image(_app()->createAbsoluteUrl('images/logo.png')), _app()->createAbsoluteUrl('site/index')); ?>
                                    </div>
                                </td>
                                <td width="338" valign="top" align="right" style="color: white;">
                                    <?php echo Utils::date(date('Y-m-d'), array('time' => false, 'year' => true, 'monthAbbreviated' => false, 'dayAbbreviated' => false)); ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="696" border="0" align="center" bgcolor="#FFFFFF">
                <tr>
                    <td width="690" height="623" valign="top">
                        <table width="696" border="0">
                            <tr>
                                <td width="661" height="57" bgcolor="#CCCCCC"><?php echo _t('newsletter', 'some random text'); ?></td>
                            </tr>
                        </table>
                        <br />
                        <?php $i = 0; ?>
                        <?php foreach ($deals as $deal) : ?>
                            <?php if ($i % 2 == 0) : ?>
                                <table width="693" border="0">
                                    <tr>
                                    <?php endif; ?>
                                    <?php if ($i % 2 == 0) : ?>
                                        <td width="346" height="169"><table width="340" border="0">
                                                <tr>
                                                    <td colspan="2"><span class="title"><strong><?php echo $deal->name; ?></strong></span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><?php echo $deal->details->coupon_instructions; ?></td>
                                                </tr>
                                                <tr>
                                                    <td height="166" colspan="2" bgcolor="#CCCCCC"><?php echo CHtml::image(_app()->createAbsoluteUrl('images/deals/' . $deal->image), '', array('height' => 166)); ?></td>
                                                </tr>
                                                <tr>
                                                    <td width="174" bgcolor="#FFCC33">
                                                        <table width="174" border="0">
                                                            <tr>
                                                                <td width="94" height="28"><strong class="price"><?php echo Utils::currency($deal->coupon_price); ?></strong></td>
                                                                <td width="70"><?php echo CHtml::link(CHtml::image(_app()->createAbsoluteUrl('images/newsletter_button.png'), _t('newsletter', 'see it')), _app()->createAbsoluteUrl('deal/index', array('id' => $deal->id))); ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="156" bgcolor="#99bcdc">
                                                        <table width="156" border="0">
                                                            <tr>
                                                                <td width="77" class="discount1"><?php echo _t('newsletter', 'value'); ?>:</td>
                                                                <td width="69" class="discount1"><?php echo Utils::currency($deal->original_price); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="discount1"><?php echo _t('newsletter', 'discount'); ?>:</td>
                                                                <td class="discount1"><?php echo $deal->coupon_perc; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="discount1"><?php echo _t('newsletter', 'gain'); ?>:</td>
                                                                <td class="discount1"><?php echo Utils::currency($deal->original_price - $deal->coupon_price); ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table></td>
                                    <?php endif; ?>
                                    <?php if ($i % 2 == 1) : ?>
                                        <td width="337"><table width="340" border="0">
                                                <tr>
                                                    <td colspan="2"><span class="title"><strong><?php echo $deal->name; ?></strong></span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><?php echo $deal->details->coupon_instructions; ?></td>
                                                </tr>
                                                <tr>
                                                    <td height="166" colspan="2" bgcolor="#CCCCCC"><?php echo CHtml::image(_app()->createAbsoluteUrl('images/deals/' . $deal->image), '', array('height' => 166)); ?></td>
                                                </tr>
                                                <tr>
                                                    <td width="174" bgcolor="#FFCC33">
                                                        <table width="174" border="0">
                                                            <tr>
                                                                <td width="94" height="28"><strong class="price"><?php echo Utils::currency($deal->coupon_price); ?></strong></td>
                                                                <td width="70"><?php echo CHtml::link(CHtml::image(_app()->createAbsoluteUrl('images/newsletter_button.png'), _t('newsletter', 'see it')), _app()->createAbsoluteUrl('deal/index', array('id' => $deal->id))); ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="156" bgcolor="#99bcdc">
                                                        <table width="156" border="0">
                                                            <tr>
                                                                <td width="77" class="discount1"><?php echo _t('newsletter', 'value'); ?>:</td>
                                                                <td width="69" class="discount1"><?php echo Utils::currency($deal->original_price); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="discount1"><?php echo _t('newsletter', 'discount'); ?>:</td>
                                                                <td class="discount1"><?php echo $deal->coupon_perc; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="discount1"><?php echo _t('newsletter', 'gain'); ?>:</td>
                                                                <td class="discount1"><?php echo Utils::currency($deal->original_price - $deal->coupon_price); ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table></td>
                                    <?php endif; ?>
                                    <?php if ($i % 2 == 1) : ?>
                                    </tr>
                                </table>
                                <br />
                            <?php endif; ?>
                            <?php ++$i; ?>
                        <?php endforeach; ?>
                    </td>
                </tr>
            </table>
            <table width="700" border="0">
                <tr>
                    <td width="169"><?php echo CHtml::link(CHtml::image(_app()->createAbsoluteUrl('images/logo.png'), '', array('width' => 169, 'height' => 34)), _app()->createAbsoluteUrl('site/index')); ?></td>
                    <td width="521">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table width="700" border="0" align="center">
    <tr>
        <td><?php echo _t('newsletter', 'click {url}here{/url} to unsubscribe or unsubscribe in your profile if you are a member', array(
            '{url}' => CHtml::openTag('a', array('href' => _app()->createAbsoluteUrl('site/unsubscribe', array('email' => $email)))),
            '{/url}' => CHtml::closeTag('a'),
        )); ?></td>
    </tr>
</table>




