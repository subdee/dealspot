<div class="span-7 white-box">
    <div class="grey-box  contact-boxes">
        <h2><?php echo _t('main', 'contact form'); ?></h2>
        <div class="form">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'contact-form',
                    ));
            ?>

            <div style="float: right; font-size: 0.8em; font-style: italic;"><?php echo _t('main', 'fields with * are required'); ?></div>

            <div class="row">
                <?php echo $form->labelEx($company, 'fullname'); ?>
                <?php echo $form->textField($company, 'fullname'); ?>
                <?php echo $form->error($company, 'fullname'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($company, 'name'); ?>
                <?php echo $form->textField($company, 'name'); ?>
                <?php echo $form->error($company, 'name'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($company, 'phone'); ?>
                <?php echo $form->textField($company, 'phone'); ?>
                <?php echo $form->error($company, 'phone'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($company, 'website'); ?>
                <?php echo $form->textField($company, 'website'); ?>
                <?php echo $form->error($company, 'website'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($company, 'email'); ?>
                <?php echo $form->textField($company, 'email'); ?>
                <?php echo $form->error($company, 'email'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($company, 'address'); ?>
                <?php echo $form->textField($company, 'address'); ?>
                <?php echo $form->error($company, 'address'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($company, 'description'); ?>
                <?php echo $form->textArea($company, 'description'); ?>
                <?php echo $form->error($company, 'description'); ?>
            </div>
            <div class="login-button">
                <?php echo CHtml::image(Utils::imageUrl('send.png'), '', array('onclick' => '$("#contact-form").submit();')); ?>
            </div>
            <div class="clear"></div>
            <?php $this->endWidget(); ?>
        </div>
        <div class="right"><?php echo _t('main', 'for more info contact us at 123123123'); ?></div>
        <div class="clear"></div>
    </div>
    <div class="contact-boxes grey-box">
        <h2><?php echo _t('main', 'reasons to work with us'); ?></h2>
        <div class="white-box">
            <span style="font-size: 0.6em;"><?php echo _t('main', 'text for companies contact'); ?></span>
        </div>
    </div>
</div>