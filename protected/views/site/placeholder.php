<?php echo CHtml::image(Utils::imageUrl('placeholder/coming-soon-title.png')); ?>
<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2FDealspot.gr&amp;send=false&amp;layout=box_count&amp;width=450&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=90"
        scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:200px; height:70px; float: right;" allowTransparency="true"></iframe>
<div style="width: 1100px; margin: 30px auto;" class="grey-box">
    <div style="margin-left: 100px; float: left;">
        <div><?php echo CHtml::image(Utils::imageUrl('placeholder/days.png')); ?></div>
        <div style="background: url(<?php echo _url("images/placeholder/timer.png"); ?>) no-repeat; width: 160px; height: 123px; font-size: 5em;padding: 5px 0 0 40px;" id="days"></div>
    </div>
    <div style="margin-left: 40px; float: left;">
        <div><?php echo CHtml::image(Utils::imageUrl('placeholder/hours.png')); ?></div>
        <div style="background: url(<?php echo _url("images/placeholder/timer.png"); ?>) no-repeat; width: 160px; height: 123px; font-size: 5em;padding: 5px 0 0 40px;" id="hours"></div>
    </div>
    <div style="margin-left: 40px; float: left;">
        <div><?php echo CHtml::image(Utils::imageUrl('placeholder/mins.png')); ?></div>
        <div style="background: url(<?php echo _url("images/placeholder/timer.png"); ?>) no-repeat; width: 160px; height: 123px; font-size: 5em;padding: 5px 0 0 40px;" id="mins"></div>
    </div>
    <div style="margin-left: 40px; float: left;">
        <div><?php echo CHtml::image(Utils::imageUrl('placeholder/sec.png')); ?></div>
        <div style="background: url(<?php echo _url("images/placeholder/timer.png"); ?>) no-repeat; width: 160px; height: 123px; font-size: 5em;padding: 5px 0 0 40px;" id="secs"></div>
    </div>
    <div class="clear"></div>
</div>
<h1>Super Deal για τους πρώτους 200 χρήστες</h1>
<div class="grey-box" style="margin: 30px 0; width: 1100px;">
    <p>Οι πρώτοι 200 που θα κάνουν εγγραφή, θα λάβουν 200 Deal Points  για τις μελλοντικές τους αγορές.</p>
</div>
<div style="margin: 30px 0; width: 1100px;">
    <div style="width: 450px; margin-right: 100px; float: left;">
        <h1>Εγγραφή στο newsletter</h1>
        <div class="grey-box">
            <?php echo CHtml::image(Utils::imageUrl('placeholder/email-icon.png'), '', array('style' => 'float: left;')); ?>
            <div class="form placeholder-nl" style="text-align: right;">
                <?php echo CHtml::form(_url('site/newsletter')); ?>
                <?php echo CHtml::label(_t('main', 'email:'), 'email', array('style' => 'display: inline !important;')); ?>
                <?php echo CHtml::textField('email', '', array('style' => 'width: 220px;')); ?>
                <div style="margin-top: 30px;"><?php echo CHtml::linkButton(CHtml::image(Utils::imageUrl('send.png'))); ?></div>
                <?php echo CHtml::endForm(); ?>
                <p class="nl-error"></p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div style="width: 450px; margin-left: 100px; float: left;">
        <h1>Έχετε επιχείρηση;</h1>
        <div class="grey-box">
            <p>Συνεργαστείτε με το Dealspot και κερδίστε άμεσα και γρήγορα!</p>
            <div style="text-align: center;"><?php echo CHtml::image(Utils::imageUrl('case.png')); ?></div>
            <p>Για περισσότερες πληροφορίες επικοινωνήστε μαζί μας μέσω ηλεκτρονικού ταχυδρομείου στο <a href="mailto:sales@dealspot.gr">sales@dealspot.gr</a></p>
        </div>
    </div>
</div>
<input type="hidden"
       class="countdown"
       value="<?php echo strtotime("2012-12-07 00:00:00") - time(); ?>">
<script>
    $(function(){
        countDown();
    });
    function countDown() {
        $(".countdown").each(function(){
            var valore = $(this).val()-1;
            $(this).val(valore);

            secondi = valore;
            _secondi = secondi % 60;
            _minuti = ((secondi - _secondi) / 60) % 60;
            _ore = ((secondi - _secondi - _minuti * 60) / 3600) % 24;
            _days = (secondi - _secondi - _minuti * 60 - _ore * 24) / 86400;

            _days = Math.floor(_days);
            _days = _days < 10 ? "0"+_days : _days;
            _ore = _ore < 10 ? "0"+_ore : _ore;
            _minuti = _minuti < 10 ? "0"+_minuti : _minuti;
            _secondi = _secondi < 10 ? "0"+_secondi : _secondi;

            $("#days").html(_days);
            $("#hours").html(_ore);
            $("#mins").html(_minuti);
            $("#secs").html(_secondi);
        });
        setTimeout("countDown();", 1000);
    }
</script>
