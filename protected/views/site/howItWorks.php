<div class="span-7 white-box">
    <h2><?php echo _t('main', 'how it works title'); ?></h2>
    <p><?php echo _t('main', 'how it works subtitle'); ?></p>
    <div class="grey-box how-it-works-box">
        <?php echo CHtml::image(Utils::imageUrl('cart.png')); ?>
        <h2><?php echo _t('main', 'how it works title 1'); ?></h2>
        <p><?php echo _t('main', 'how it works subtitle 1'); ?></p>
    </div>
    <div class="grey-box how-it-works-box">
        <?php echo CHtml::image(Utils::imageUrl('megaphone.png')); ?>
        <h2><?php echo _t('main', 'how it works title 2'); ?></h2>
        <p><?php echo _t('main', 'how it works subtitle 2'); ?></p>
    </div>
    <div class="grey-box how-it-works-box">
        <?php echo CHtml::image(Utils::imageUrl('label.png')); ?>
        <h2><?php echo _t('main', 'how it works title 3'); ?></h2>
        <p><?php echo _t('main', 'how it works subtitle 3'); ?></p>
    </div>
</div>