<?php foreach ($deals as $deal) : ?>
    <div class="deal">
        <div class="deal-title left"><?php echo $deal->name; ?></div>
        <div class="deal-tag right"><div><strong>- <?php echo $deal->coupon_perc; ?></strong></div></div>
        <div class="clear"></div>
        <div class="deal-description left"><strong><?php echo _t('at') . '&nbsp;' . $deal->company->name; ?></strong></div>
        <div class="deal-price right"><strong><?php echo Utils::currency($deal->coupon_price); ?></strong></div>
        <div class="clear"></div>
        <div class="deal-image"><?php echo CHtml::image(Utils::imageUrl('deals/' . $deal->image)); ?></div>
        <div class="deal-time-wrapper">
            <div class="deal-time-bg"></div>
            <div class="deal-time-wrapper">
                <div class="deal-time">
                    <?php echo CHtml::image(Utils::imageUrl('clock25x25.png'), '', array('class' => 'clock')); ?>
                    <?php echo CHtml::image(Utils::imageUrl('sprite.png'), '', array('class' => 'dots')); ?>
                    <span class="countdown-time"><strong><?php $this->widget('CountDown', array('seconds' => $deal->time_left)); ?></strong></span>
                    <span class="deal-button"><?php echo CHtml::link(_t('Make a deal!'), _url('deal/index', array('id' => $deal->id))); ?></span>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>
