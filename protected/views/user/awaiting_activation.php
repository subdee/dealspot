<div class="activation-box span-7 last">
    <h3>
        <?php echo _t('user', 'check your email'); ?>
        <?php echo CHtml::image(Utils::imageUrl('green-check.png'), '', array('style' => 'vertical-align:middle;')); ?>
    </h3>
    <p style="font-weight: bold;"><?php echo _t('user', 'an email has been sent to your email to activate your account'); ?></p>
    <div class="grey-box">
        <p><?php echo _t('user', "in case you haven't received the email from dealspot.gr, check your spam folder"); ?></p>
        <div class="success">
            <?php echo _t('user', 'check your email now and click on the link to activate your account'); ?>
        </div>
        <div class="activation-image">
            <?php echo CHtml::image(Utils::imageUrl('activation-mail.png')); ?>
        </div>
        <div class="clear"></div>
    </div>
</div>