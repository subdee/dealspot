<h2><?php echo _t('profile', 'total points'); ?>:&nbsp;<span class="total-points"><?php echo _profile()->details->points; ?></span></h2>
<table class="custom-table">
    <thead>
        <tr>
            <th style="text-align: right; width: 10%;"><?php echo _t('profile', 'points'); ?></th>
            <th><?php echo _t('profile', 'reason'); ?></th>
            <th><?php echo _t('profile', 'transaction date'); ?></th>
            <th><?php echo _t('profile', 'points expire'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach (_profile()->pointTransactions as $trans) : ?>
        <tr style="background-color:<?php echo $trans->points > 0 ? '#d3ffd1' : 'pink'; ?>;">
            <td style="text-align: right;"><?php echo $trans->points; ?></td>
            <td><?php echo PointTransactionReason::toText($trans->reason); ?></td>
            <td><?php echo Utils::date($trans->transaction_date); ?></td>
            <td><?php echo Utils::dateLeft($trans->transaction_date, 'P8M'); ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
