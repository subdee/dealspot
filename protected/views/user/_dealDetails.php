<table class="custom-table">
    <thead>
        <tr>
            <th><?php echo _t('profile', 'coupon code'); ?></th>
            <th><?php echo _t('profile', 'name'); ?></th>
            <th><?php echo _t('profile', 'email'); ?></th>
            <th><?php echo _t('profile', 'date bought'); ?></th>
            <th><?php echo _t('profile', 'date used'); ?></th>
            <th><?php echo _t('profile', 'status'); ?></th>
            <th><?php echo _t('profile', 'manager'); ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($coupons as $coupon) : ?>
            <tr style="background-color:<?php echo $coupon->is_active ? '#d3ffd1' : 'pink'; ?>">
                <td><?php echo $coupon->coupon_code; ?></td>
                <td><?php echo $coupon->transaction->user->details->fullname; ?></td>
                <td><?php echo $coupon->transaction->user->email; ?></td>
                <td><?php echo Utils::date($coupon->transaction->transaction_date); ?></td>
                <td><?php echo $coupon->date_activated ? Utils::date($coupon->date_activated) : '-'; ?></td>
                <td><?php echo $coupon->is_active ? _t('profile', 'available') : _t('profile', 'used'); ?>
                </td>
                <td><?php echo isset($coupon->user) ? $coupon->user->username : '-'; ?></td>
                <td>
                    <?php
                    if (!isset($coupon->user)) :
                        echo CHtml::link(CHtml::image(Utils::imageUrl('gears.png')), _url('user/coupon', array('id' => $coupon->id)));
                    else :
                        echo CHtml::link(CHtml::image(Utils::imageUrl('gears.png')), _url('user/coupon', array('id' => $coupon->id)));
                    endif;
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
