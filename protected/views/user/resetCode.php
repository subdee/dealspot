<div class="white-box">
    <div class="form">
        <?php echo _user()->hasFlash('error') ? CHtml::tag('span', array('class' => 'errorMessage'), _user()->getFlash('error')) : ''; ?>

        <?php echo CHtml::form('changePassword'); ?>

        <h2><?php echo _t('user', 'enter the code you received from your email'); ?></h2>
        <div class="row">
            <?php echo CHtml::label(_t('user', 'enter the code'), 'reset_code', array('style' => 'font-size: 1.2em;')); ?>
            <?php echo CHtml::textField('reset_code'); ?>
        </div>
        <div class="row buttons">
            <?php echo CHtml::image(Utils::imageUrl('send.png'), '', array('onclick' => '$("form").submit();')); ?>
        </div>
        <p style="text-align: right;">
            <em>
                <?php echo CHtml::link(_t('user', "click here if you haven't received an email with the code"), _url('user/resetCode'));
                ?>
            </em>
        </p>
    </div>
</div>