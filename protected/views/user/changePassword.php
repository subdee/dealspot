<div class="white-box">
    <div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'user-password-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'focus' => array($user, 'password'),
            'action' => 'submitPassword'
                ));
        ?>

        <div class="row">
            <?php echo $form->labelEx($user, 'password'); ?>
            <?php echo $form->passwordField($user, 'password'); ?>
            <?php echo $form->error($user, 'password'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($user, 'password2'); ?>
            <?php echo $form->passwordField($user, 'password2'); ?>
            <?php echo $form->error($user, 'password2'); ?>
        </div>
        <?php echo $form->hiddenField($user, 'id'); ?>
        <div class="row buttons">
            <?php echo CHtml::submitButton(_t('user', 'send')); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>