<table class="custom-table">
    <thead>
        <tr>
            <th><?php echo _t('profile', 'deals'); ?></th>
            <th><?php echo _t('profile', 'date of sale'); ?></th>
            <th><?php echo _t('profile', 'expiration'); ?></th>
            <th><?php echo _t('profile', 'payment method'); ?></th>
            <th><?php echo _t('profile', 'amount paid'); ?></th>
            <th><?php echo _t('profile', 'code'); ?></th>
            <th><?php echo _t('profile', 'active'); ?></th>
            <th><?php echo _t('profile', 'print'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach (_profile()->transactions as $transaction) : ?>
            <?php foreach ($transaction->coupons as $coupon) : ?>
                <tr style="background-color:<?php echo $coupon->is_active ? '#d3ffd1' : 'pink'; ?>">
                    <td><?php echo $transaction->deal->name; ?></td>
                    <td><?php echo Utils::date($transaction->transaction_date); ?></td>
                    <td><?php echo Utils::date($transaction->deal->coupon_valid_to); ?></td>
                    <td><?php echo PaymentMethod::text($transaction->payment_method); ?></td>
                    <td><?php echo Utils::currency($transaction->deal->coupon_price); ?></td>
                    <td><?php echo $coupon->coupon_code; ?></td>
                    <td><?php echo $coupon->is_active ? _t('profile', 'yes') : _t('profile','no'); ?></td>
                    <td><?php echo CHtml::link(CHtml::image(Utils::imageUrl('print.png')),
                            $coupon->is_active ? _url('user/printCoupon', array('id' => $coupon->id)) : '#'); ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </tbody>
</table>
