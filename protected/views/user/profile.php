<div class="tabscontainer">
    <div class="tabs">
        <div class="tab first <?php echo $selected['details'] ? 'selected' : ''; ?>" id="tab_menu_1">
            <div class="link"><?php echo _t('profile', 'profile'); ?></div>
        </div>
        <div class="tab <?php echo $selected['deals'] ? 'selected' : ''; ?>" id="tab_menu_2">
            <div class="link"><?php echo _t('profile', 'deals'); ?></div>
        </div>
        <div class="tab <?php echo $selected['invites'] ? 'selected' : ''; ?>" id="tab_menu_3">
            <div class="link"><?php echo _t('profile', 'invites'); ?></div>
        </div>
        <div class="tab last <?php echo $selected['points'] ? 'selected' : ''; ?>" id="tab_menu_4">
            <div class="link"><?php echo _t('profile', 'points'); ?></div>
        </div>
        <?php if (User::hasRole(Role::CLIENT)) : ?>
            <div class="tab last <?php echo $selected['company'] ? 'selected' : ''; ?>" id="tab_menu_5">
                <div class="link"><?php echo _t('profile', 'company'); ?></div>
            </div>
        <?php endif; ?>
    </div>
    <div class="curvedContainer">
        <div class="tabcontent <?php echo $selected['details']; ?>" id="tab_content_1"><?php echo $details; ?></div>
        <div class="tabcontent <?php echo $selected['deals']; ?>" id="tab_content_2"><?php echo $deals; ?></div>
        <div class="tabcontent <?php echo $selected['invites']; ?>" id="tab_content_3"><?php echo $invites; ?></div>
        <div class="tabcontent <?php echo $selected['points']; ?>" id="tab_content_4"><?php echo $points; ?></div>
        <?php if (User::hasRole(Role::CLIENT)) : ?>
            <div class="tabcontent <?php echo $selected['company']; ?>" id="tab_content_5"><?php echo $company; ?></div>
        <?php endif; ?>
    </div>
</div>
<script>
    $(document).ready(function() {
 
        $(".tabs .tab[id^=tab_menu]").click(function() {
            var curMenu=$(this);
            
            $(".tabs .tab[id^=tab_menu]").removeClass("selected");
            curMenu.addClass("selected");
 
            var index=curMenu.attr("id").split("tab_menu_")[1];
            
            $(".curvedContainer .tabcontent").removeClass("show");
            
            $(".curvedContainer #tab_content_"+index).html('<div class="loader"><?php echo CHtml::image(Utils::imageUrl('loading.gif')); ?></div>');
            $(".curvedContainer #tab_content_"+index).addClass("show");
            
            switch (index) {
                case '1':
                    var url = '<?php echo _url('user/details'); ?>';
                    break;
                case '2':
                    var url = '<?php echo _url('user/deals'); ?>';
                    break;
                case '3':
                    var url = '<?php echo _url('user/invites'); ?>';
                    break;
                case '4':
                    var url = '<?php echo _url('user/points'); ?>';
                    break;
                case '5':
                    var url = '<?php echo _url('user/company'); ?>';
                    break;
                }
            
                $.ajax({
                    url: url,
                    cache: false,
                    success: function(response) {
                        $(".curvedContainer #tab_content_"+index).html(response);
                    }
                })
            });
        });
</script>