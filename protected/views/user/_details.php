<div class="details">
    <h2 style="color: #484743;"><?php echo _t('profile', 'account') . ':&nbsp;' . _profile()->username; ?></h2>
    <label><?php echo _t('profile', 'full name'); ?></label>
    <?php echo CHtml::tag('div', array(
        'id' => 'fullname',
        'class' => 'editable',
            ), _profile()->details->fullname
    ); ?>
    <label><?php echo _t('profile', 'email'); ?></label>
    <?php echo CHtml::tag('div', array(
        'id' => 'email',
        'class' => 'editable',
            ), _profile()->email
    ); ?>
    <label><?php echo _t('profile', 'gender'); ?></label>
    <?php echo CHtml::tag('div', array(
        'id' => 'gender',
        'class' => 'editable',
            ), Gender::toText(_profile()->details->gender)
    );?>
    <label><?php echo _t('profile', 'mobile'); ?></label>
    <?php echo CHtml::tag('div', array(
        'id' => 'mobile',
        'class' => 'editable',
            ), _profile()->details->mobile
    ); ?>
    <label><?php echo _t('profile', 'birthday'); ?></label>
    <?php
    echo CHtml::tag('div', array(
        'id' => 'birthdate',
//        'class' => 'editable',
            ), _profile()->details->birthdate ? : _t('profile', 'empty.birthdate')
    );

//    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//        'id' => 'dob_datepicker',
//        'name' => 'birthdate',
//        'value' => _profile()->details->birthdate,
//        'language' => _app()->language,
//        'options' => array(
//            'dateFormat' => 'yy-mm-dd',
//            'changeMonth' => true,
//            'changeYear' => true,
//            'yearRange' => '-100:+0',
//            'maxDate' => 'today',
//            'onSelect' => 'js:onDatepickerSelect',
//        ),
//        'htmlOptions' => array(
//            'style' => 'visibility: hidden',
//        ),
//    ));

    ?>
    <label><?php echo _t('profile', 'newsletter'); ?></label>
    <?php echo CHtml::tag('div', array(
        'id' => 'newsletter',
        'class' => 'editable',
            ), _profile()->details->newsletter ? _t('profile', 'yes') : _t('profile', 'no')
    ); ?>
    <hr/>
    <h2 style="color: #484743;"><?php echo _t('profile', 'password change'); ?></h2>
    <?php echo _user()->hasFlash('error') ? _user()->getFlash('error') : ''; ?>
    <?php echo CHtml::form(); ?>
    <?php echo CHtml::label(_t('profile', 'new password'), 'pass[new]'); ?>
    <?php echo CHtml::passwordField('pass[new]'); ?>
    <?php echo CHtml::label(_t('profile', 'repeat password'), 'pass[repeat]'); ?>
    <?php echo CHtml::passwordField('pass[repeat]'); ?>
    <?php echo CHtml::submitButton(_t('profile', 'save')); ?>
    <?php echo CHtml::endForm(); ?>
</div>
<script>
    var options = {
//        cssclass: 'inline-edit',
        indicator : '<?php echo CHtml::image(Utils::imageUrl('loading.gif')); ?>',
        submit  : 'OK',
        width: 400,
        autowidth: false,
        placeholder: '<?php echo _t('profile', 'edit text'); ?>',
        maxlength: 45,
        tooltip : '<?php echo _t('profile', 'editable tooltip'); ?>'
    };

    $('#fullname').editable('updateProfile', options);
    $('#gender').editable('updateProfile', {
        indicator : '<?php echo CHtml::image(Utils::imageUrl('loading.gif')); ?>',
        submit  : 'OK',
        type: 'select',
        data: '<?php echo json_encode(Gender::getGenders()); ?>',
        width: 400,
        autowidth: false,
        placeholder: '<?php echo _t('profile', 'edit text'); ?>',
        maxlength: 45,
        tooltip : '<?php echo _t('profile', 'editable tooltip'); ?>'
    });
    $('#newsletter').editable('updateProfile', {
        indicator : '<?php echo CHtml::image(Utils::imageUrl('loading.gif')); ?>',
        submit  : 'OK',
        type: 'checkbox',
        autowidth: false,
        checkbox: { trueValue: '<?php echo _t('profile', 'yes'); ?>', falseValue: '<?php echo _t('profile', 'no'); ?>' },
        tooltip : '<?php echo _t('profile', 'editable tooltip'); ?>'
    });
    $('#mobile').editable('updateProfile', options);
    $('#email').editable('updateProfile', options);
//    $('#birthdate').editable('updateProfile', options);
//    $('#birthdate').click(function(){
//        $('#dob_datepicker').datepicker('show');
//    });

    function onDatepickerSelect(dateText, inst) {
<?php
echo CHtml::ajax(array(
    'url' => array('user/updateProfile'),
    'data' => "js:{id : 'birthdate', value : $('#dob_datepicker').val()}",
    'type' => 'post',
    'update' => '#birthdate',
));
?>
    }
</script>