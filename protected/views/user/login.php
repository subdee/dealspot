<div class="already-reg">
    <h2><?php echo _t('user', 'already registered?'); ?></h2>
    <div class="form">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
                ));
        ?>

        <div class="username">
            <?php echo $form->label($model, 'username'); ?>
            <?php echo $form->textField($model, 'username'); ?>
            <?php echo $form->error($model, 'username'); ?>
        </div>
        <div class="password">
            <?php echo $form->label($model, 'password'); ?>
            <?php echo $form->passwordField($model, 'password'); ?>
            <?php echo $form->error($model, 'password'); ?>
        </div>
        <div class="login-button">
            <?php echo CHtml::imageButton(Utils::imageUrl('login.jpg'), array('onclick' => '$("#login-form").submit();', 'class' => 'image-button')); ?>
        </div>
        <div class="clear"></div>
        <div class="remember-me">
            <?php echo $form->checkBox($model, 'rememberMe'); ?>
            <?php echo $form->label($model, 'rememberMe'); ?>
            <?php echo $form->error($model, 'rememberMe'); ?>
        </div>
        <div class="forgot-password">
            <?php echo CHtml::link(_t('user', 'forgot password?'), _url('user/forgotPassword')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <hr/>
    <div class="socials">
        <?php $this->widget('application.modules.hybridauth.widgets.renderProviders'); ?>
    </div>
    <div class="clear"></div>
</div>
<?php $this->widget('RegistrationForm', array('user' => $user, 'userDetails' => $userDetails)); ?>
