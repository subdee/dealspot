<div class="activation-box span-7 last">
    <h3>
        <?php echo _t('user', 'your account has been activated'); ?>
        <?php echo CHtml::image(Utils::imageUrl('green-check.png'), '', array('style' => 'vertical-align:middle;')); ?>
    </h3>
    <p style="font-weight: bold;"><?php echo _t('user', 'thank you for choosing dealspot'); ?></p>
    <div class="click-to-login">
        <h3><?php echo _t('user', 'now you can login'); ?></h3>
        <div><?php echo CHtml::link(CHtml::image(Utils::imageUrl('go-to-login-button.png')), _url('user/login')); ?></div>
    </div>
</div>
