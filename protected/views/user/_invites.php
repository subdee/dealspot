<table class="custom-table">
    <thead>
        <tr>
            <th><?php echo _t('profile', 'tableheader.invitee'); ?></th>
            <th><?php echo _t('profile', 'tableheader.invite status'); ?></th>
            <th><?php echo _t('profile', 'tableheader.invited on'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach (_profile()->invites as $invite) : ?>
        <tr>
            <td><?php echo $invite->invited_email; ?></td>
            <td><?php echo InviteStatus::toText($invite->status); ?></td>
            <td><?php echo Utils::date($invite->timestamp); ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
