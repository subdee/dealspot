<div class="white-box">
    <div class="form">
        <?php echo _user()->hasFlash('error') ? CHtml::tag('span', array('class' => 'errorMessage'), _user()->getFlash('error')) : ''; ?>

        <?php echo CHtml::form('resetCode'); ?>

        <h2><?php echo _t('user', 'enter your email that you used to register.'); ?></h2>
        <div class="info">
            <?php echo _t('user', 'if you registered with your facebook account, provide the email that you use for your facebook account'); ?>
        </div>
        <div style="text-align: center;"><?php echo _t('user', 'you will receive a temporary code in your email'); ?></div>
        <div class="row">
            <?php echo CHtml::label(_t('user', 'your email'), 'email', array('style' => 'font-size: 1.2em;')); ?>
            <?php echo CHtml::textField('email'); ?>
        </div>
        <div class="row buttons">
            <?php echo CHtml::image(Utils::imageUrl('send.png'), '', array('onclick' => '$("form").submit();')); ?>
        </div>
        <p style="text-align: right;">
            <em>
                <?php echo CHtml::link(_t('user', "click here if you've already requested and received a code"), _url('user/resetCode'));
                ?>
            </em>
        </p>
    </div>
</div>