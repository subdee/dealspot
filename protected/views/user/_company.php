<h2><?php echo _t('profile', 'my company'); ?></h2>
<div class="my-company">
    <?php if ($id) :
        $deal = Deal::model()->findByPk($id);
        $this->renderPartial('_dealDetails', array('coupons' => $deal->allCoupons));
    else : ?>
    <table class="custom-table">
        <thead>
            <tr>
                <th><?php echo _t('profile', 'deal'); ?></th>
                <th><?php echo _t('profile', 'status'); ?></th>
                <th><?php echo _t('profile', 'start date'); ?></th>
                <th><?php echo _t('profile', 'end date'); ?></th>
                <th><?php echo _t('profile', 'sales'); ?></th>
                <th><?php echo _t('profile', 'amount'); ?></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach (_profile()->company->deals as $deal) : ?>
                <tr style="background-color:<?php echo $deal->isActive() ? '#d3ffd1' : 'pink'; ?>">
                    <td><?php echo $deal->name; ?></td>
                    <td><?php echo $deal->isActive() ? _t('profile', 'active') : _t('profile', 'inactive'); ?></td>
                    <td><?php echo Utils::date($deal->valid_from); ?></td>
                    <td><?php echo Utils::date($deal->valid_to); ?></td>
                    <td><span class="blue"><?php echo count($deal->transactions); ?></span></td>
                    <td><span class="blue"><?php echo Utils::currency(Transaction::getSalesByDeal($deal->id)); ?></span></td>
                    <td><?php echo CHtml::linkButton(CHtml::image(Utils::imageUrl('details.png')), array('class' => 'details', 'id' => $deal->id)); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>
</div>
<script>
    $(function(){
        $(".details").click(function(){
            var deal = $(this).attr("id");
            $.ajax({
                url: '<?php echo _url('user/dealDetails'); ?>?id=' + deal,
                type: 'get',
                success: function(data) {
                    $(".my-company").html(data);
                }
            })
            return false;
        })
    })
</script>
