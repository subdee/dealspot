<div class="span-5">
    <div class="a-deal-content-box append-bottom">
        <div class="append-bottom last a-deal-instructions"><?php echo $deal->details->coupon_instructions; ?></div>
        <div class="container">
            <div class="span-2">
                <div class="a-deal-image">
                    <div class="a-deal-image-text"><?php echo Utils::currency($deal->coupon_price); ?></div>
                    <?php if ($deal->time_left) : ?>
                        <div class="a-deal-image-button"><?php echo CHtml::link(_t('deals', 'button.make a deal'), _url('shop/addToCart', array('id' => $deal->id)), array('class' => 'a-deal-buy-button')); ?></div>
                    <?php else : ?>
                        <div class="a-deal-image-button"><?php echo CHtml::link(_t('deals', 'button.make a deal'), _url('shop/addToCart', array('id' => $deal->id)), array('class' => 'hide')); ?></div>
                    <?php endif; ?>
                </div>
                <div class="a-deal-details">
                    <div class="a-deal-details-item"><?php echo _t('deals', 'value'); ?><br/><?php echo Utils::currency($deal->original_price); ?></div>
                    <div class="a-deal-details-item"><?php echo _t('deals', 'discount'); ?><br/><?php echo $deal->coupon_perc; ?></div>
                    <div class="a-deal-details-item no-right-border"><?php echo _t('deals', 'profit'); ?><br/><?php echo Utils::currency($deal->original_price - $deal->coupon_price); ?></div>
                    <?php if ($deal->time_left) : ?>
                        <div class="a-deal-details-gift"><?php echo CHtml::link(_t('deals', 'button.send as gift'), _url('shop/addToCart', array('id' => $deal->id, 'gift' => true))); ?></div>
                    <?php else : ?>
                        <div class="a-deal-details-gift"><?php echo _t('deals', 'send as gift'); ?></div>
                    <?php endif; ?>
                </div>
                <div class="a-deal-countdown"><?php $this->widget('CountDown', array('time_left' => $deal->time_left, 'theme' => 'deal')); ?></div>
                <div class="clear"></div>
                <div class="a-deal-bought">
                    <div class="a-deal-bought-text">
                        <span><?php // echo $deal->coupons_bought; ?></span>
                        <?php echo _t('deals', '<span>{n}</span> purchase|<span>{n}</span> purchases', array($deal->coupons_bought)); ?>
                    </div>
                    <?php if ($deal->coupons_bought >= $deal->min_required) : ?>
                        <div class="a-deal-bought-active">
                            <?php echo CHtml::image(Utils::imageUrl('active-check.png')); ?>
                            <?php echo _t('deals', 'deal is active'); ?>
                        </div>
                    <?php else : ?>
                        <div class="a-deal-bought-inactive">
                            <?php echo CHtml::image(Utils::imageUrl('inactive-check.png')); ?>
                            <?php echo _t('deals', 'deal is not yet active'); ?>
                            <p><?php echo _t('deals', 'there must be {number} purchases to be active', array('{number}' => $deal->min_required)); ?></p>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="a-deal-social">
                    <p><?php echo _t('deals', 'share it'); ?></p>
                    <?php $this->widget('application.extensions.WSocialButton', array('style' => 'box')); ?>
                </div>
            </div>
            <div class="span-3 append-bottom last a-deal-content">
                <div><?php echo CHtml::image(Utils::imageUrl('deals/' . $deal->side_image)); ?></div>
                <div>
                    <h2><?php echo _t('deals', 'terms of use link'); ?></h2>
                    <p><?php echo $deal->details->terms_of_use; ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="a-deal-content-box append-bottom">
        <div class="container">
            <div class="span-2">
                <div class="company-image">
                    <?php echo CHtml::image(Utils::imageUrl('companies/' . $deal->company->image)); ?>
                </div>
                <div class="company-name-details">
                    <span class="large"><?php echo $deal->company->name; ?></span>
                    <br/>
                    <?php echo $deal->company->address; ?>
                    <br/>
                    <?php echo $deal->company->phone; ?>
                    <br/>
                    <?php echo $deal->company->fax; ?>
                    <br/>
                    <?php echo $deal->company->mobile; ?>
                    <br/>
                    <?php echo $deal->company->email; ?>
                </div>
                <div class="google-maps">
                    <?php $map->renderMap(); ?>
                </div>
            </div>
            <div class="span-3 append-bottom last a-deal-content">
                <hr/>
                <h3><?php echo $deal->company->name; ?></h3>
                <p>
                    <?php echo $deal->company->description; ?>
                </p>
                <p>
                    <?php echo CHtml::link($deal->company->website, $deal->company->website, array('target' => '_blank')); ?>
                </p>
            </div>
        </div>
    </div>
</div>