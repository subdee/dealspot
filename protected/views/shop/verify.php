<div class="cart">
    <div class="progress-bar">
        <?php echo CHtml::image(Utils::imageUrl('bar2.png')); ?>
    </div>
    <?php if ($cart) : ?>
        <div class="cart-content-box-container long">
            <h3><?php echo _t('shop', 'your deal'); ?></h3>
            <div class="cart-content-box">
                <?php echo $cart->deal->details->coupon_instructions; ?>
            </div>
        </div>
        <div class="cart-content-box-container short">
            <h3><?php echo _t('shop', 'payment method'); ?></h3>
            <div class="cart-content-box" style="text-align: center;">
                <?php echo CHtml::image($response['image']); ?>
            </div>
        </div>
        <div class="cart-content-box-container short-last">
            <h3><?php echo _t('shop', 'deal cost'); ?></h3>
            <div class="cart-content-box">
                <div><?php echo _t('shop', 'amount'); ?>:<span class="text-right right"><?php echo $cart->amount; ?></span></div>
                <div><?php echo _t('shop', 'price per unit'); ?>:
                    <span class="text-right right"><?php echo Utils::currency($cart->deal->coupon_price); ?></span></div>
                <hr/>
                <div><?php echo _t('shop', 'total'); ?>:<span class="text-right right" style="font-size: 2em; margin-top: -13px;">
                        <?php echo Utils::currency($cart->total); ?></span></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="cart-content-box-container full last">
            <div class="cart-content-box">
                <div class="span-5"><?php echo $response['text']; ?></div>
                <div class="span-1 last">
                    <form action="<?php echo _url('shop/process'); ?>" method="post">
                        <?php if (isset($response['TOKEN'])) : ?>
                            <input type="hidden" name="token" value="<?php echo $response['TOKEN']; ?>">
                        <?php endif; ?>
                        <?php if (isset($response['PAYERID'])) : ?>
                            <input type="hidden" name="payerID" value="<?php echo $response['PAYERID']; ?>">
                        <?php endif; ?>
                        <input type="hidden" name="amount" value="<?php echo $cart->total; ?>">
                        <input type="image" src="<?php echo Utils::imageUrl('confirm-button.png'); ?>" value="<?php echo _t('shop', 'click to complete payment'); ?>">
                    </form>
                </div>
            </div>
        </div>
        <div><?php echo CHtml::link(_t('shop', 'cancel'), _url('site/index')); ?></div>
        <div class="clearfix"></div>
    <?php else : ?>
        <?php echo _t('shop', 'no items in cart'); ?>
    <?php endif; ?>
</div>