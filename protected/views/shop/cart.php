<div class="cart">
    <div class="progress-bar">
        <?php echo CHtml::image(Utils::imageUrl('bar1.png')); ?>
    </div>
    <?php if (_user()->hasFlash('notavailable')) : ?>
        <div class="errorSummary"><?php echo _user()->getFlash('notavailable'); ?></div>
    <?php elseif ($cart) : ?>
        <?php if (_user()->hasFlash('nopoints')) : ?>
            <div class="errorSummary"><?php echo _user()->getFlash('nopoints'); ?></div>
        <?php endif; ?>
        <div class="cart-header cart-item">
            <div class="name"><?php echo _t('shop', 'item'); ?></div>
            <div class="amount text-right"><?php echo _t('shop', 'amount'); ?></div>
            <div class="item-price"><?php echo _t('shop', 'item price'); ?></div>
            <div class="price"><?php echo _t('shop', 'price'); ?></div>
        </div>
        <div class="clearfix"></div>
        <div class="cart-items">
            <div class="cart-item">
                <div class="name"><?php echo $cart->deal->name; ?>
                    <?php if (isset($cart->deal->details)) : ?>
                        -
                        <span><?php echo $cart->deal->details->subtitle; ?></span>
                    <?php endif; ?>
                </div>
                <div class="amount" style="text-align: right">
                    <?php if ($gift) : ?>
                        <?php
                        echo $cart->amount;
                        ?>
                    <?php else : ?>
                        <?php
                        echo CHtml::dropDownList('amount', $cart->amount, $cart->deal->getCouponsAvailableArray(), array('id' => 'amount', 'class' => 'right amount-dropdown'));
                        ?>
                    <?php endif; ?>
                </div>
                <div class="item-price"><?php echo Utils::currency($cart->deal->coupon_price); ?></div>
                <div class="price"><?php echo Utils::currency($cart->total); ?></div>
                <?php echo CHtml::hiddenField('id', $cart->deal->id, array('id' => 'itemID')); ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="recipients-block">
            <?php if ($gift) : ?>
                <?php $this->renderPartial('_gift', array('papers' => _app()->wagclient->getPapers(), 'gift' => $gift)); ?>
            <?php else : ?>
                <?php $this->renderPartial('_recipientFields', array('amount' => $cart->amount, 'name' => _profile()->details->fullname)); ?>
            <?php endif; ?>
        </div>
        <div class="payment-methods">
            <ul>
                <li>
                    <?php echo CHtml::image(Utils::imageUrl('number-1.png')); ?>
                    <span><?php echo _t('shop', 'pay by bank'); ?></span>
                    <div class="payment-details">
                        <div class="payment-description">
                            <?php echo _t('shop', 'description for bank payment'); ?>
                        </div>
                        <div class="payment-image">
                            <?php echo CHtml::image(Utils::imageUrl('bank.png')); ?>
                        </div>
                        <div class="payment-button">
                            <?php echo CHtml::link(Chtml::image(Utils::imageUrl('button.jpg')), '#', array('class' => 'payment-button', 'type' => PaymentMethod::BANK)); ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                </li>
                <li>
                    <?php echo CHtml::image(Utils::imageUrl('number-2.png')); ?>
                    <span><?php echo _t('shop', 'pay by hellaspay'); ?></span>
                    <div class="payment-details">
                        <div class="payment-description">
                            <?php echo _t('shop', 'description for hellaspay payment'); ?>
                        </div>
                        <div class="payment-image">
                            <?php echo CHtml::image(Utils::imageUrl('hellaspay.png')); ?>
                        </div>
                        <div class="payment-button">
                            <?php echo CHtml::link(Chtml::image(Utils::imageUrl('button.jpg')), '#', array('class' => 'payment-button', 'type' => PaymentMethod::HELLASPAY)); ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                </li>
                <li>
                    <?php echo CHtml::image(Utils::imageUrl('number-3.png')); ?>
                    <span><?php echo _t('shop', 'pay by paypal'); ?></span>
                    <div class="payment-details">
                        <div class="payment-description">
                            <?php echo _t('shop', 'description for paypal payment'); ?>
                        </div>
                        <div class="payment-image">
                            <?php echo CHtml::image(Utils::imageUrl('paypal.png')); ?>
                        </div>
                        <div class="payment-button">
                            <?php echo CHtml::link(Chtml::image(Utils::imageUrl('button.jpg')), '#', array('class' => 'payment-button', 'type' => PaymentMethod::PAYPAL)); ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                </li>
                <li>
                    <?php echo CHtml::image(Utils::imageUrl('number-4.png')); ?>
                    <span><?php echo _t('shop', 'pay by points'); ?></span>
                    <div class="payment-details">
                        <div class="payment-description">
                            <?php echo _t('shop', 'description for points payment'); ?>
                        </div>
                        <div class="payment-image">
                            <?php echo CHtml::image(Utils::imageUrl('points.png')); ?>
                        </div>
                        <div class="payment-button">
                            <?php echo CHtml::link(Chtml::image(Utils::imageUrl('button.jpg')), '#', array('class' => 'payment-button', 'type' => PaymentMethod::POINTS)); ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                </li>
            </ul>
        </div>
    <?php else : ?>
        <?php echo _t('shop', 'no items in cart'); ?>
    <?php endif; ?>
    <div><?php echo CHtml::link(_t('shop', 'cancel'), _url('site/index')); ?></div>
    <div class="clearfix"></div>
</div>
<script>
    $(document).ready(function() {
        $("#amount").live('change', function() {
            $.ajax({
                url: '<?php echo _url('shop/increaseAmount'); ?>',
                data: {id: $(this).parent().parent().children("#itemID").val(), amount: $(this).val()},
                type: 'get',
                success: $.proxy(function(data) {
                    if (data.status) {
                        $(this).parent().parent().children(".price").fadeOut(200, function() {
                            $(this).html(data.price).fadeIn(200);
                        });
                        $(".recipients-block").html(data.recipients);
                    }
                }, this)
            });
        });

        $(".payment-button").click(function(){
            $("#type").val($(this).attr("type"));
            $("#cart-form").submit();
            return false;
        })
    })
</script>
