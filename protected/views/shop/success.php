<div class="cart">
    <div class="progress-bar">
        <?php echo CHtml::image(Utils::imageUrl('bar3.png')); ?>
    </div>
    <h3>
        <?php echo _t('shop', 'your order is succesfully completed.'); ?>
        <?php echo CHtml::image(Utils::imageUrl('green-check.png'), '', array('style' => 'vertical-align:middle;')); ?>
    </h3>
    <div class="grey-box">
        <?php echo _user()->hasFlash('gift-error') ? '<div class="error">' . _user()->getFlash('gift-error') . '</div>' : ''; ?>
        <?php echo $response; ?>
        <div style="text-align: center;"><?php echo _t('shop', 'for any questions you can contact the dealspot team to guide you'); ?></div>
        <div style="text-align: center;"><?php echo _t('shop', 'thank you for choosing dealspot'); ?></div>
        <div style="text-align: right;">
            <?php echo CHtml::link(_t('shop', 'go back'), _url('site/index')); ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://gr.linkwi.se/delivery/lwc/lwc.js"></script>
<script type="text/javascript">
    Linkwise.load_action("258", "<?php echo $id; ?>", "1::<?php echo $amount; ?>", "", "pending");
</script>
<noscript>
<img src="https://gr.linkwi.se/delivery/acl.php?cam_id=258&amp;trans_id=<?php echo $id; ?>&amp;sale_amount=1::<?php echo $amount; ?>&amp;adv_subid=&amp;status=pending" style="width:0px;height:0px;"/>
</noscript>
