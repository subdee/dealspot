<?php echo CHtml::form('pay', 'post', array('id' => 'cart-form')); ?>
    <?php echo _user()->hasFlash('error') ? CHtml::tag('span', array('class' => 'error'), _user()->getFlash('error')) : ''; ?>
    <div class="recipient1">
        <?php echo CHtml::label(_t('shop', 'recipient {number}: ', array('{number}' => 1)), 'recipients[]'); ?>
        <?php echo CHtml::textField('recipients[]', $name); ?>
    </div>
    <?php for ($i = 2; $i <= $amount; ++$i) : ?>
        <div class="recipient<?php echo $i; ?>">
            <?php echo CHtml::label(_t('shop', 'recipient {number}: ', array('{number}' => $i)), 'recipients[]'); ?>
            <?php echo CHtml::textField('recipients[]'); ?>
        </div>
    <?php endfor; ?>
    <?php if ($amount > 1) : ?>
        <div>
            <?php echo CHtml::checkBox('sameRecipient'); ?>
            <?php echo CHtml::label(_t('shop', 'recipient 1 for all coupons'), 'sameRecipient'); ?>
        </div>
    <?php endif; ?>
    <?php echo CHtml::hiddenField('type'); ?>
<?php echo CHtml::endForm(); ?>