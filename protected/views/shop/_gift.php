<?php
Utils::registerCssFile('mobiscroll-2.3.custom.min');
Utils::registerJsFile('mobiscroll-2.3.custom.min');
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
                'id' => 'cart-form',
                'action' => 'pay',
                    ));
?>
<div class="gift">
    <?php echo $form->errorSummary($gift); ?>
    <div class="left">
        <?php echo $form->label($gift, 'fullname'); ?>
        <?php echo $form->textField($gift, 'fullname'); ?>
        <?php echo $form->label($gift, 'email'); ?>
        <?php echo $form->textField($gift, 'email'); ?>
        <div class="clear"></div>
        <div>
            <?php echo $form->label($gift, 'paper'); ?>
            <br/>
            <?php $i = 1; ?>
            <?php foreach ($papers as $paper) : ?>
                <?php echo CHtml::image($paper->url, '', array('id' => $paper->id, 'class' => $i >= count($papers) ? 'paper last' : 'paper')); ?>
                <?php ++$i; ?>
            <?php endforeach; ?>
            <?php echo $form->hiddenField($gift, 'paper'); ?>
        </div>
    </div>
    <div class="right">
        <?php echo $form->label($gift, 'unwrap_time'); ?>
        <?php echo $form->textField($gift, 'unwrap_time'); ?>
        <?php echo $form->label($gift, 'personal_message'); ?>
        <?php echo $form->textArea($gift, 'personal_message', array('style' => 'width: 220px; height: 100px;')); ?>
        
        <?php echo _app()->wagclient->getTooltip(); ?>
    </div>
    <div class="clear"></div>
</div>
<?php echo CHtml::hiddenField('type'); ?>
<?php $this->endWidget(); ?>
<script>
    $(function(){
        $(".paper").click(function(){
            $(this).siblings().removeClass('selected');
            $(this).addClass('selected');
            $('#GiftForm_paper').val($(this).attr("id"));
        });
        
        var now = new Date();
        
        (function ($) {
            $.mobiscroll.i18n.el = $.extend($.mobiscroll.i18n.el, {
                setText: 'OK',
                cancelText: 'Αναίρεση'
            });
        })(jQuery);
        
        (function ($) {
            $.mobiscroll.i18n.el = $.extend($.mobiscroll.i18n.el, {
                dateFormat: 'dd-mm-yy',
                dateOrder: 'ddmmyy',
                dayNames: ['Κυριακή', 'Δευτέρα', 'Τρίτη', 'Τετάρτη', 'Πέμπτη', 'Παρασκευή', 'Σάββατο'],
                dayNamesShort: ['Κυ', 'Δε', 'Τρ', 'Τε', 'Πε', 'Πα', 'Σα'],
                dayText: 'Ημέρα',
                hourText: 'Ώρα',
                minuteText: 'Λεπτό',
                monthNames: ['ιανουάριος', 'Φεβρουάριος', 'Μάρτιος', 'Απρίλιος', 'Μάιος', 'Ιούνιος', 'Ιούλιος', 'Αύγουστος', 'Σεπτέμβριος', 'Οκτώβριος', 'Νοέμβριος', 'Δεκέμβριος'],
                monthNamesShort: ['Ιαν', 'Φεβ', 'Μαρ', 'Απρ', 'Μαι', 'Ιον', 'Ιολ', 'Αυγ', 'Σεπ', 'Οκτ', 'Νοε', 'Δεκ'],
                monthText: 'Μήνας',
                secText: 'Δευτ/λεπτο',
                timeFormat: 'HH:ii',
                timeWheels: 'HHii',
                yearText: 'Έτος'
            });
        })(jQuery);

        $('#GiftForm_unwrap_time').scroller({
            preset: 'datetime',
            minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes() + 5),
            display: 'bubble',
            animate: 'fade',
            mode: 'clickpick',
            lang: '<?php echo _app()->language; ?>'
        });
    })
</script>