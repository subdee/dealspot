<style type="text/css">
    body,td,th {
        color: #3C3C3C;
        font-family: Arial, Helvetica, sans-serif;
    }
    a:link {
        color: #FFFFFF;
        text-decoration: none;
    }
    a:visited {
        color: #FFFFFF;
        text-decoration: none;
    }
    a:hover {
        text-decoration: none;
    }
    a:active {
        text-decoration: none;
    }
</style>
<table width="601" height="705" border="0" align="center" >
    <tr>
        <td width="595" height="701" valign="top" bgcolor="#3c3c3c"><table width="601" border="0">
                <tr>
                    <td width="590" bgcolor="#3c3c3c"><table width="601" border="0">
                            <tr>
                                <td width="343"><div class="span-2 append-1 logo"><?php echo CHtml::link(CHtml::image(_app()->createAbsoluteUrl('images/logo.png')), _app()->createAbsoluteUrl('site/index')); ?></div></td>
                                <td width="248" valign="top" align="right" style="color: white;"><?php echo Utils::date(date('Y-m-d'), array('time' => false, 'year' => true, 'monthAbbreviated' => false, 'dayAbbreviated' => false)); ?></td>
                            </tr>
                        </table></td>
                </tr>
            </table>
            <table width="595" border="0" align="center" bgcolor="#FFFFFF">
                <tr>
                    <td width="595" height="623"><table width="591" border="0">
                            <tr>
                                <td width="203"><table width="200" border="0">
                                        <tr>
                                            <td><strong><?php echo _t('coupon', 'coupon number'); ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td height="43"><strong><?php echo $coupon->coupon_code; ?></strong></td>
                                        </tr>
                                    </table></td>
                                <td width="378"><table width="310" border="0" align="right">
                                        <tr>
                                            <td width="124" align="right"><strong><?php echo _t('coupon', 'full name'); ?>:</strong></td>
                                            <td width="176" align="right"><?php echo $coupon->name; ?></td>
                                        </tr>
                                        <tr>
                                            <td align="right"><strong><?php echo _t('coupon', 'expiration date'); ?>:</strong></td>
                                            <td align="right"><?php echo $coupon->transaction->deal->coupon_valid_to; ?></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table>
                        <p><br>
                            <strong><?php echo _t('coupon', 'your deal'); ?></strong><br>
                            <br>
                            <?php echo $coupon->transaction->deal->details->coupon_instructions; ?><br>
                            <br>
                            <br>
                            <br>
                            <strong><?php echo _t('coupon', 'location directions'); ?></strong></p>
                        <table width="525" border="0">
                            <tr>
                                <td width="294" height="247" bgcolor="#999999"><img src="http://maps.googleapis.com/maps/api/staticmap?center=
                                 <?php echo urlencode($coupon->transaction->deal->company->coordinates); ?>&markers=size:mid|
                                 <?php echo urlencode($coupon->transaction->deal->company->coordinates); ?>&zoom=14&size=300x250&maptype=roadmap&sensor=false"></td>
                                <td width="215" valign="top"><?php echo $coupon->transaction->deal->company->name; ?><br>
                                    <?php echo $coupon->transaction->deal->company->address; ?><br>
                                    <?php echo $coupon->transaction->deal->company->phone; ?><br>
                                    <?php echo $coupon->transaction->deal->company->email; ?></td>
                            </tr>
                        </table>
                        <p><?php echo _t('coupon', 'thank you for choosing dealspot'); ?><br>
                        </p></td>
                </tr>
            </table>
            <table width="600" border="0">
                <tr>
                    <td width="169"><?php echo CHtml::link(CHtml::image(_app()->createAbsoluteUrl('images/logo.png'), '', array('width' => 169, 'height' => 34)), _app()->createAbsoluteUrl('site/index')); ?></td>
                    <td width="415">&nbsp;</td>
                </tr>
            </table></td>
    </tr>
</table>
