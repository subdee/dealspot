<?php

/**
 * This is the model class for table "coupon".
 *
 * The followings are the available columns in table 'coupon':
 * @property integer $id
 * @property integer $transaction_id
 * @property string $name
 * @property string $coupon_code
 * @property string $sent_on
 *
 * The followings are the available model relations:
 * @property Transaction $transaction
 */
class Coupon extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Coupon the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'coupon';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('transaction_id, name', 'required'),
            array('transaction_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('coupon_code', 'length', 'max' => 14),
            array('coupon_code', 'unique'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, transaction_id, name, coupon_code, sent_on', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'transaction' => array(self::BELONGS_TO, 'Transaction', 'transaction_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'transaction_id' => 'Transaction',
            'name' => 'Name',
            'coupon_code' => 'Coupon Code',
            'sent_on' => 'Sent On',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('name', $this->name, true);
        $criteria->compare('coupon_code', $this->coupon_code, true);
        $criteria->compare('transaction_id', $this->transaction_id);

        $criteria->with = 'transaction';
        $criteria->order = 'transaction.transaction_date asc';
        $criteria->addCondition('transaction.status = :status');
        $criteria->params[':status'] = Transaction::COMPLETED;

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    protected function beforeSave() {
        $this->createCode();
        return parent::beforeSave();
    }

    private function createCode() {
        $code = substr(md5($this->transaction->deal->company->name), 12, 2);
        $code .= substr(md5($this->transaction->deal->name), 3, 3);
        $code .= rand(100000000, 999999999);

        $this->coupon_code = strtoupper($code);

        if (!$this->validate('coupon_code', true))
            $this->createCode();
    }

    public function send() {
        if (isset(_app()->session['gift'])) {
            $gift = new GiftForm;
            $gift->attributes = _app()->session['gift'];
            unset(_app()->session['gift']);

            $mPDF1 = Yii::app()->ePdf->mpdf();
            $mPDF1->WriteHTML(_app()->controller->renderPartial('//shop/_coupon', array('coupon' => $this), true));
            $mPDF1->Output(_bp("/coupons/{$this->coupon_code}.pdf"), 'F');

            if ($gift->send($this)) {
                $this->sent_on = date(DATE_ISO8601);
                return $this->save(false);
            } else {
                _user()->setFlash('gift-error', _t('gift', 'unfortunately your gift was not sent. you can send the gift manually by downloading the coupon from your profile. sorry for the inconvenience.'));
            }
        } else {
            if (Utils::sendCouponEmail($this->transaction->user->email, _t('coupon', 'coupon for {deal}', array('{deal}' => $this->transaction->deal->name)), $this)) {
                $this->sent_on = date(DATE_ISO8601);
                return $this->save(false);
            }
        }

        return false;
    }

}
