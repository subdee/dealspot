<?php

/**
 * This is the model class for table "point_transaction".
 *
 * The followings are the available columns in table 'point_transaction':
 * @property integer $id
 * @property integer $user_id
 * @property integer $reason
 * @property string $transaction_date
 *
 * The followings are the available model relations:
 * @property User $user
 */
class PointTransaction extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PointTransaction the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'point_transaction';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, reason', 'numerical', 'integerOnly' => true),
            array('transaction_date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, user_id, reason, transaction_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'reason' => 'Reason',
            'transaction_date' => 'Transaction Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('reason', $this->reason);
        $criteria->compare('transaction_date', $this->transaction_date, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function behaviors() {
        return array('disableDefaultScopeBehavior' => array('class' => 'ext.behaviors.DisableDefaultScopeBehavior'));
    }

    public function defaultScope() {
        return $this->getDefaultScopeDisabled() ?
                array() :
                array(
                    'condition' => 'are_redeemable',
                    'order' => 'transaction_date DESC',
                );
    }

    public static function makeTransaction($points, $reason, $user = null) {
        if (!$user)
            $user = _profile();

        $trans = new PointTransaction;
        $trans->user_id = $user->id;
        $trans->reason = $reason;
        $trans->points = $points;

        $user->points += $points;
        if ($user->save() && $trans->save())
            return true;
        return false;
    }

}