<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property integer $role
 * @property string $username
 * @property string $password
 * @property string $email
 * @property integer $company_id
 *
 * The followings are the available model relations:
 * @property UserDetails $userDetails
 */
class User extends CActiveRecord {

    public $email2;
    public $password2;
    public $terms_accept;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('role, username, email, email2, password, password2, terms_accept', 'required'),
            array('terms_accept', 'isTrue'),
            array('role', 'numerical', 'integerOnly' => true),
            array('username', 'length', 'max' => 20),
            array('password', 'length', 'max' => 60),
            array('email', 'length', 'max' => 100),
            array('username', 'unique', 'message' => _t('user', 'this username is not available')),
            array('email', 'unique'),
            array('password, email', 'ext.validators.ESameValueValidator'),
            array('password', 'ext.validators.EPasswordStrength'),
            array('registration_date, last_login, company_id, newsletter', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, role, username, password, email', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'details' => array(self::HAS_ONE, 'UserDetails', 'user_id'),
            'activation' => array(self::HAS_ONE, 'UserActivation', 'user_id'),
            'passwordReset' => array(self::HAS_ONE, 'UserPasswordReset', 'user_id'),
            'transactions' => array(self::HAS_MANY, 'Transaction', 'user_id', 'scopes' => array('completed')),
            'invites' => array(self::HAS_MANY, 'UserInvite', 'user_id'),
            'pointTransactions' => array(self::HAS_MANY, 'PointTransaction', 'user_id'),
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'coupons' => array(self::HAS_MANY, 'Coupon', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'role' => 'Role',
            'username' => _t('user', 'username'),
            'password' => _t('user', 'password'),
            'password2' => _t('user', 'repeat password'),
            'email' => _t('user', 'email'),
            'email2' => _t('user', 'repeat email'),
            'terms_accept' => _t('user', 'do you accept {url}terms and conditions{/url}?', array(
                '{url}' => CHtml::openTag('a', array('href' => _url('site/termsOfUse'), 'target' => '_blank')),
                '{/url}' => CHtml::closeTag('a'),
            )),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('role', $this->role);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('email', $this->email, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function isTrue($attribute) {
        if (!$this->$attribute)
            $this->addError($attribute, _t('user', 'you must agree to {url}terms and conditions{/url}', array(
                        '{url}' => CHtml::openTag('a', array('href' => _url('site/terms'), 'target' => '_blank')),
                        '{/url}' => CHtml::closeTag('a'),
                    )));
    }

    public function scopes() {
        return array(
            'newsletter' => array(
                'with' => 'details',
                'condition' => 'details.newsletter',
            ),
        );
    }

    public function authenticate($password) {
        $ph = new PasswordHash(Yii::app()->params['phpass']['iteration_count_log2'], Yii::app()->params['phpass']['portable_hashes']);
        if (!$ph->CheckPassword($password, $this->password))
            return false;
        else
            return $this->id;
    }

    protected function beforeSave() {
        if (!empty($this->password)) {
            if ($this->isNewRecord || $this->password != User::model()->findByPk($this->id)->password) {
                $ph = new PasswordHash(Yii::app()->params['phpass']['iteration_count_log2'], Yii::app()->params['phpass']['portable_hashes']);
                $this->password = $ph->HashPassword($this->password);
            }
        }
        if ($this->isNewRecord)
            $this->registration_date = date('Y-m-d H:i:s');
        if (isset(_app()->session['invite_code'])) {
            $invite = UserInvite::model()->find('code = :code AND status = :status', array(':code' => _app()->session['invite_code'], ':status' => InviteStatus::UNUSED));
            if ($invite) {
                $invite->status = InviteStatus::REGISTERED;
                $invite->save(false);

                $pt = new PointTransaction;
                $pt->user_id = $invite->sender->id;
                $pt->points += 200;
                $pt->reason = PointTransactionReason::INVITATION;
                $pt->transaction_date = date(DATE_ISO8601);
                $pt->save();

                unset(_app()->session['invite_code']);
            }
        }
        if ($newsletter = NewsletterUsers::model()->findByAttributes(array('email' => $this->email)))
            $newsletter->delete();
        return parent::beforeSave();
    }

    public function afterSave() {
        if (isset($this->passwordReset))
            $this->passwordReset->delete();
    }

    public function isAdmin() {
        return $this->role == Role::ADMIN;
    }

    public function managesCompany() {
        return $this->role == Role::CLIENT;
    }

    public static function hasRole($role) {
        if (Utils::isLoggedIn())
            return Role::is(_user()->profile->role, $role);
        return false;
    }

    public function sendInviteEmail($invitee) {
        if (UserInvite::hasExceededLimit($this->id))
            return array('status' => false, 'message' => _t('user', 'you have already sent your allowed invitations'));
        if (UserInvite::alreadyExists($this->id, $invitee))
            return array('status' => false, 'message' => _t('user', 'you have already sent an invitation to this email'));

        $subject = _t('user', 'you have an invitation to dealspot.gr subject email');

        $body = _t('user', '{sender} has sent you an invitation to join dealspot. click {url}here{/url} to join!', array(
            '{sender}' => $this->username,
            '{url}' => CHtml::openTag('a', array('href' => $this->createInvitationLink($invitee))),
            '{/url}' => CHtml::closeTag('a'),
        ));

        if (Utils::sendEmail($invitee, $subject, $body))
            return array('status' => true, 'message' => _t('user', 'your invitation has been succesfully sent'));
        else
            return array('status' => false, 'message' => _t('user', 'your invitation could not be sent'));
    }

    public function createInvitationLink($invitee) {
        $code = sha1($this->username . rand());

        $invite = new UserInvite;
        $invite->user_id = $this->id;
        $invite->invited_email = $invitee;
        $invite->code = $code;
        $invite->status = InviteStatus::UNUSED;
        if ($invite->save())
            return  _app()->createAbsoluteUrl('user/login', array('invitation_code' => $code));
        return false;
    }

    public function changePassword($new, $repeat) {
        $user = User::model()->findByPk(_profile()->id);

        $user->password = $new;
        $user->password2 = $repeat;

        if (!$user->save(true, array('password'))) {
            _user()->setFlash('error', $user->getError('password'));
            return false;
        }

        _user()->setFlash('success', _t('user', 'password changed succesfully'));
        _profile()->refresh();
        return true;
    }

    public function createActivationCode() {
        $userActivation = new UserActivation;
        $userActivation->user_id = $this->id;
        $uniqueCode = function() {
                    do {
                        $randId = md5(uniqid());
                    } while (UserActivation::model()->exists('activation_code = :code', array(':code' => $randId)));
                    return $randId;
                };
        $userActivation->activation_code = $uniqueCode();
        $userActivation->requested_on = time();
        if ($userActivation->save()) {
            Utils::sendEmail($this->email, _t('user', 'activation email subject'), _t('user', 'activate your account to dealspot by clicking on this {link}', array('{link}' => CHtml::link('Dealspot.gr', _app()->createAbsoluteUrl('user/activate', array('code' => $userActivation->activation_code))))));
            return true;
        }
        return false;
    }

    public static function getRegisteredUsersPerDate($days) {
        --$days;
        $sql = <<<SQL
SELECT count( id ) AS cnt, DATE_FORMAT(registration_date, '%d-%m') AS reg_date
FROM user
WHERE registration_date > NOW() - INTERVAL :days DAY
GROUP BY DATE(registration_date)
ORDER BY DATE(registration_date) ASC
SQL;

        $conn = _app()->db;
        $comm = $conn->createCommand($sql);
        $comm->bindParam(':days', $days);

        return $comm->queryAll();
    }

    public static function getRegisteredUsers() {
        return count(_app()->db->createCommand()->select('count(id)')->from('user')->join('user_activation', 'user_activation.user_id = user.id'));
    }

    public function createResetCode() {
        $upr = isset($this->passwordReset) ? $this->passwordReset : new UserPasswordReset;
        $upr->user_id = $this->id;
        $upr->setResetCode();
        $upr->date_sent = date(DATE_ISO8601);
        if ($upr->save()) {
            $body = _t('profile', 'here\'s your code for resetting your password<br/>code: {code}', array(
                '{code}' => $upr->reset_code,
                    ));
            return Utils::sendEmail($this->email, _t('profile', 'password reset email subject'), $body);
        }
    }

    public function totalSales() {
        $sum = 0;
        foreach ($this->transactions as $transaction)
            $sum += $transaction->amount;

        return $sum;
    }

}
