<?php

/**
 * Description of Cart
 *
 * @author kthermos
 */
class Cart extends CModel {

    public $deal;
    public $amount;
    public $transaction;

    public function attributeNames() {
        return array(
            'deal',
            'amount',
            'transaction'
        );
    }

    public function add($deal, $amount = 1, Transaction $transaction = null) {
        if (!($this->deal instanceof Deal) || $this->deal->id != $deal->id) {
            $this->deal = $deal;
        }
        $this->amount = $amount;
        $this->transaction = $transaction;

        return true;
    }

    public function getTotal() {
        return $this->deal->coupon_price * $this->amount;
    }

}