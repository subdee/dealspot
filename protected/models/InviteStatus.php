<?php

/**
 * Description of InviteStatus
 *
 * @author kthermos
 */
class InviteStatus {

    const UNUSED = 1;
    const REGISTERED = 2;
    const BOUGHT = 3;

    public static function toText($status) {
        switch ($status) {
            case self::UNUSED:
                return _t('profile', 'unused');
                break;
            case self::REGISTERED:
                return _t('profile', 'registered');
                break;
            case self::BOUGHT:
                return _t('profile', 'bought');
                break;
            default:
                return false;
                break;
        }
    }
    
}