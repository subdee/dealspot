<?php
 
class FileUpload extends CFormModel {
 
    public $image;
 
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('image', 'file', 'allowEmpty' => true, 'types' => 'jpg, jpeg, gif, png', 'maxSize' => 1024000),
        );
    }
 
}