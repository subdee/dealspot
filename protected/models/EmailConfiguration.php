<?php

/**
 * This is the model class for table "email_configuration".
 *
 * The followings are the available columns in table 'email_configuration':
 * @property integer $id
 * @property string $invitation_subject
 * @property string $invitation_body
 */
class EmailConfiguration extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EmailConfiguration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_configuration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invitation_subject, invitation_body', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, invitation_subject, invitation_body', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invitation_subject' => 'Invitation Subject',
			'invitation_body' => 'Invitation Body',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('invitation_subject',$this->invitation_subject,true);
		$criteria->compare('invitation_body',$this->invitation_body,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}