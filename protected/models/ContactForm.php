<?php

class ContactForm extends CFormModel {

    public $fullname;
    public $email;
    public $phone;
    public $reason;
    public $description;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // username and password are required
            array('fullname, email, reason, description', 'required'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'fullname' => _t('user', 'fullname'),
            'email' => _t('user', 'email'),
            'phone' => _t('user', 'phone'),
            'reason' => _t('user', 'reason'),
            'description' => _t('user', 'description'),
        );
    }

    public static function reasonsArray() {
        return array(
            'problem with the website' => _t('main', 'problem with the website'),
            'question about daily deal' => _t('main', 'question about daily deal'),
            'problem with my coupon' => _t('main', 'problem with my coupon'),
            'problem with my account' => _t('main', 'problem with my account'),
            'other' => _t('main', 'other'),
        );
    }

    public function save() {
        $body = "You have an contact request from {$this->fullname} at {$this->email}.<br/><br/>";
        $body .= "Phone: {$this->phone}<br/>";
        $body .= "Reason: {$this->reason}<br/><br/>";
        $body .= "{$this->description}";
        return Utils::sendEmail(_param('emails', 'contact'), 'company contact', $body);
    }

}
