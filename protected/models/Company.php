<?php

/**
 * This is the model class for table "company".
 *
 * The followings are the available columns in table 'company':
 * @property integer $id
 * @property string $name
 * @property string $website
 * @property string $email
 * @property string $phone
 * @property string $fax
 * @property string $mobile
 * @property string $description
 * @property string $address
 * @property string $social
 * @property string $payment_details
 * @property string $coordinates
 *
 * The followings are the available model relations:
 * @property Deal[] $deals
 * @property UserDetails[] $userDetails
 */
class Company extends CActiveRecord {

    public $facebook;
    public $twitter;
    public $google_plus;
    public $image_file;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Company the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'company';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, email', 'required'),
            array('name', 'length', 'max' => 50),
            array('website, email', 'length', 'max' => 200),
            array('phone, fax, mobile', 'length', 'max' => 20),
            array('description, address, social, payment_details, facebook, twitter, google_plus, image,, coordinates', 'safe'),
            array('image_file', 'file', 'allowEmpty' => true, 'types' => 'jpg, jpeg, gif, png', 'maxSize' => 1024000, 'safe' => true),
            array('image', 'length', 'max' => 100),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, website, email, phone, fax, description, address, social, payment_details, coordinates', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'deals' => array(self::HAS_MANY, 'Deal', 'company_id'),
            'userDetails' => array(self::HAS_MANY, 'UserDetails', 'company_id'),
            'users' => array(self::HAS_MANY, 'User', 'company_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'website' => 'Website',
            'email' => 'Email',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'description' => 'Description',
            'address' => 'Address',
            'social' => 'Social',
            'payment_details' => 'Payment Details',
            'coordinates' => 'Coordinates',
            'mobile' => 'Mobile',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('website', $this->website, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('fax', $this->fax, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('social', $this->social, true);
        $criteria->compare('payment_details', $this->payment_details, true);
        $criteria->compare('coordinates', $this->coordinates, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function beforeSave() {
        $this->social = json_encode(array('facebook' => $this->facebook, 'twitter' => $this->twitter, 'google_plus' => $this->google_plus));
        
        //Get latitude longitude from Google API
        $address = urlencode($this->address);
        $data = json_decode(@file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address={$address}&sensor=false"));
        if ($data && isset($data->results[0]))
            $this->coordinates = $data->results[0]->geometry->location->lat . ',' . $data->results[0]->geometry->location->lng;
        
        return parent::beforeSave();
    }

    public function afterFind() {
        $social = json_decode($this->social);

        if ($social) {
            $this->facebook = $social->facebook;
            $this->twitter = $social->twitter;
            $this->google_plus = $social->google_plus;
        }
    }

}