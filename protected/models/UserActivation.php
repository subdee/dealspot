<?php

/**
 * This is the model class for table "user_activation".
 *
 * The followings are the available columns in table 'user_activation':
 * @property integer $user_id
 * @property string $activation_code
 * @property integer $requested_on
 *
 * The followings are the available model relations:
 * @property User $user
 */
class UserActivation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserActivation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_activation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, activation_code, requested_on', 'required'),
			array('user_id, requested_on', 'numerical', 'integerOnly'=>true),
			array('activation_code', 'length', 'max'=>32),
            array('activation_code', 'unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, activation_code, requested_on', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'activation_code' => 'Activation Code',
			'requested_on' => 'Requested On',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('activation_code',$this->activation_code,true);
		$criteria->compare('requested_on',$this->requested_on);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
    public function verifyCode($code) {
        return $this->find('activation_code = :code AND (:now - requested_on) < 86400', array(':code' => $code, ':now' => time()));
    }
}