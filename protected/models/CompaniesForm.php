<?php

class CompaniesForm extends CFormModel {

    public $fullname;
    public $name;
    public $phone;
    public $website;
    public $email;
    public $address;
    public $description;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // username and password are required
            array('fullname, name, phone, description', 'required'),
            array('address, email, website', 'safe'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'fullname' => _t('user', 'fullname'),
            'name' => _t('user', 'name'),
            'phone' => _t('user', 'phone'),
            'address' => _t('user', 'address'),
            'email' => _t('user', 'email'),
            'website' => _t('user', 'website'),
            'description' => _t('user', 'description'),
        );
    }
    
    public static function reasonsArray() {
        return array(
            
        );
    }
    
    public function save() {
        $body = "You have an contact request from {$this->fullname} &lt;{$this->email}&gt;.<br/><br/>";
        $body .= "Company name: {$this->name}<br/>";
        $body .= "Phone: {$this->phone}<br/>";
        $body .= "Address: {$this->address}<br/>";
        $body .= "Website: {$this->website}<br/><br/>";
        $body .= "{$this->description}";
        return Utils::sendEmail(_param('emails', 'companies'), 'company contact', $body);
    }

}
