<?php

/**
 * Description of PointTransactionReason
 *
 * @author kthermos
 */
class PointTransactionReason {

    const INVITATION = 1;
    const PURCHASE = 2;
    const DEAL = 3;
    const REFUND = 4;

    public static function toText($reason) {
        switch ($reason) {
            case self::INVITATION:
                return _t('profile', 'invitation');
                break;
            case self::PURCHASE:
                return _t('profile', 'point purchase');
                break;
            case self::DEAL:
                return _t('profile', 'deal purchase');
                break;
            case self::REFUND:
                return _t('profile', 'refund');
                break;
            default:
                return false;
                break;
        }
    }
    
}