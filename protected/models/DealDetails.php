<?php

/**
 * This is the model class for table "deal_details".
 *
 * The followings are the available columns in table 'deal_details':
 * @property integer $deal_id
 * @property string $title
 * @property string $subtitle
 * @property string $image
 * @property string $terms_of_use
 * @property string $coupon_instructions
 *
 * The followings are the available model relations:
 * @property Deal $deal
 */
class DealDetails extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DealDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'deal_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('deal_id', 'numerical', 'integerOnly'=>true),
            array('subtitle', 'length', 'max'=>100),
			array('title', 'length', 'max'=>30),
			array('image', 'length', 'max'=>50),
			array('terms_of_use, coupon_instructions', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('deal_id, title, subtitle, image, terms_of_use, coupon_instructions', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'deal' => array(self::BELONGS_TO, 'Deal', 'deal_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'deal_id' => 'Deal',
			'title' => _t('models', 'title'),
			'subtitle' => _t('models', 'description'),
			'terms_of_use' => _t('models', 'terms of use'),
			'coupon_instructions' => _t('models', 'coupon instructions'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('deal_id',$this->deal_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('subtitle',$this->subtitle,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('terms_of_use',$this->terms_of_use,true);
		$criteria->compare('coupon_instructions',$this->coupon_instructions,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}