<?php

/**
 * Description of PaymentMethod
 *
 * @author kthermos
 */
class PaymentMethod {

    const BANK = 1;
    const POINTS = 2;
    const PAYPAL = 3;
    const HELLASPAY = 4;

    public static function text($source) {
        switch ($source) {
            case self::BANK:
                return _t('profile', 'bank');
            case self::PAYPAL:
                return _t('profile', 'PayPal');
            case self::HELLASPAY:
                return _t('profile', 'HellasPay');
            case self::POINTS:
                return _t('profile', 'points');
            default:
                return false;
                break;
        }
    }

    public static function getArray() {
        return array(
            self::BANK => _t('profile', 'bank'),
            self::PAYPAL => _t('profile', 'PayPal'),
            self::HELLASPAY => _t('profile', 'HellasPay'),
            self::POINTS => _t('profile', 'points'),
        );
    }

}