<?php

/**
 * Description of Role
 *
 * @author kthermos
 */
class Role {

    const USER = 1;
    const CLIENT = 2;
    const DEV = 4;
    const ADMIN = 8;
    
    public static function is($userRole, $role) {
        if ($userRole) {
            if (($role & $userRole) == $userRole)
                return true;
        }
        return false;
    }

    public function getRoles() {
        return array(
            self::USER => 'User',
            self::CLIENT => 'Client',
            self::DEV => 'Dev',
            self::ADMIN => 'Admin',
        );
    }

    public function toText($role) {
        switch ($role) {
            case self::USER:
                return 'User';
            case self::CLIENT:
                return 'Client';
            case self::DEV:
                return 'Dev';
            case self::ADMIN:
                return 'Admin';
            default:
                return false;
                break;
        }
    }

}