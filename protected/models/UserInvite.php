<?php

/**
 * This is the model class for table "user_invite".
 *
 * The followings are the available columns in table 'user_invite':
 * @property integer $id
 * @property integer $user_id
 * @property string $invited_email
 * @property string $code
 * @property string $timestamp
 *
 * The followings are the available model relations:
 * @property User $user
 */
class UserInvite extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserInvite the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_invite';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id', 'numerical', 'integerOnly' => true),
            array('invited_email', 'length', 'max' => 50),
            array('code', 'length', 'max' => 40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, user_id, invited_email, code, timestamp', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'sender' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'invited_email' => 'Invited Email',
            'code' => 'Code',
            'timestamp' => 'Timestamp',
        );
    }

    public function defaultScope() {
        return array(
            'order' => 'timestamp DESC',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('invited_email', $this->invited_email, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('timestamp', $this->timestamp, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public static function alreadyExists($id, $invitee) {
        return UserInvite::model()->exists('user_id = :id AND invited_email = :invitee', array(':id' => $id, ':invitee' => $invitee)) ||
            User::model()->exists('email = :email', array(':email' => $invitee));
    }

    public static function hasExceededLimit($id) {
        return count(UserInvite::model()->findAll('user_id = :id', array(':id' => $id))) >= Configuration::get('max_invites');
    }

}