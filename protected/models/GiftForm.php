<?php

class GiftForm extends CFormModel {

    public $fullname;
    public $email;
    public $paper;
    public $unwrap_time;
    public $personal_message;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // username and password are required
            array('fullname, email, paper, unwrap_time, personal_message', 'required'),
//            array('unwrap_time', 'notPast'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'fullname' => _t('gift', 'fullname'),
            'email' => _t('gift', 'email'),
            'paper' => _t('gift', 'paper'),
            'unwrap_time' => _t('gift', 'unwrap_time'),
            'personal_message' => _t('gift', 'personal_message'),
        );
    }

    public function send(Coupon $coupon) {
        $gift = _app()->wagclient->setGift(array(
            'paper' => $this->paper,
            'unwrapDate' => $this->unwrap_time,
            'senderEmail' => $coupon->transaction->user->email,
            'senderName' => $coupon->transaction->user->details->fullname,
            'recipientEmail' => $this->email,
            'recipientName' => $this->fullname,
            'personalMessage' => $this->personal_message,
            'title' => 'Dealspot|κουπόνι',
            'giftLink' => false,
            'image' => _bp("../images/deals/{$coupon->transaction->deal->image}"),
            'file' => _bp("/coupons/{$coupon->coupon_code}.pdf"),
                ));

        return _app()->wagclient->sendGift($gift);
    }
    
    public function notPast($attribute, $params) {
        if (strtotime($attribute) < (time() - 300))
            $this->addError($attribute, _t('gift', 'Unwrap time should be in the future'));
    }

}
