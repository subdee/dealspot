<?php

class Utils {

    /**
     *
     * @param type $val
     * @param type $options
     * @return type
     */
    public static function date($val, $options = array()) {

        $default = array(
            'time' => true,
            'year' => false,
            'dayAbbreviated' => true,
            'monthAbbreviated' => true
        );
        $options = array_merge($default, $options);

        $string = 'EEE';
        if (!$options['dayAbbreviated'])
            $string .= 'E';
        $string .= ', d MMM';
        if (!$options['monthAbbreviated'])
            $string .= 'M';
        if ($options['year'])
            $string .= ' y';
        if ($options['time'])
            $string .= ' HH:mm';

        if (!$val || $val == '0000-00-00 00:00:00' || $val == '0000-00-00')
            return null;

        return _app()->dateFormatter->format($string, $val);
    }

    public static function dateLeft($dateStart, $dateInterval) {
        $date = new DateTime($dateStart);
        $date->add(new DateInterval($dateInterval));
        $now = new DateTime();
        return $now->diff($date)->format('%a ' . _t('main', 'days'));
    }

    /**
     *
     * @param type $val
     * @param type $leadingZeros
     * @param type $decimals
     * @return type
     */
    public static function number($val, $leadingZeros = false, $decimals = 1) {
        if ($leadingZeros) {
            if ($decimals == 1)
                return _app()->numberFormatter->format('#,###.0', $val);
            elseif ($decimals == 2)
                return _app()->numberFormatter->format('#,###.00', $val);
        } else
            return _app()->numberFormatter->format('#,###.##', $val);
    }

    /**
     *
     * @param type $p
     * @param type $showSign
     * @param type $round
     * @return type
     */
    public static function percent($p, $showSign = false, $round = true, $showDecimals = true) {
        if ($round)
            $p = round($p, 4);
        if ($showDecimals) {
            if ($showSign)
                return Yii::app()->numberFormatter->format('###.00%', $p);
            else
                return Yii::app()->numberFormatter->format('###.00', $p) * 100;
        } else {
            if ($showSign)
                return Yii::app()->numberFormatter->format('###%', $p);
            else
                return Yii::app()->numberFormatter->format('###', $p) * 100;
        }
    }

    /**
     *
     * @param type $to
     * @param type $subject
     * @param type $body
     * @param type $from
     * @param type $name
     * @return type
     */
    public static function sendEmail($to, $subject, $body) {
        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $mailer->IsSMTP();
        $mailer->SMTPDebug = 0;
        $mailer->Host = 'localhost';
        $mailer->From = 'support@dealspot.gr';
        $mailer->FromName = 'Dealspot.gr';
        $mailer->AddAddress($to);
        $mailer->CharSet = 'UTF-8';
        $mailer->ContentType = 'text/html;charset=utf8';
        $mailer->Subject = $subject;
        $mailer->Body = $body;
        return $mailer->Send();
    }

    /**
     *
     * @param type $to
     * @param type $subject
     * @param Coupon $coupon
     * @return type
     */
    public static function sendCouponEmail($to, $subject, $coupon) {
        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $mailer->IsSMTP();
        $mailer->SMTPDebug = 0;
        $mailer->Host = 'localhost';
        $mailer->From = 'support@dealspot.gr';
        $mailer->FromName = 'Dealspot.gr';
        $mailer->AddAddress($to);
        $mailer->CharSet = 'UTF-8';
        $mailer->ContentType = 'text/html;charset=utf8';
        $mailer->Subject = $subject;
        $mailer->Body = _app()->controller->renderPartial('//shop/_coupon', array('coupon' => $coupon), true);
        return $mailer->Send();
    }

    /**
     *
     * @param type $data
     */
    public static function jsonReturn($data) {
        header('Content-type: application/json');
        echo json_encode($data);
        _app()->end();
    }

    /**
     *
     * @param type $msg
     * @return type
     */
    public static function replaceSmileys($msg) {

        $smilies = array(':)', ':(', ':P', ';)', ':lol', ':thumbsup', ':laugh', ':ohmy', ':rolleyes', ':scared', ':sneaky', ':unsure', ':w00t', ':wub');
        $images = array('smile', 'sad', 'tongue', 'wink', 'lol', 'thumbsup', 'laugh', 'ohmy', 'rolleyes', 'scared', 'sneaky', 'unsure', 'w00t', 'wub');

        foreach ($images as &$image) {
            $image = CHtml::image(Utils::imageUrl("smileys/$image.gif"), $image);
        }

        return str_replace($smilies, $images, $msg);
    }

    /**
     * Registers a callback with an event using the event system application component.
     * @param string $event
     * @param mixed $subject
     * @param string $callback
     */
    public static function registerCallback($event, $subject, $callback) {
        _app()->eventSystem->$event = array($subject, $callback);
    }

    /**
     * Registers a callback with an event using the event system application component.
     * @param string $event
     * @param mixed $subject
     * @param string $callback
     */
    public static function registerNamedCallback($behavior, $event, $subject, $callback) {
        if (isset(_app()->eventSystem->$behavior)) {
            _app()->eventSystem->$behavior->$event = array($subject, $callback);
        }
    }

    /**
     * Triggers an event using the event system application component.
     * @param string $event
     * @param mixed $sender
     * @param array $params
     */
    public static function triggerEvent($event, $sender, $params = array()) {
        _app()->eventSystem->$event(new CEvent($sender, $params));
    }

    /**
     *
     * @param type $file
     * @param type $width
     * @param type $height
     * @param type $proportional
     * @param type $output
     * @param type $delete_original
     * @param type $use_linux_commands
     * @return type
     */
    public static function ImageResize($file, $width = 0, $height = 0, $proportional = false, $output = 'file', $delete_original = true, $use_linux_commands = false) {

        if ($height <= 0 && $width <= 0)
            return false;

        // Setting defaults and meta
        $info = getimagesize($file);
        $image = '';
        $final_width = 0;
        $final_height = 0;
        list($width_old, $height_old) = $info;

        // Calculating proportionality
        if ($proportional) {
            if ($width == 0)
                $factor = $height / $height_old;
            elseif ($height == 0)
                $factor = $width / $width_old;
            else
                $factor = min($width / $width_old, $height / $height_old);

            $final_width = round($width_old * $factor);
            $final_height = round($height_old * $factor);
        }
        else {
            $final_width = ( $width <= 0 ) ? $width_old : $width;
            $final_height = ( $height <= 0 ) ? $height_old : $height;
        }

        // Loading image to memory according to type
        switch ($info[2]) {
            case IMAGETYPE_GIF: $image = imagecreatefromgif($file);
                break;
            case IMAGETYPE_JPEG: $image = imagecreatefromjpeg($file);
                break;
            case IMAGETYPE_PNG: $image = imagecreatefrompng($file);
                break;
            default: return false;
        }


        // This is the resizing/resampling/transparency-preserving magic
        $image_resized = imagecreatetruecolor($final_width, $final_height);
        if (($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG)) {
            $transparency = imagecolortransparent($image);

            if ($transparency >= 0) {
                $trnprt_indx = imagecolorat($image, 0, 0);
                $trnprt_color = imagecolorsforindex($image, $trnprt_indx);
                $transparency = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
                imagefill($image_resized, 0, 0, $transparency);
                imagecolortransparent($image_resized, $transparency);
            } elseif ($info[2] == IMAGETYPE_PNG) {
                imagealphablending($image_resized, false);
                $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
                imagefill($image_resized, 0, 0, $color);
                imagesavealpha($image_resized, true);
            }
        }
        imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);

        // Taking care of original, if needed
        if ($delete_original) {
            if ($use_linux_commands)
                exec('rm ' . $file);
            else
                @unlink($file);
        }

        // Preparing a method of providing result
        switch (strtolower($output)) {
            case 'browser':
                $mime = image_type_to_mime_type($info[2]);
                header("Content-type: $mime");
                $output = NULL;
                break;
            case 'file':
                $output = $file;
                break;
            case 'return':
                return $image_resized;
                break;
            default:
                break;
        }

        // Writing image according to type to the output destination
        switch ($info[2]) {
            case IMAGETYPE_GIF: imagegif($image_resized, $output);
                break;
            case IMAGETYPE_JPEG: imagejpeg($image_resized, $output, 100);
                break;
            case IMAGETYPE_PNG: imagepng($image_resized, $output);
                break;
            default: return false;
        }

        return true;
    }

    /**
     * @param string $file
     * @return string
     */
    public static function photoUrl($file = '') {
        return Utils::imageUrl('photo/' . $file);
    }

    /**
     * Shorthand for Yii is ajax request code.
     */
    public static function isAjax() {
        return Yii::app()->request->isAjaxRequest;
    }

    /**
     *
     * @param string $date
     * @return string
     */
    public static function fromLocalDatetime($date) {
        return Yii::app()->localtime->fromLocalDateTime($date, 'Y-m-d H:i:s');
    }

    /**
     *
     * @param string $date
     * @return string
     */
    public static function toLocalDatetime($date) {
        return Yii::app()->localtime->toLocalDateTime($date, 'Y-m-d H:i:s');
    }

    /**
     * @return boolean Whether a user is logged in or not.
     */
    public static function isLoggedIn() {
        return !_user()->isGuest;
    }

    /**
     * Return the formatted value as a currency.
     * @param string $value
     * @return string the formatted value
     */
    public static function currency($value) {
        return Yii::app()->numberFormatter->format('#,###,###.00¤', $value, 'EUR');
    }

    /**
     * Add days to a date
     * @param DateTime $date
     * @param int $days
     * @return string The formatted date
     */
    public static function daysAdd($date, $days) {
        $date = new DateTime($date);
        $date->add(new DateInterval('P' . $days . 'D'));

        return self::date($date->format('Y-m-d H:i:s'));
    }

    /**
     *
     * @param type $str
     * @param type $start
     * @param type $length
     * @param type $append
     */
    public static function substrFromStart($str, $length, $append = '') {
        if (isset($str[$length])) {
            $str = mb_substr($str, 0, $length) . $append;
        }
        return $str;
    }

    public function isValidLang($language) {
        return in_array($language, array('en', 'nl'));
    }

    public static function getFileInfo($filepath) {
        preg_match('/[^?]*/', $filepath, $matches);
        $string = $matches[0];

        $pattern = preg_split('/\./', $string, -1, PREG_SPLIT_OFFSET_CAPTURE);

        $lastdot = $pattern[count($pattern) - 1][1];
        #now extract the filename using the basename function
        $filename = basename(substr($string, 0, $lastdot - 1));

        if (count($pattern) > 1) {
            $filenamepart = $pattern[count($pattern) - 1][0];
            preg_match('/[^?]*/', $filenamepart, $matches);
            $filename .= '.' . $matches[0];
        }

//        return array($filename, $matches[0]);
        return $filename;
    }

    public static function logArErrors($errors) {
        $output = '';
        foreach ($errors as $error) {
            $output .= $error . "\n";
        }
        return $output;
    }

    public static function fileList($dir, $ext) {
        $list = array();
        foreach (array_diff(scandir($dir), array('.', '..')) as $file) {
            if (is_file($dir . '/' . $file) && (($ext) ? ereg($ext . '$', $file) : 1))
                $list[] = $file;
        }
        return $list;
    }

    /**
     * Helpful function to include a css file in your application.
     * @param string $file
     */
    public static function registerCssFile($file) {
        _cs()->registerCSSFile(_tbu("css/{$file}.css"));
    }

    /**
     * Helpful function to include a module css file in your application.
     * @param string $module
     * @param string $file
     */
    public static function registerModuleCssFile($module, $file) {
        _cs()->registerCSSFile(self::moduleAssetUrl($module, "css/{$file}.css"));
    }

    /**
     * Helpful function to include a javascript file in your application.
     * @param string $file
     */
    public static function registerJsFile($file, $position = CClientScript::POS_END) {
        _cs()->registerScriptFile(_bu("js/{$file}.js"), $position);
    }

    /**
     * Helpful function to include a module javascript file in your application.
     * @param string $module
     * @param string $file
     */
    public static function registerModuleJsFile($module, $file, $position = CClientScript::POS_END) {
        _cs()->registerScriptFile(self::moduleAssetUrl($module, "js/{$file}.js"), $position);
    }

    /**
     * The function returns the url of an asset taking in consideration whether there is a theme active or not. Also, it force publishes
     * the assets when debug is on.
     * @param string $module Module name
     * @param string $asset Asset name
     * @return string Complete asset url
     */
    public static function moduleAssetUrl($module, $asset = '') {
        if (_app()->hasModule($module)) {
            return self::_assetUrl($module, $asset);
        }
        return '';
    }

    /**
     * @param string $module Module name
     * @param string $asset Asset name
     * @return string Complete asset url
     */
    private static function _assetUrl($module, $asset = '') {
        $alias = "$module.assets";
        if (_app()->theme) {
            $alias .= '.' . _app()->theme->name;
        }
        $assetUrl = _app()->assetManager->publish(Yii::getPathOfAlias($alias)) . '/' . $asset;
        return $assetUrl;
    }

    /**
     * @param string $image The image name/relative path
     * @return string The complete image url
     */
    public static function imageUrl($image) {
        return _bu("images/$image");
    }

    public static function inArrayRecursive($needle, $haystack) {

        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($haystack));

        foreach ($it AS $element) {
            if ($element == $needle) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @param type $asset
     * @return string
     */
    private static function _adminAssetUrl($asset='') {
        $alias = "admin.assets";
        $assetUrl = _app()->assetManager->publish(Yii::getPathOfAlias($alias), false, -1, false) . '/' . $asset;
        return $assetUrl;
    }

    /**
     *
     * @param type $image
     * @return type
     */
    public static function adminImageUrl($image) {
        return self::_adminAssetUrl("images/$image");
    }

    /**
     *
     * @param type $array
     * @return type
     */
    public static function isAssoc($array) {
        return (is_array($array) && count(array_filter(array_keys($array), 'is_string')) == count($array));
    }

    /**
     *
     * @param type $module
     * @return type
     */
    public static function moduleExists($module) {
        return array_key_exists($module, _app()->modules);
    }

    /**
     *
     * @param array $originals
     * @param array $replacements
     * @param type $subject
     * @return type
     */
    public static function replaceSmartFields(array $originals, array $replacements, $subject) {
        $res = str_replace($originals, $replacements, $subject);
        return $res;
    }

    /**
     *
     * @param type $prepend
     * @param type $id
     * @param type $extension
     * @param type $folder
     * @return type
     */
    public static function createFileName($prepend, $id, $extension, $folder = false) {
        if ($folder)
            return _bp('../images/' . $folder . '/' . $prepend . '_' . $id . '.' . $extension);
        else
            return $prepend . '_' . $id . '.' . $extension;
    }
}

?>
