<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $name
 *
 * The followings are the available model relations:
 * @property Subcategory[] $subcategories
 */
class Category extends CActiveRecord {

    public $image_file;
    private $_tree;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Category the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'category';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('image', 'safe'),
            array('image_file', 'file', 'allowEmpty' => true, 'safe' => true),
            array('name', 'length', 'max' => 50),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'subcategories' => array(self::HAS_MANY, 'Subcategory', 'category_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
        );
    }

    public function scopes() {
        return array(
            'withDeals' => array(
                'with' => array('subcategories' => array('with' => array('deals'))),
                'condition' => 'deals.is_active = 1 AND deals.valid_to > NOW()',
                'group' => 'deals.id',
                'having' => 'COUNT(deals.id) > 0'
            ),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function getTree() {
        //TODO: Improve this shit

        $categories = Category::model()->findAll();

        foreach ($categories as $category) {
            $this->_tree['category'.$category->id] = $category->name;
            foreach ($category->subcategories as $sub) {
                $this->_tree[$sub->id] = str_repeat('-', 1) . ' ' . $sub->name;
            }
        }

        return $this->_tree;
    }

    public function getNoOfDeals() {
        $total = 0;
        foreach ($this->subcategories as $sub)
            $total += count($sub->deals);

        return $total;
    }
    
    public function allActiveDeals() {
        return Deal::model()->count('is_active = 1 && valid_to >NOW()');
    }

}