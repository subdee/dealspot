<?php
/**
 * This is the model class for table "deal".
 *
 * The followings are the available columns in table 'deal':
 * @property integer $id
 * @property integer $company_id
 * @property integer $city_id
 * @property integer $subcategory_id
 * @property integer $valid_from
 * @property integer $valid_to
 * @property integer $coupon_valid_to
 * @property double $original_price
 * @property double $coupon_price
 * @property integer $min_required
 * @property integer $max_coupons_user
 * @property integer $available_coupons
 * @property string $image
 * @property string $side_image
 * @property string $name
 * @property integer $is_active
 *
 * The followings are the available model relations:
 * @property Subcategory $subcategory
 * @property Company $company
 * @property City $city
 * @property DealDetails $dealDetails
 * @property DealPeriod $dealPeriod
 */
class Deal extends CActiveRecord {
    public $image_file;
    public $side_image_file;
    public $coupon_perc;
    public $time_left;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Deal the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'deal';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('company_id, city_id, subcategory_id, original_price, coupon_price, min_required, max_coupons_user, available_coupons, name, valid_from, valid_to, coupon_valid_to', 'required'),
            array('company_id, city_id, min_required, max_coupons_user, available_coupons, is_active, coupons_bought', 'numerical', 'integerOnly' => true),
            array('original_price, coupon_price', 'numerical', 'min' => 0.01),
            array('image, side_image', 'length', 'max' => 50),
            array('name', 'length', 'max' => 100),
            array('image_file, side_image_file', 'file', 'allowEmpty' => true, 'types' => 'jpg, jpeg, gif, png', 'maxSize' => 1024000, 'safe' => true),
            array('valid_to', 'correctDates'),
            array('subcategory_id', 'validSubcategory'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, company_id, city_id, subcategory_id, original_price, coupon_price, min_required, max_coupons_user, available_coupons, image, side_image, name, is_active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'subcategory' => array(self::BELONGS_TO, 'Subcategory', 'subcategory_id'),
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'city' => array(self::BELONGS_TO, 'City', 'city_id'),
            'details' => array(self::HAS_ONE, 'DealDetails', 'deal_id'),
            'transactions' => array(self::HAS_MANY, 'Transaction', 'deal_id', 'scopes' => array('completed')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'company_id' => _t('models', 'company'),
            'city_id' => _t('models', 'city'),
            'subcategory_id' => _t('models', 'subcategory'),
            'original_price' => _t('models', 'original price'),
            'coupon_price' => _t('models', 'coupon price'),
            'min_required' => _t('models', 'min coupons required'),
            'max_coupons_user' => _t('models', 'max coupons per user'),
            'available_coupons' => _t('models', 'available coupons'),
            'image' => _t('models', 'main image'),
            'side_image' => _t('models', 'details and side image'),
            'name' => _t('models', 'name'),
            'is_active' => _t('models', 'is active'),
            'valid_from' => _t('models', 'valid from'),
            'valid_to' => _t('models', 'valid to'),
            'coupon_valid_to' => _t('models', 'coupon valid to'),
            'image_file' => _t('models', 'main image'),
            'side_image_file' => _t('models', 'details and side image'),
        );
    }

    public function scopes() {
        return array(
            'active' => array(
                'condition' => 'is_active = 1 AND valid_to > NOW() AND valid_from < NOW()',
            ),
            'ordered' => array(
                'order' => 'valid_to asc',
            ),
            'latest' => array(
                'condition' => 'is_active = 1 AND valid_from BETWEEN NOW() - INTERVAL 1 DAY AND NOW()',
            ),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

    protected function beforeSave() {
        $this->valid_from = date(DATE_ISO8601, strtotime($this->valid_from));
        $this->valid_to = date(DATE_ISO8601, strtotime($this->valid_to));
        $this->coupon_valid_to = date(DATE_ISO8601, strtotime($this->coupon_valid_to));

        return parent::beforeSave();
    }

    protected function afterFind() {
        $now = new DateTime();
        $time_left = $now->diff(new DateTime($this->valid_to));
        $this->coupon_perc = Utils::percent(1 - ($this->coupon_price / $this->original_price), true, true, false);
        $this->time_left = $time_left->invert ? false : $time_left;
        $this->available_coupons = $this->available_coupons ? : 1000000000;
        $this->max_coupons_user = $this->max_coupons_user ? : 1000000000;
    }

    //Validators
    public function correctDates($attribute, $params) {
        if (strtotime($this->valid_from) > strtotime($this->valid_to)) {
            $this->addError('valid_to', _t('admin', 'deal cannot end before it starts'));
            return false;
        }
        return true;
    }

    public function validSubcategory($attribute, $params) {
        if (strpos($this->subcategory_id, 'category') !== false) {
            $this->addError('subcategory_id', _t('admin', 'you must select a subcategory'));
            return false;
        }
        return true;
    }

    public static function getRandomDeals($limit, $categoryID = false) {
        $sql = <<<SQL
SELECT id
    FROM deal
    WHERE is_active = 1 AND valid_to > NOW()
SQL;
        $conn = _app()->db;
        $comm = $conn->createCommand($sql);
        $ids = $comm->queryAll();

        $idS = array();
        foreach ($ids as $id) {
            $idS[] = $id['id'];
        }

        $limit = max(array($limit, count($idS)));

        $IDs = array();

        for ($i = 0; $i < $limit; ++$i)
            $IDs[] = $idS[rand(0, count($idS) - 1)];

        $criteria = new CDbCriteria;
        $criteria->addInCondition('id', $IDs);
        return Deal::model()->findAll($criteria);
    }

    public static function getNewsletterDeals($limit) {
        return Deal::model()->latest()->ordered()->findAll(array('limit' => $limit));
    }

    public function getCouponsAvailableArray() {
        $data = array();
        $maxCoupons = min(array($this->available_coupons - $this->coupons_bought, $this->max_coupons_user, 10));
        for ($i = 1; $i < $maxCoupons; ++$i)
            $data[$i] = $i;

        return $data;
    }

    public function updateCouponsBought($amount, $return = false) {
        if ($return)
            $this->coupons_bought -= $amount;
        else
            $this->coupons_bought += $amount;
        return $this->save(false);
    }

    public function isActive() {
        return $this->is_active && strtotime($this->valid_to) > time() && strtotime($this->valid_from) < time();
    }

    public function getAllCoupons() {
        $coupons = array();
        foreach ($this->transactions as $transaction) {
            foreach ($transaction->coupons as $coupon)
                $coupons[] = $coupon;
        }

        return $coupons;
    }

    public static function createXML() {
        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><deals></deals>');
        $deals = Deal::model()->active()->findAll();

        foreach ($deals as $deal) {
            list($long, $lat) = explode(',', $deal->company->coordinates);

            $node = $xml->addChild('deal');
            $node->addChild('deal_id', $deal->id);
            $node->addChild('deal_title', str_replace('&nbsp;', ' ', strip_tags($deal->details->coupon_instructions)));
            $node->addChild('deal_url', _app()->createAbsoluteUrl('deal/index', array('id' => $deal->id)));
            $node->addChild('deal_city', $deal->city->name);
            $node->addChild('deal_price', $deal->coupon_price);
            $node->addChild('deal_previous_price', $deal->original_price);
            $node->addChild('deal_discount', round(1 - ($deal->coupon_price / $deal->original_price), 4) * 100);
            $node->addChild('deal_start', $deal->valid_from);
            $node->addChild('deal_end', $deal->valid_to);
            $node->addChild('deal_image', _app()->createAbsoluteUrl("images/deals/{$deal->small_image}"));
            $node->addChild('deal_sales', $deal->coupons_bought);
            $node->addChild('deal_active', $deal->isActive() ? 'true' : 'false');
            $node->addChild('deal_description', str_replace('&nbsp;', ' ', strip_tags($deal->details->coupon_instructions)));
            $node->addChild('deal_category', $deal->subcategory->tag);
            $node->addChild('deal_type', 'main');
            $node->addChild('deal_gmaps_latitude', $lat);
            $node->addChild('deal_gmaps_longitude', $long);
            $node->addChild('deal_gmaps_address', $deal->company->address);
        }

        return $xml->asXML();
    }

    public function hasUserReachedLimit() {
        if (isset(_profile()->id))
            return Coupon::model()->with('transaction')->count('transaction.deal_id = :deal AND transaction.user_id = :user AND transaction.status = :status',
                    array(':deal' => $this->id, ':user' => _profile()->id, ':status' => Transaction::COMPLETED)) >= $this->max_coupons_user;
    }


    ///TODO: Remove this shit
    private function categoryMap($category) {

    }

}
