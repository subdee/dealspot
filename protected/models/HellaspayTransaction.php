<?php
/**
 * This is the model class for table "hellaspay_transaction".
 *
 * The followings are the available columns in table 'hellaspay_transaction':
 * @property integer $transaction_id
 * @property string $txn_id
 *
 * The followings are the available model relations:
 * @property Transaction $transaction
 */
class HellaspayTransaction extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return HellaspayTransaction the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hellaspay_transaction';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('transaction_id, txn_id', 'required'),
            array('transaction_id', 'numerical', 'integerOnly' => true),
            array('txn_id', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('transaction_id, txn_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'transaction' => array(self::BELONGS_TO, 'Transaction', 'transaction_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'transaction_id' => 'Transaction',
            'txn_id' => 'Txn',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('transaction_id', $this->transaction_id);
        $criteria->compare('txn_id', $this->txn_id, true);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

}
