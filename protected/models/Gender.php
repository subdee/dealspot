<?php

/**
 * Description of Gender
 *
 * @author kthermos
 */
class Gender {

    const MALE = 1;
    const FEMALE = 2;

    public static function getGenders() {
        return array(
            self::MALE => _t('main', 'male'),
            self::FEMALE => _t('main', 'female'),
        );
    }

    public static function toText($gender) {
        switch ($gender) {
            case self::MALE:
                return _t('main', 'male');
            case self::FEMALE:
                return _t('main', 'female');
            default:
                return false;
                break;
        }
    }
    
    public function parseText($gender) {
        switch ($gender) {
            case 'Male':
            case 'male':
            case 'm':
            case 'M':
                return self::MALE;
                break;
            case 'Female':
            case 'female':
            case 'f':
            case 'F':
                return self::FEMALE;
                break;
            default:
                return false;
                break;
        }
    }

}