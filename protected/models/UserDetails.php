<?php

/**
 * This is the model class for table "user_details".
 *
 * The followings are the available columns in table 'user_details':
 * @property integer $user_id
 * @property integer $company_id
 * @property string $fullname
 * @property string $mobile
 * @property integer $gender
 * @property string $birthdate
 * @property integer $points
 * @property boolean $newsletter
 *
 * The followings are the available model relations:
 * @property Company $company
 * @property User $user
 */
class UserDetails extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserDetails the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_details';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, company_id, gender, points', 'numerical', 'integerOnly' => true),
            array('fullname', 'length', 'max' => 100),
            array('mobile', 'length', 'max' => 20),
            array('birthdate, newsletter', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('user_id, company_id, fullname, mobile, gender, birthdate, points', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'company' => array(self::BELONGS_TO, 'Company', 'company_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'user_id' => 'User',
            'company_id' => _t('user', 'company'),
            'fullname' => _t('user', 'full name'),
            'mobile' => _t('user', 'mobile'),
            'gender' => _t('user', 'gender'),
            'birthdate' => _t('user', 'birthdate'),
            'points' => _t('user', 'points'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('company_id', $this->company_id);
        $criteria->compare('fullname', $this->fullname, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('gender', $this->gender);
        $criteria->compare('birthdate', $this->birthdate, true);
        $criteria->compare('points', $this->points);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    protected function beforeSave() {
        if ($this->birthdate && trim($this->birthdate) != '')
            $this->birthdate = date(DATE_ISO8601, strtotime($this->birthdate));
        else
            $this->birthdate = null;
        return parent::beforeSave();
    }

    protected function afterFind() {
        $this->birthdate = Utils::date($this->birthdate, array('time' => false, 'year' => true));
        return true;
    }

}