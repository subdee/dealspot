<?php

/**
 * Description of UserSource
 *
 * @author kthermos
 */
class UserSource {

    const REGISTERED = 1;
    const FACEBOOK = 2;

    public function toText($source) {
        switch ($source) {
            case self::REGISTERED:
                return 'Registered';
            case self::FACEBOOK:
                return 'Facebook';
            default:
                return false;
                break;
        }
    }

}