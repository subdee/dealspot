<?php
/**
 * This is the model class for table "user_password_reset".
 *
 * The followings are the available columns in table 'user_password_reset':
 * @property integer $user_id
 * @property string $reset_code
 * @property string $date_sent
 *
 * The followings are the available model relations:
 * @property User $user
 */
class UserPasswordReset extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserPasswordReset the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_password_reset';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, reset_code, date_sent', 'required'),
            array('user_id', 'numerical', 'integerOnly' => true),
            array('reset_code', 'length', 'max' => 6),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('user_id, reset_code, date_sent', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'user_id' => 'User',
            'reset_code' => 'Reset Code',
            'date_sent' => 'Date Sent',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('reset_code', $this->reset_code, true);
        $criteria->compare('date_sent', $this->date_sent, true);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

    public function setResetCode() {
        $uniqueCode = function() {
                do {
                    $randId = substr(uniqid(), 7, 6);
                } while (UserPasswordReset::model()->exists('reset_code = :code', array(':code' => $randId)));
                return $randId;
            };
        return $this->reset_code = $uniqueCode();
    }

    public static function verifyByCode($resetCode) {
        $upr = self::model()->find('reset_code = :reset_code', array(':reset_code' => $resetCode));
        return $upr && ((time() - strtotime($upr->date_sent)) < 86400) ? $upr : false;
    }

}