<?php

/**
 * This is the model class for table "transaction".
 *
 * The followings are the available columns in table 'transaction':
 * @property integer $id
 * @property integer $user_id
 * @property integer $no_of_items
 * @property double $amount
 * @property integer $payment_method
 * @property integer $status
 * @property string $transaction_date
 *
 * The followings are the available model relations:
 * @property User $user
 */
class Transaction extends CActiveRecord {
    const UNVERIFIED = 1;
    const VERIFIED = 2;
    const COMPLETED = 3;
    const FAILED = 4;

    public $user_username;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Transaction the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'transaction';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('user_id, amount, payment_method, transaction_date, txn_id, cart_contents, correlation_id, status, recipients, deal_id, token, user_username', 'safe'),
            array('payment_method, user_username', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'coupons' => array(self::HAS_MANY, 'Coupon', 'transaction_id'),
            'deal' => array(self::BELONGS_TO, 'Deal', 'deal_id'),
            'paypal' => array(self::HAS_ONE, 'PaypalTransaction', 'transaction_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'amount' => _t('admin', 'Amount Paid'),
            'payment_method' => _t('admin', 'Payment Method'),
            'status' => 'Is Valid',
            'transaction_date' => _t('admin', 'Transaction Date'),
            'user_username' => _t('admin', 'Username'),
        );
    }

    public function scopes() {
        return array(
            'valid' => array(
                'condition' => 'status = 1',
                'order' => 'transaction_date DESC',
            ),
            'completed' => array(
                'condition' => 'status = :status',
                'params' => array(':status' => self::COMPLETED),
                'order' => 'transaction_date DESC',
            ),
            'pending' => array(
                'condition' => 'status = :status',
                'params' => array(':status' => self::VERIFIED),
                'order' => 'transaction_date DESC',
            ),
        );
    }

    public function search() {

        $criteria = new CDbCriteria;
        $criteria->with = array('user');
        $criteria->order = 'transaction_date DESC';

        $criteria->compare('user.username', $this->user_username, true);
        $criteria->compare('payment_method', $this->payment_method);
        $criteria->compare('status', $this->status);
        $criteria->compare('deal_id', $this->deal_id);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function findByTokenOrTransaction($txnID) {
        return $this->paypal->find('token = :id OR txn_id = :id', array(':id' => $txnID));
    }

    public static function createNew($cart, $method, $data = null) {
        $transaction = new Transaction;
        $transaction->user_id = _profile()->id;
        $transaction->amount = $cart->total;
        $transaction->payment_method = $method;
        $transaction->transaction_date = date(DATE_ISO8601);
        $transaction->status = Transaction::UNVERIFIED;
        $transaction->deal_id = $cart->deal->id;

        if ($transaction->save()) {
            if ($method == PaymentMethod::PAYPAL) {
                $ppt = new PaypalTransaction;
                $ppt->transaction_id = $transaction->id;
                $ppt->save();
            } elseif ($method == PaymentMethod::HELLASPAY) {
                $hpt = new HellaspayTransaction;
                $hpt->transaction_id = $transaction->id;
                $hpt->txn_id = $data['txn_id'];
                $hpt->save();
            }

            return $transaction;
        }
        return false;
    }

    public function cancel() {
        if ($this->deal->updateCouponsBought(count($this->coupons), true)) {
            $this->status = self::FAILED;

            return $this->save();
        }
        return false;
    }

    public function verify() {
        $this->status = self::VERIFIED;

        foreach (_app()->session['coupons'] as $rec => $amount) {
            for ($i = 0; $i < $amount; ++$i) {
                $coupon = new Coupon;
                $coupon->transaction_id = $this->id;
                $coupon->name = $rec;
                $coupon->is_active = true;
                $coupon->save();
            }
        }

        return $this->save(false);
    }

    public function complete() {
        $this->status = self::COMPLETED;

        if ($this->save(false)) {
            $this->deal->updateCouponsBought(count($this->coupons));
            $this->sendCoupons();

            return true;
        }
        return false;
    }

    private function sendCoupons() {
        foreach ($this->coupons as $coupon)
            $coupon->send();

        return true;
    }

    public static function getTotalSavings() {
//        $sql = <<<SQL
//SELECT (SUM(d.original_price) - SUM(d.coupon_price)) * COUNT(c.id) AS total
//    FROM transaction t LEFT JOIN deal d ON d.id = t.deal_id LEFT JOIN coupon c ON c.transaction_id = t.id
//    WHERE t.status = 3
//SQL;
//        $conn = _app()->db;
//        $comm = $conn->cache(600)->createCommand($sql);
//        return $comm->queryScalar();
        $total = 0;
        $trans = Transaction::model()->cache(600)->completed()->findAll();
        foreach ($trans as $t)
            $total += ($t->deal->original_price - $t->deal->coupon_price) * count($t->coupons);

        return $total;
    }

    public static function getSalesByDeal($deal) {
        $sql = <<<SQL
SELECT SUM(amount) AS total
FROM transaction
WHERE deal_id = :deal
AND status = 3
SQL;

        $conn = _app()->db;
        $comm = $conn->createCommand($sql);
        $comm->bindParam(':deal', $deal);

        return $comm->queryScalar();
    }

    public static function getSalesByDate($days) {
        $sql = <<<SQL
SELECT SUM(amount) AS total , DATE_FORMAT(transaction_date, '%d-%m') AS trans_date
FROM transaction
WHERE transaction_date > NOW() - INTERVAL :days DAY
AND status = 3
GROUP BY DATE(transaction_date)
ORDER BY DATE(transaction_date) DESC
SQL;

        $conn = _app()->db;
        $comm = $conn->createCommand($sql);
        $comm->bindParam(':days', $days);

        return $comm->queryAll();
    }

    public static function getSalesByCategory() {
        $sql = <<<SQL
SELECT COUNT(t.id) AS cnt , cat.name AS category

FROM transaction t
LEFT JOIN (deal d LEFT JOIN (subcategory s LEFT JOIN category cat ON cat.id = s.category_id) ON s.id = d.subcategory_id) ON d.id = t.deal_id
WHERE t.status = 3
GROUP BY cat.name
ORDER BY cat.name DESC
SQL;

        $conn = _app()->db;
        $comm = $conn->createCommand($sql);

        return $comm->queryAll();
    }

    public static function getSalesByPaymentMethod() {
        $sql = <<<SQL
SELECT COUNT(t.id) AS cnt , payment_method AS method
FROM transaction t
WHERE t.status = 3
GROUP BY payment_method
ORDER BY payment_method DESC
SQL;

        $conn = _app()->db;
        $comm = $conn->createCommand($sql);

        return $comm->queryAll();
    }

    public function getDetails() {
        return $this->deal->name . ' - ' . $this->user->email . ' - ' . Utils::date($this->transaction_date);
    }

}
