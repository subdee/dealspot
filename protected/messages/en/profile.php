<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 *
 * @version $Id: $
 */
return array (
  'HellasPay' => '',
  'PayPal' => '',
  'account' => '',
  'active' => '',
  'amount' => '',
  'amount paid' => '',
  'available' => '',
  'bank' => '',
  'birthday' => '',
  'bought' => '',
  'code' => '',
  'company' => '',
  'coupon code' => '',
  'date bought' => '',
  'date of sale' => '',
  'date used' => '',
  'deal' => '',
  'deal purchase' => '',
  'deals' => '',
  'edit text' => '',
  'editable tooltip' => '',
  'email' => '',
  'empty.birthdate' => '',
  'end date' => '',
  'expiration' => '',
  'full name' => '',
  'gender' => '',
  'here\'s your code for resetting your password<br/>code: {code}' => '',
  'inactive' => '',
  'invitation' => '',
  'invites' => '',
  'manager' => '',
  'mobile' => '',
  'my company' => '',
  'name' => '',
  'new password' => '',
  'newsletter' => '',
  'no' => '',
  'password change' => '',
  'password reset email subject' => '',
  'payment method' => '',
  'point purchase' => '',
  'points' => '',
  'points expire' => '',
  'print' => '',
  'profile' => '',
  'reason' => '',
  'refund' => '',
  'registered' => '',
  'repeat password' => '',
  'sales' => '',
  'save' => '',
  'start date' => '',
  'status' => '',
  'tableheader.invite status' => '',
  'tableheader.invited on' => '',
  'tableheader.invitee' => '',
  'total points' => '',
  'transaction date' => '',
  'unused' => '',
  'used' => '',
  'yes' => '',
  'Paysafe' => '@@@@',
  'credit card' => '@@@@',
  'details' => '@@@@',
);
