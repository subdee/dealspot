<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 *
 * @version $Id: $
 */
return array (
  'You have succesfully registered for our newsletter' => '',
  'click here to close this window' => '',
  'common questions' => '',
  'contact' => '',
  'contact form' => '',
  'days' => '',
  'email:' => '',
  'enter friends email' => '',
  'female' => '',
  'fields with * are required' => '',
  'footer copyright by {operators}' => '',
  'for more info contact us at 123123123' => '',
  'how it works subtitle' => '',
  'how it works subtitle 1' => '',
  'how it works subtitle 2' => '',
  'how it works subtitle 3' => '',
  'how it works title' => '',
  'how it works title 1' => '',
  'how it works title 2' => '',
  'how it works title 3' => '',
  'if you already subscribed to the newsletter, click here to close this window' => '',
  'index title append' => '',
  'invite' => '',
  'invite a friend' => '',
  'invite friend' => '',
  'invite your friends' => '',
  'invite your friends now and when they make purchase 20, you get 200 points!' => '',
  'male' => '',
  'newsletter registration' => '',
  'other' => '',
  'problem with my account' => '',
  'problem with my coupon' => '',
  'problem with the website' => '',
  'question about daily deal' => '',
  'reasons to work with us' => '',
  'register' => '',
  'register for newsletter' => '',
  'register now to get updates on all deals with coupons up to 90%' => '',
  'send' => '',
  'terms of use' => '',
  'text for companies contact' => '',
  '%d day ago' => '@@@@',
  '%d day ago at %H:%i' => '@@@@',
  '%d days ago' => '@@@@',
  '%d days ago at %H:%i' => '@@@@',
  '%h hour ago' => '@@@@',
  '%h hours ago' => '@@@@',
  '%i minute ago' => '@@@@',
  '%i minutes ago' => '@@@@',
  '%m months and %d days ago' => '@@@@',
  '%m months and %d days ago at %H:%i' => '@@@@',
  '%s second ago' => '@@@@',
  '%s seconds ago' => '@@@@',
  'click {url}here{/url} to unsubscribe or unsubscribe in your profile if you are a member' => '@@@@',
  'discount' => '@@@@',
  'enter your email address' => '@@@@',
  'gain' => '@@@@',
  'see it' => '@@@@',
  'some random text' => '@@@@',
  'today ' => '@@@@',
  'unsubscribe text' => '@@@@',
  'value' => '@@@@',
  'yesterday ' => '@@@@',
);
