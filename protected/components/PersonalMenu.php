<?php

class PersonalMenu extends CWidget {
    
    public function init() {
        if (Utils::isLoggedIn())
            _profile()->refresh();
    }
    
    public function run() {
        $this->render('personalMenu');
    }
    
}