<?php

class RegistrationForm extends CWidget {
    
    public $user = null;
    
    public $userDetails = null;
    
    public $action = null;
    private $_actionUrl;
    
    public function init() {
        if (!$this->action || $this->action == 'registration')
            $this->_actionUrl = _url('user/register');
        if ($this->action == 'facebook')
            $this->_actionUrl = _url('hybridauth/default/login', array('provider' => 'Facebook'));
    }
    
    public function run() {
        $this->render('registrationForm', array(
            'user' => $this->user,
            'userDetails' => $this->userDetails,
            'action' => $this->action,
            'actionUrl' => $this->_actionUrl,
        ));
    }
    
}