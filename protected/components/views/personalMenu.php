<div class="span-2 last personalmenu text-right">
    <?php if (Utils::isLoggedIn()) : ?>
        <?php
        echo CHtml::link(_t('menu', 'greeting to {username} you have {points} points', array(
                    '{username}' => _profile()->username,
                    '{points}' => CHtml::tag('span', array('class' => 'points-bubble', 'original-title' => Utils::currency(_profile()->details->points / 100)), _profile()->details->points),
                )), '#', array('id' => 'personal-link'));
        ?>
        <ul class="personal-menu">
            <li><?php echo CHtml::link(_t('menu', 'profile'), _url('user/profile')); ?></li>
            <li><?php echo CHtml::link(_t('menu', '{n} deal|{n} deals', array(count(_profile()->transactions))), _url('user/profile', array('view' => 'deals'))); ?></li>
            <li><?php echo CHtml::link(_t('menu', 'invites'), _url('user/profile', array('view' => 'invites'))); ?></li>
            <?php if (User::hasRole(Role::CLIENT)) : ?>
                <li><?php echo CHtml::link(_t('menu', 'company'), _url('user/profile', array('view' => 'company'))); ?></li>
    <?php endif; ?>
            <li><?php echo CHtml::link(_t('menu', 'logout'), _url('user/logout')); ?></li>
        </ul>
    <?php else : ?>
        <?php echo CHtml::link(_t('menu', 'login or register'), _url('user/login')); ?>
<?php endif; ?>
</div>
<?php
$this->widget('application.extensions.tipsy.Tipsy', array(
    'gravity' => 'n',
    'fade' => true,
    'items' => array(
        array('id' => '.points-bubble'),
    ),
));
?>
