<?php if ($theme == 'index') : ?>
    <span class="countdown-div">
        <div class="days">
            <?php echo _app()->numberFormatter->format('00', $timeLeft->d); ?>
            <div><?php echo _t('widgets', 'days'); ?></div>
        </div>
        <div class="colon">:</div>
        <div class="hours">
            <?php echo _app()->numberFormatter->format('00', $timeLeft->h); ?>
            <div><?php echo _t('widgets', 'hours'); ?></div>
        </div>
        <div class="colon">:</div>
        <div class="mins">
            <?php echo _app()->numberFormatter->format('00', $timeLeft->i); ?>
            <div><?php echo _t('widgets', 'mins'); ?></div>
        </div>
    </span>
<?php elseif ($theme == 'deal') : ?>
    <div class="countdown-deal-div">
        <?php if (!$timeLeft) : ?>
            <div class="text"><?php echo _t('widgets', 'expired deal'); ?></div>
        <?php else : ?>
            <div class="text"><?php echo _t('widgets', 'deal expires in'); ?></div>
            <div class="mins">
                <div class="times"><?php echo _app()->numberFormatter->format('00', $timeLeft->i); ?></div>
                <div class="times-text"><?php echo _t('widgets', 'mins'); ?></div>
            </div>
            <div class="hours">
                <div class="times"><?php echo _app()->numberFormatter->format('00', $timeLeft->h); ?></div>
                <div class="times-text"><?php echo _t('widgets', 'hours'); ?></div>
            </div>
            <div class="days">
                <div class="times"><?php echo _app()->numberFormatter->format('00', $timeLeft->d); ?></div>
                <div class="times-text"><?php echo _t('widgets', 'days'); ?></div>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
