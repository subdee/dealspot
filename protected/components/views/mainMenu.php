<ul class="main-menu">
    <?php $i = 0; ?>
<?php foreach($this->menuItems as $item) : ?>
    <?php if (!$i) {$first = 'first-item';} else {$first = '';} ?>
    <?php echo CHtml::link(CHtml::tag('li', array('class' => $first), 
            $item['icon'] . $item['text']), $item['url']); ?>
    <?php ++$i; ?>
<?php endforeach; ?>
</ul>