<div class="span-2 last">
    <div class="sidebar-deals append-bottom">
        <div class="header"><?php echo _t('deals', 'related deals'); ?></div>
        <div class="deals">
            <?php $i = 1; ?>
            <?php foreach ($deals as $deal) : ?>
                <div><?php echo $deal->details->coupon_instructions; ?></div>
                <div><?php echo CHtml::link(
                        CHtml::image(Utils::imageUrl('deals/' . $deal->small_image)), _url('deal/index', array('id' => $deal->id))); ?></div>
                <div class="buy-button">
                    <?php
                    echo CHtml::link(
                            CHtml::image(Utils::imageUrl('right-small-check.png')) . _t('deals', '{n} purchase|{n} purchases', array($deal->coupons_bought)), _url('deal/index', array('id' => $deal->id)));
                    ?>
                </div>
                <?php if ($i != count($deals)) : ?>
                    <hr/>
                <?php endif; ?>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="sidebar-deals append-bottom">
        <div class="header"><?php echo _t('deals', 'other deals'); ?></div>
        <div class="deals">
            <?php $i = 1; ?>
            <?php foreach ($dealsCategory as $deal) : ?>
                <div><?php echo $deal->details->coupon_instructions; ?></div>
                <div><?php echo CHtml::link(
                        CHtml::image(Utils::imageUrl('deals/' . $deal->small_image)), _url('deal/index', array('id' => $deal->id))); ?></div>
                <div class="buy-button">
                    <?php
                    echo CHtml::link(
                            CHtml::image(Utils::imageUrl('right-small-check.png')) . _t('deals', '{n} purchase|{n} purchases', array($deal->coupons_bought)), _url('deal/index', array('id' => $deal->id)));
                    ?>
                </div>
                    <?php if ($i != count($deals)) : ?>
                    <hr/>
                <?php endif; ?>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>