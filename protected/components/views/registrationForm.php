<div class="reg-form">
    <h2><?php echo _t('user', 'register now'); ?></h2>
    <div class="form">
        <?php
        $regform = $this->beginWidget('CActiveForm', array(
            'id' => 'register-form',
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'action' => $actionUrl,
                ));
        ?>

        <?php echo $regform->errorSummary($user); ?>

        <?php if (User::getRegisteredUsers() < 200) : ?>
            <div class="info">
                <?php echo _t('user', 'you can still be one of the first 200 people to register'); ?>
            </div>
        <?php endif; ?>

        <div class="left">
            <div class="row">
                <?php echo $regform->labelEx($user, 'username'); ?>
                <?php echo $regform->textField($user, 'username'); ?>
            </div>
            <?php if (!$action || $action == 'registration') : ?>
                <div class="row">
                    <?php echo $regform->labelEx($user, 'password'); ?>
                    <p class="form-note"><?php echo _t('user', 'password must contain at least 7 characters, one digit'); ?></p>
                    <?php echo $regform->passwordField($user, 'password'); ?>
                </div>

                <div class="row">
                    <?php echo $regform->labelEx($user, 'password2'); ?>
                    <?php echo $regform->passwordField($user, 'password2'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <?php echo $regform->labelEx($userDetails, 'fullname'); ?>
                <?php echo $regform->textField($userDetails, 'fullname'); ?>
            </div>
            <div class="row remember-me">
                <?php echo $regform->checkBox($user, 'terms_accept'); ?>
                <?php echo $regform->labelEx($user, 'terms_accept'); ?>
            </div>
        </div>
        <div class="right">
            <div class="row">
                <?php echo $regform->labelEx($user, 'email'); ?>
                <?php echo $regform->textField($user, 'email'); ?>
            </div>

            <div class="row">
                <?php echo $regform->labelEx($user, 'email2'); ?>
                <?php echo $regform->textField($user, 'email2'); ?>
            </div>
            <div class="row">
                <?php echo $regform->labelEx($userDetails, 'gender'); ?>
                <?php echo $regform->dropDownList($userDetails, 'gender', Gender::getGenders()); ?>
            </div>
            <div class="row">
                <?php echo $regform->labelEx($userDetails, 'mobile'); ?>
                <?php echo $regform->textField($userDetails, 'mobile'); ?>
            </div>
            <div class="row">
                <?php echo $regform->labelEx($userDetails, 'birthdate'); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $userDetails,
                    'attribute' => 'birthdate',
                    'themeUrl' => _bu('css/jqueryui'),
                    'theme' => 'dealspot',
                    'options' => array(
                        'showAnim' => 'fold',
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '-100:+0',
                        'maxDate' => 'today',
                        'dateFormat' => 'dd-mm-yy',
                        'altField' => '#fake-date',
                    ),
                    'language' => _app()->language,
                    'htmlOptions' => array(
                        'style' => 'height:20px;',
                    ),
                ));
                ?>
            </div>
        </div>

        <div class="clear"></div>

        <div class="row buttons">
            <?php echo CHtml::imageButton(Utils::imageUrl('register.jpg'), array('onclick' => '$("#register-form").submit();', 'class' => 'image-button')); ?>
        </div>

        <?php $this->endWidget(); ?>

        <div class="right"><?php echo _t('user', 'the fields with * are mandatory'); ?></div>
    </div>
</div>