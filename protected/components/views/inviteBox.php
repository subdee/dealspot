<h3><?php echo _t('widgets', 'invite box header'); ?></h3>
<div class="invite-box-text"><?php echo _t('widgets', 'invite box top text'); ?></div>
<div class="invite-box-image"><a href="<?php echo _url('site/companies'); ?>"><?php echo CHtml::image(Utils::imageUrl('case.png')); ?></a></div>
<div class="invite-box-text"><?php echo _t('widgets', 'invite box bottom text'); ?></div>