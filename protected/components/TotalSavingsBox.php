<?php

class TotalSavingsBox extends CWidget {

    private $totalSavings;

    public function init() {
        $this->totalSavings = Transaction::getTotalSavings();
        if ($this->totalSavings)
            $this->totalSavings = Utils::currency($this->totalSavings);
        else
            $this->totalSavings = Utils::currency(0);
    }

    public function run() {
        $this->render('totalSavingsBox', array('totalSavings' => $this->totalSavings));
    }

}