<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {
    public $layout = '//layouts/main';

    public function init() {
        parent::init();
        $this->pageTitle = 'Dealspot.gr';
    }

    public function beforeAction($action) {
        parent::beforeAction($action);

        /***SHOW PLACEHODER BEFORE LAUNCH. REMOVE AFTER LAUNCH***/
//        if (!$this->module || $this->module->id != 'admin') {
//            if ($this->action->id != 'placeholder' && $this->action->id != 'newsletter') {
//                $this->redirect(_url('site/placeholder'));
//                _app()->end();
//            }
//        }
        /********************************************************/

        return true;
    }

    public function preloadModules() {
        foreach (_app()->getModules() as $name => $module) {
            if (isset($module['autoinit']) && $module['autoinit'] === true) {
                _app()->getModule($name);
            }
        }
    }

    public function filterHttps($filterChain) {
        $filter = new HttpsFilter;
        $filter->filter($filterChain);
    }

    public function filterAccessControl($filterChain) {
        $filter = new AccessControlFilter;
        $filter->setRules($this->accessRules());
        $filter->filter($filterChain);
    }

}