<?php

class SidebarDeals extends CWidget {
    
    private $deals;
    private $dealsCategory;
    public $categoryID = false;
    
    public function init() {
        $this->dealsCategory = Deal::getRandomDeals(2, $this->categoryID);
        $this->deals = Deal::getRandomDeals(2);
    }
    
    public function run() {
        $this->render('sidebarDeals', array('deals' => $this->deals, 'dealsCategory' => $this->dealsCategory));
    }
    
}