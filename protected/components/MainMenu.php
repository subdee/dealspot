<?php

class MainMenu extends CWidget {

    public $isSubcategory = false;
    public $categoryId = null;
    public $menuItems;

    public function init() {

        if ($this->isSubcategory) {
            $categories = Subcategory::model()->findAll('category_id = :id', array(':id' => $this->categoryId));
            $this->menuItems[0]['url'] = _url('');
            $this->menuItems[0]['text'] = _t('menu', 'all items for category {category}', array('{category}' => $categories[0]->category->name));
            $this->menuItems[0]['icon'] = '';
        } else {
            $categories = Category::model()->withDeals()->findAll();
            $this->menuItems[0]['url'] = _url('site/index');
            $this->menuItems[0]['text'] = _t('menu', 'all categories') . '<span class="category-count">' . Category::allActiveDeals() . '</span>';
            $this->menuItems[0]['icon'] = CHtml::tag('span', array('class' => 'menu-item-icon'),
                    CHtml::image(Utils::imageUrl('/categories/all_categories.png')));
        }
        $i = 1;
        foreach ($categories as $category) {
            $this->menuItems[$i]['url'] = _url('site/index', array('category' => $category->id));
            $this->menuItems[$i]['text'] = $category->name . '<span class="category-count">' . $category->noOfDeals . '</span>';
            $this->menuItems[$i]['icon'] = CHtml::tag('span', array('class' => 'menu-item-icon'),
                    CHtml::image(Utils::imageUrl('/categories/'.$category->image)));
            ++$i;
        }
    }

    public function run() {
        $this->render('mainMenu');
    }

}