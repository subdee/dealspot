<?php

/**
 * Description of AccessControlFilter
 *
 * @author kthermos
 */
class AccessControlFilter extends CAccessControlFilter {

    private $_rules = array();

    protected function preFilter($filterChain) {
        $app = Yii::app();
        $request = $app->getRequest();
        $user = $app->getUser();
        $verb = $request->getRequestType();
        $ip = $request->getUserHostAddress();
        foreach ($this->getRules() as $rule) {
            if (($allow = $rule->isUserAllowed($user, $filterChain->controller, $filterChain->action, $ip, $verb)) > 0) // allowed
                break;
            else if ($allow < 0) {
//                var_dump($rule); die();
                if ($rule->redirect)
                    $request->redirect($app->createUrl($rule->redirect[0]));
                else
                    $this->accessDenied($user,$this->resolveErrorMessage($rule));
                return false;
            }
        }

        return true;
    }

    public function setRules($rules) {
        foreach ($rules as $rule) {
            if (is_array($rule) && isset($rule[0])) {
                $r = new AccessRule;
                $r->allow = $rule[0] === 'allow';
                foreach (array_slice($rule, 1) as $name => $value) {
                    if ($name === 'expression' || $name === 'roles' || $name === 'message')
                        $r->$name = $value;
                    else
                        $r->$name = array_map('strtolower', $value);
                }
                $this->_rules[] = $r;
            }
        }
    }

    public function getRules() {
        return $this->_rules;
    }

}
