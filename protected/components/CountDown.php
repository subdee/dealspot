<?php

class CountDown extends CWidget {

    public $time_left;
    public $theme = 'index';

    public function init() {
    }

    public function run() {
        $this->render('countDown', array('theme' => $this->theme, 'timeLeft' => $this->time_left));
    }

}