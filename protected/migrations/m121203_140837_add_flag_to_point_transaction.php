<?php

class m121203_140837_add_flag_to_point_transaction extends CDbMigration {

    public function safeUp() {
        $this->addColumn('point_transaction', 'are_redeemable', 'tinyint(1) DEFAULT 0');
    }

    public function safeDown() {
        $this->dropColumn('point_transaction', 'are_redeemable');
    }

}