<?php

class m121207_133924_add_mobile_to_company extends CDbMigration {

    public function safeUp() {
        $this->addColumn('company', 'mobile', 'varchar(20)');
    }

    public function safeDown() {
        $this->dropColumn('company', 'mobile');
    }

}