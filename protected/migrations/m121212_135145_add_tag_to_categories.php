<?php

class m121212_135145_add_tag_to_categories extends CDbMigration {

    public function safeUp() {
        $this->addColumn('subcategory', 'tag', 'varchar(255)');
    }

    public function safeDown() {
        $this->dropColumn('subcategory', 'tag');
    }

}