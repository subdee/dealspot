<?php
class UserController extends Controller {
    public $layout = '//layouts/basic';

    public function filters() {
        return array(
            'accessControl',
//            'https',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'login', 'register', 'activate', 'forgotPassword', 'resetCode', 'resetPassword', 'changePassword', 'submitPassword', 'test'),
                'users' => array('*'),
            ),
            array('allow',
                'expression' => 'Utils::isLoggedIn()',
            ),
            array('deny', // deny all users
                'users' => array('*'),
                'redirect' => array('site/index'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionLogin() {
        if (!_user()->isGuest)
            $this->redirect(_url('site/index'));
        $model = new LoginForm;
        $user = new User;
        $userDetails = new UserDetails;

        if (isset($_GET['invitation_code'])) {
            _app()->session['invite_code'] = $_GET['invitation_code'];
            $invitation = UserInvite::model()->find('code = :code', array(':code' => $_GET['invitation_code']));
            if ($invitation)
                $user->email = $user->email2 = $invitation->invited_email;
        }

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate() && $model->login())
                $this->redirect(_user()->returnUrl);
        }
        $this->render('login', array('model' => $model, 'user' => $user, 'userDetails' => $userDetails));
    }

    public function actionRegister() {
        if (isset($_POST['User']) && isset($_POST['UserDetails'])) {
            $user = new User;
            $userDetails = new UserDetails;
            $user->attributes = $_POST['User'];
            $userDetails->attributes = $_POST['UserDetails'];
            $user->role = Role::USER;

            if ($user->save()) {
                $userDetails->user_id = $user->id;
                if ($userDetails->save()) {
                    if ($user->source == UserSource::FACEBOOK) {
                        $login = new LoginForm;
                        $login->username = $user->username;
                        $login->password = $_POST['User']['password'];
                        if ($login->login())
                            $this->redirect(_user()->returnUrl);
                    } else {
                        $user->createActivationCode();
                        $this->render('awaiting_activation');
                        Yii::app()->end();
                    }
                }
            } else {
                $model = new LoginForm;
                $this->render('login', array('model' => $model, 'user' => $user, 'userDetails' => $userDetails));
            }
        }
    }

    public function actionActivate($code) {
        $userAct = UserActivation::model()->verifyCode($code);
        if ($userAct) {
            $userAct->delete();

            $this->render('activation_success');
            _app()->end();
//            $login = new LoginForm;
//            $login->username = $user->username;
//            if ($login->login())
//                $this->redirect(_user()->returnUrl);
        }
        $this->render('activation_failed');
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(_url('site/index'));
    }

    public function actionInvite() {
        if (Utils::isAjax() && isset($_POST['invitee'])) {
            $response = array();
            if (_profile())
                $response = _profile()->sendInviteEmail($_POST['invitee']);
            Utils::jsonReturn($response);
        }
        return;
    }

    public function actionProfile($view = 'details', $companyDetails = false) {

        if (isset($_POST['pass']))
            _profile()->changePassword($_POST['pass']['new'], $_POST['pass']['repeat']);

        _profile()->refresh();
        Utils::registerJsFile('jquery.jeditable.mini', CClientScript::POS_BEGIN);
        Utils::registerJsFile('jeditable.checkbox', CClientScript::POS_BEGIN);

        $selected = array(
            'deals' => '',
            'invites' => '',
            'points' => '',
            'details' => '',
            'company' => '',
        );
        switch ($view) {
            case 'deals':
                $selected['deals'] = 'show';
                break;
            case 'invites':
                $selected['invites'] = 'show';
                break;
            case 'points':
                $selected['points'] = 'show';
                break;
            case 'company':
                $selected['company'] = 'show';
                break;
            case 'details':
            default:
                $selected['details'] = 'show';
                break;
        }

        $details = $this->renderPartial('_details', null, true);
        $deals = $this->renderPartial('_deals', null, true);
        $invites = $this->renderPartial('_invites', null, true);
        $points = $this->renderPartial('_points', null, true);
        if (User::hasRole(Role::CLIENT))
            $company = $this->renderPartial('_company', array('id' => $companyDetails), true);
        else
            $company = false;

        $this->render('profile', array('selected' => $selected, 'details' => $details, 'deals' => $deals, 'invites' => $invites, 'points' => $points, 'company' => $company));
    }

    public function actionDeals() {
        $this->renderPartial('_deals');
    }

    public function actionDetails() {
        $this->renderPartial('_details');
    }

    public function actionInvites() {
        $this->renderPartial('_invites');
    }

    public function actionPoints() {
        $this->renderPartial('_points');
    }

    public function actionCompany() {
        $this->renderPartial('_company', array('id' => false));
    }

    public function actionUpdateProfile() {
        if (!Utils::isAjax())
            return;

        if (!isset($_POST['id']))
            return;

        $attribute = $_POST['id'];

        if (isset(_profile()->$attribute)) {
            $oldValue = _profile()->$attribute;

            if (_profile()->$attribute != $_POST['value']) {

                // Update only if value is different
                _profile()->$attribute = $_POST['value'];
                if (!_profile()->save(false, array($attribute))) {
                    var_dump(_profile()->getErrors());
                }

                // After refresh, the old values will return if the udpate failed.
                _profile()->refresh();
            }

            switch ($attribute) {
                default:
                    echo _profile()->$attribute;
                    break;
            }
        } else {
            $oldValue = _profile()->details->$attribute;

            if (_profile()->details->$attribute != $_POST['value']) {

                // Update only if value is different
                _profile()->details->$attribute = $_POST['value'];

                if ($attribute == 'newsletter') {
                    _profile()->details->$attribute = $_POST['value'] == _t('profile', 'yes') ? 1 : 0;
                }

                if (!_profile()->details->save(true, array($attribute))) {
                    var_dump(_profile()->details->getErrors());
                }

                // After refresh, the old values will return if the udpate failed.
                _profile()->details->refresh();
            }

            switch ($attribute) {
                case 'birthdate':
                    echo _profile()->details->birthdate;
                    break;
                case 'gender':
                    echo Gender::toText(_profile()->details->gender);
                    break;
                case 'newsletter':
                    echo _profile()->details->newsletter ? _t('profile', 'yes') : _t('profile', 'no');
                    break;
                default:
                    echo _profile()->details->$attribute;
                    break;
            }
        }

        _app()->end();
    }

    public function actionDealDetails($id) {
        $deal = Deal::model()->findByPk($id);

        $this->renderPartial('_dealDetails', array('coupons' => $deal->allCoupons));
    }

    public function actionCoupon($id) {
        $coupon = Coupon::model()->findByPk($id);
        if ($coupon && _profile()) {
            $coupon->is_active = !$coupon->is_active;
            $coupon->date_activated = date(DATE_ISO8601);
            $coupon->user_id = _profile()->id;
            $coupon->save();
        }

        $this->redirect(_url('user/profile', array('view' => 'company', 'companyDetails' => $coupon->transaction->deal->id)));
    }

    public function actionPrintCoupon($id) {
        $coupon = Coupon::model()->findByPk($id);
        if (!$coupon || ($coupon && _profile()->id != $coupon->transaction->user->id))
            $this->redirect(_url('site/index'));

        $this->layout = 'print';
        $this->render('coupon', array('coupon' => $coupon));
    }

    public function actionForgotPassword() {
        $this->render('forgotPasswordForm');
    }

    public function actionResetCode() {
        if (isset($_POST['email'])) {
            $user = User::model()->find('email = :email', array(':email' => $_POST['email']));
            if ($user && $user->createResetCode()) {
                $this->render('resetCode');
                _app()->end();
            }
        }
        _user()->setFlash('error', _t('user', 'error message for user not found'));
        $this->redirect(_url('user/forgotPassword'));
    }

    public function actionChangePassword() {
        if (isset($_REQUEST['reset_code'])) {
            $upr = UserPasswordReset::verifyByCode($_REQUEST['reset_code']);
            if ($upr) {
                $this->render('changePassword', array('user' => $upr->user));
                _app()->end();
            }
        }
        _user()->setFlash('error', _t('user', 'wrong reset code'));
        $this->redirect(_url('user/forgotPassword'));
    }

    public function actionSubmitPassword() {
        if (isset($_POST['User'])) {
            $user = User::model()->findByPk($_POST['User']['id']);
            if ($user) {
                $user->attributes = $_POST['User'];
                $user->email2 = $user->email;
                $user->terms_accept = true;
                if ($user->save()) {
                    $this->redirect(_url('user/login'));
                } else {
                    $this->render('changePassword', array('user' => $user));
                    _app()->end();
                }
            }
        }
        $this->redirect(_url('user/forgotPassword'));
    }

    public function actionTest() {
        $this->render('activation_success');
    }

}
