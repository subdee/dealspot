<?php

class ShopController extends Controller {

    public $layout = '//layouts/basic';

    public function filters() {
        return array(
            'accessControl',
//            'https',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'users' => array('*'),
            ),
//            array('deny', // deny all users
//                'users' => array('*'),
//                'redirect' => array('admin/main/login'),
//            ),
        );
    }

    public function actionIndex($id) {
        $this->redirect('cart');
    }

    public function actionAddToCart($id, $amount = 1, $gift = false) {
        $deal = Deal::model()->findByPk($id);
        if ($deal) {
            if ($deal->hasUserReachedLimit()) {
                _user()->setFlash('notavailable', _t('shop', 'you have reached the maximum purchases allowed for this deal'));
            } elseif ($deal->available_coupons - $deal->coupons_bought < 1) {
                _user()->setFlash('notavailable', _t('shop', 'there are no more coupons available for this deal'));
            } else {
                isset(_app()->session['cart']) ? $cart = unserialize(_app()->session['cart']) : $cart = new Cart;
                $cart->add($deal, $amount);
                _app()->session['cart'] = serialize($cart);
            }
        }

        if ($gift) {
            _app()->session['gift'] = true;
        } else {
            if (isset(_app()->session['gift']))
                unset(_app()->session['gift']);
        }

        if (_user()->isGuest) {
            _user()->returnUrl = _url('shop/cart');
            $this->redirect(_url('user/login'));
        } else {
            $this->redirect('cart');
        }
    }

    public function actionCart() {
        $cart = isset(_app()->session['cart']) ? unserialize(_app()->session['cart']) : false;
        
        $gift = false;
        if (isset(_app()->session['gift-form'])) {
            $gift = unserialize(_app()->session['gift-form']);
            unset(_app()->session['gift-form']);
        } elseif (isset(_app()->session['gift'])) {
            $gift = new GiftForm;
        }

        $this->render('cart', array('cart' => $cart, 'gift' => $gift));
    }

    public function actionIncreaseAmount($id, $amount) {
        $deal = Deal::model()->findByPk($id);
        isset(_app()->session['cart']) ? $cart = unserialize(_app()->session['cart']) : $cart = new Cart;
        $cart->add($deal, $amount);
        _app()->session['cart'] = serialize($cart);

        Utils::jsonReturn(array(
            'status' => true,
            'price' => Utils::currency($amount * $deal->coupon_price),
            'total' => Utils::currency($cart->total),
            'recipients' => $this->recipientFields($amount),
        ));
    }

    public function actionPay() {
        if (isset($_POST['recipients']))
            $this->_storeCouponRecipients();

        if (isset($_POST['GiftForm']))
            $this->_storeGiftDetails();

        $factory = new CPaymentFactory;
        $p = $factory->build($_POST['type']);
        if (!$p->create(unserialize(_app()->session['cart'])))
            $this->redirect('cart');
    }

    public function actionVerify() {
        $factory = new CPaymentFactory;
        $p = $factory->build();
        if (!$p)
            $this->render('failure');
        $response = $p->verify($_REQUEST);
        if ($response) {
            $this->render('verify', array('response' => $response, 'cart' => unserialize(_app()->session['cart'])));
        } else {
            $this->render('failure');
        }
    }

    public function actionFailure() {
        $factory = new CPaymentFactory;
        $p = $factory->build();
        if (!$p)
            $this->render('failure');
        $response = $p->cancel($_REQUEST);
        $this->render('failure', array('response' => $response));
    }

    public function actionProcess() {
        $factory = new CPaymentFactory;
        $p = $factory->build();
        if (!$p)
            $this->render('failure');
        $response = $p->process($_REQUEST);
        if ($response) {
            $amount = $id = 0;
            if (isset(_app()->session['amount']) && isset(_app()->session['id'])) {
                $amount = _app()->session['amount'];
                $id = _app()->session['id'];
            }

            $this->render('success', array('response' => $response, 'amount' => $amount / 1.23, 'id' => $id));
        } else {
            $this->render('failure', array('response' => $response));
        }
    }

    public function actionCancel() {
        PPayment::cancel();
        $this->redirect('cart');
    }

    public function actionIpn() {
        CPaypalPayment::ipn($this);
    }

    public function actionCoupon() {
        $this->renderPartial('_coupon', array(
            'couponCode' => 'AS9757SD9759VDF',
            'deal' => Deal::model()->find(),
        ));
    }

    private function recipientFields($amount) {
        return $this->renderPartial('_recipientFields', array('amount' => $amount, 'name' => _profile()->details->fullname), true);
    }

    private function _storeCouponRecipients() {
        if (isset($_POST['sameRecipient']) && $_POST['recipients'][0] != '') {
            $recps = array($_POST['recipients'][0] => unserialize(_app()->session['cart'])->amount);
        } else {
            $recps = array();
            foreach ($_POST['recipients'] as $rec) {
                if ($rec != '') {
                    $recps[$rec] = isset($recps[$rec]) ? $recps[$rec] + 1 : 1;
                } else {
                    _user()->setFlash('error', _t('shop', 'recipient must not be empty'));
                    $this->redirect(_url('shop/cart'));
                }
            }
        }
        _app()->session['coupons'] = $recps;
    }

    private function _storeGiftDetails() {
        $gift = new GiftForm;
        $gift->attributes = $_POST['GiftForm'];
        
        if ($gift->validate()) {
            _app()->session['coupons'] = array($_POST['GiftForm']['fullname'] => 1);
            _app()->session['gift'] = $_POST['GiftForm'];
            return true;
        } else {
            _app()->session['gift-form'] = serialize($gift);
            $this->redirect(_url('shop/cart'));
        }
    }

}
