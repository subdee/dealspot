<?php

class SiteController extends Controller {

    public $layout = '//layouts/basic';

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'users' => array('*'),
            ),
//            array('deny', // deny all users
//                'users' => array('*'),
//                'redirect' => array('user/login'),
//            ),
        );
    }

    public function actionIndex() {
        $this->layout = '//layouts/site';

        $this->pageTitle = $this->pageTitle . ' | ' . _t('main', 'index title append');

        $criteria = new CDbCriteria;
//        $cityId = '';
//        if (isset($_GET['id'])) {
//            $criteria->compare('city_id', $_GET['id']);
//            $cityId = $_GET['id'];
//        }
        if (isset($_GET['category'])) {
            $criteria->with = 'subcategory';
            $criteria->compare('subcategory.category_id', $_GET['category']);
        }

        $deals = Deal::model()->active()->ordered()->findAll($criteria);

        $this->render('index', array('deals' => $deals));
    }

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionNewsletter() {
        if (Utils::isAjax() && isset($_POST['email'])) {
            $newsletter = new NewsletterUsers;
            $newsletter->email = $_POST['email'];
            if ($newsletter->save())
                Utils::jsonReturn(array('status' => true, 'message' => _t('main', 'You have succesfully registered for our newsletter')));
            else
                Utils::jsonReturn(array('status' => false, 'message' => $newsletter->getError('email')));
        }
        return;
    }

    public function actionAbout() {
        $this->render('about');
    }

    public function actionContact() {
        $contact = new ContactForm;

        if (isset($_POST['ContactForm'])) {
            $contact->attributes = $_POST['ContactForm'];
            if ($contact->save())
                $this->redirect('index');
        }

        $this->render('contact', array('contact' => $contact));
    }

    public function actionHowItWorks() {
        $this->render('howItWorks');
    }

    public function actionJobs() {
        $this->render('jobs');
    }

    public function actionFaq() {
        $this->render('faq');
    }

    public function actionCompanies() {
        $form = new CompaniesForm;

        if (isset($_POST['CompaniesForm'])) {
            $form->attributes = $_POST['CompaniesForm'];
            if ($form->save())
                $this->redirect('index');
        }

        $this->render('companies', array('company' => $form));
    }

    public function actionTermsOfUse() {
        $this->render('termsOfUse');
    }

    public function actionPrivacyPolicy() {
        $this->render('privacyPolicy');
    }

    public function actionTest() {
        $this->renderPartial('_newsletter', array('deals' => Deal::getRandomDeals(8)));
    }

    public function actionFeed() {
        $dealXML = Deal::createXML();
        header('Content-type: text/xml');
        $this->renderPartial('_feed', array('dealXML' => $dealXML));
    }

    public function actionPlaceholder() {
//        $this->layout = 'placeholder';
//        $this->render('placeholder');
        $this->redirect(_url('site/index'));
    }

    public function actionUnsubscribe($email) {
        $nu = NewsletterUsers::model()->findByAttributes(array('email' => $email));
        if ($nu)
            $nu->delete();

        $this->redirect(_url('site/index'));
    }

}