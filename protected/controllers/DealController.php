<?php

class DealController extends Controller {

    public $layout = '//layouts/deal';
    
    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'users' => array('*'),
            ),
//            array('deny', // deny all users
//                'users' => array('*'),
//                'redirect' => array('admin/main/login'),
//            ),
        );
    }

    public function actionIndex($id) {
        $deal = Deal::model()->findByPk($id);
        $map = $this->getGoogleMap($deal->company->coordinates);
        
        $this->pageTitle = strip_tags(html_entity_decode($deal->details->coupon_instructions, ENT_COMPAT, 'UTF-8')) . ' | ' . $this->pageTitle;

        $this->render('index', array('deal' => $deal, 'map' => $map));
    }

    public function getGoogleMap($latlong) {
        Yii::import('ext.egmap.*');

        $gMap = new EGMap();
        $gMap->zoom = 15;
        $gMap->setWidth('274px');
        $gMap->setHeight(274);

        $gMap->setOptions(array('mapTypeControl' => false, 'streetViewControl' => false));

        $dim = explode(',', $latlong);
        if (count($dim) === 2)
            $gMap->setCenter($dim[0], $dim[1]);

        $icon = new EGMapMarkerImage(Utils::imageUrl('marker.png'));

        $icon->setSize(40, 52);
        $icon->setAnchor(16, 20);
        $icon->setOrigin(0, 0);

        if (count($dim) === 2) {
            $marker = new EGMapMarker($dim[0], $dim[1], array('title' => '', 'icon' => $icon));
            $gMap->addMarker($marker);
        }

        return $gMap;
    }

}