<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'dealspot.gr',
    'preload' => array('log'),
    'language' => 'el',
    'sourceLanguage' => '00',
    'onBeginRequest' => array('Controller', 'preloadModules'),
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.extensions.PasswordHash',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '123',
            'ipFilters' => array('*'),
        ),
        'admin' => array(
            'autoinit' => true,
        ),
        'hybridauth' => array(
            'baseUrl' => 'http://localhost/dealspot/hybridauth',
            'withYiiUser' => false,
            "providers" => array(
//                "Google" => array(
//                    "enabled" => true,
//                    "keys" => array("id" => "406626435804.apps.googleusercontent.com", "secret" => "B9BNl9Ftl_BUN_eT_bv0z66c"),
//                    "scope" => ""
//                ),
                "Facebook" => array(
                    "enabled" => true,
                    "keys" => array("id" => "447665841933638", "secret" => "27f75a2e8046f319f1a9a86965b28200"),
                    "scope" => "",
                    "display" => "page"
                ),
            ),
        ),
        'payPal' => array(
            'autoinit' => true,
            'env' => '',
            'account' => array(
                'username' => 'dealspot_api1.dealspot.gr',
                'password' => 'CVP54Z4KZCLARE4W',
                'signature' => 'A1ztvUxHALvUPCV0-1oeWMXLcFbYAAFt.SnQux1yt-FqM19.Y0yeefrV',
                'email' => 'dealspot@dealspot.gr',
                'identityToken' => 'PX8XQCAYJZMQ2',
            ),
//            'env' => 'sandbox',
//            'account' => array(
//                'username' => 'nmorgo_1349428604_biz_api1.gmail.com',
//                'password' => '1349428631',
//                'signature' => 'AM0Qif0E1klKqMnQq2mA9i6uJ1YAAqvO2efwZ9THzKuqBY50nBnOiDdf',
//                'email' => 'nmorgo_1349428604_biz@gmail.com',
//                'identityToken' => 'olDlU73JMyfmaRnPcbBYrGIbG6APyeBY6Pcpzp18umLGNQVVkgnM3f5txnG',
//            ),
        ),
        'payment' => array(
            'autoinit' => true,
        ),
    ),
    'components' => array(
        'widgetFactory' => array(
            'widgets' => array(
                'CJuiDialog' => array(
                    'themeUrl' => '/css/jqueryui',
                    'theme' => 'dealspot',
                ),
                'CJuiDatePicker' => array(
                    'themeUrl' => '/css/jqueryui',
                    'theme' => 'dealspot',
                ),
                'CJuiTabs' => array(
                    'themeUrl' => '/css/jqueryui',
                    'theme' => 'dealspot',
                ),
            ),
        ),
        'messages' => array(
            'class' => 'CPhpMessageSource',
            'cachingDuration' => '3600',
        ),
        'cache' => array(
            'class' => 'CApcCache',
        ),
        'eventSystem' => array(
            'class' => 'EventSystem',
        ),
        'user' => array(
            'allowAutoLogin' => true,
        ),
        'session' => array(
            'cookieParams' => array(
                'httpOnly' => true,
            ),
        ),
        'wagclient' => array(
            'class' => 'ext.wagclient.WClient',
            'key' => '0NKFQZObfuR6j3ZKkCQ2k',
            'secret' => 'Rav7ySsADhC3UcwFHozpdYShI1UFpAkZFJS70QL5MXs',
            'url' => 'https://www.wrapthegift.com/api/',
        ),
        'ePdf' => array(
            'class' => 'ext.yii-pdf.EYiiPdf',
            'params' => array(
                'mpdf' => array(
                    'librarySourcePath' => 'application.vendors.mpdf.*',
                    'constants' => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.coupons'),
                    ),
                    'class' => 'mpdf', // the literal class filename to be loaded from the vendors folder
                /* 'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
                  'mode'              => '', //  This parameter specifies the mode of the new document.
                  'format'            => 'A4', // format A4, A5, ...
                  'default_font_size' => 0, // Sets the default document font size in points (pt)
                  'default_font'      => '', // Sets the default font-family for the new document.
                  'mgl'               => 15, // margin_left. Sets the page margins for the new document.
                  'mgr'               => 15, // margin_right
                  'mgt'               => 16, // margin_top
                  'mgb'               => 16, // margin_bottom
                  'mgh'               => 9, // margin_header
                  'mgf'               => 9, // margin_footer
                  'orientation'       => 'P', // landscape or portrait orientation
                  ) */
                ),
            ),
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
                'coming' => 'site/placeholder',
                '<module:\w+>/<action:\w+>' => '<module>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
            'showScriptName' => false,
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=dealspot',
            'emulatePrepare' => true,
            'username' => 'dealspot',
            'password' => 'BxGxJrUJJhvt76FF',
            'charset' => 'utf8',
//            'schemaCachingDuration' => 360000,
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                array(
                    'class' => 'LiveLogRoute',
                    'levels' => 'error, warning, info',
                    'maxFileSize' => '1024',
                    'logFile' => 'mobgen.log',
                    'categories' => 'mobgen',
                ),
                array(
                    'class' => 'LiveLogRoute',
                    'levels' => 'error, info',
                    'maxFileSize' => '1024',
                    'categories' => 'apns',
                    'logFile' => 'apns.log',
                ),
            ),
        ),
    ),
    'params' => array(
        'phpass' => array(
            'iteration_count_log2' => 8,
            'portable_hashes' => false,
        ),
        'emails' => array(
            'contact' => 'support@dealspot.gr',
            'companies' => 'sales@dealspot.gr',
        ),
    ),
);
