<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Dealspot',
    'language' => 'el',
    'sourceLanguage' => '00',
    'import' => array(
        'application.models.*',
    ),
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=dealspot',
            'emulatePrepare' => true,
            'username' => 'dealspot',
            'password' => 'BxGxJrUJJhvt76FF',
            'charset' => 'utf8',
//            'schemaCachingDuration' => 360000,
//            'enableProfiling' => true,
//            'enableParamLogging' => true,
        ),
        'request' => array(
            'hostInfo' => 'http://www.dealspot.gr/',
            'baseUrl' => '',
            'scriptUrl' => '',
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
                '<module:\w+>/<action:\w+>' => '<module>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
            'showScriptName' => false,
        ),
        'messages' => array(
            'class' => 'CPhpMessageSource',
            'cachingDuration' => '3600',
        ),
    ),
);
