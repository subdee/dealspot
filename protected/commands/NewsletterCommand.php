<?php

/**
 * @author kthermos
 */
class NewsletterCommand extends CConsoleCommand {

    protected $deals;

    public function actionSend() {
        $this->deals = Deal::getRandomDeals(8);

        $nusers = NewsletterUsers::model()->findAll();
        $users = User::model()->newsletter()->findAll();

        foreach ($nusers as $nuser)
            $this->_sendEmail($nuser);

        foreach ($users as $user)
            $this->_sendEmail($user);
    }

    private function _sendEmail($user) {
        $body = $this->renderFile(Yii::getPathOfAlias('application.views.site').'/_newsletter.php', array('deals' => $this->deals, 'email' => $user->email), true);
        Utils::sendEmail($user->email, _t('newsletter', 'newsletter email subject'), $body);
    }

}