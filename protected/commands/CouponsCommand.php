<?php

/**
 * @author kthermos
 */
class CouponsCommand extends CConsoleCommand {
    
    public function actionCleanup() {
        $deals = Deal::model()->findAll('NOW() > valid_to AND coupons_bought < min_required');
        foreach ($deals as $deal) {
            $transactions = $deal->transactions;
            foreach ($transactions as $transaction) {
                if ($transaction->payment_method == PaymentMethod::POINTS) {
                    $transaction->user->details->points += ($transaction->amount * 100);
                    $transaction->user->details->save(false);
                    
                    $pt = new PointTransaction;
                    $pt->user_id = $transaction->user->id;
                    $pt->points = $transaction->amount * 100;
                    $pt->reason = PointTransactionReason::REFUND;
                    $pt->transaction_date = date(DATE_ISO8601);
                    $pt->are_redeemable = true;
                    $pt->save();
                }
                
                $coupons = $transaction->coupons;
                foreach ($coupons as $coupon) {
                    $coupon->is_active = false;
                    $coupon->save();
                }
            }
        }
    }
    
}