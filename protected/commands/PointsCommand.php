<?php

/**
 * @author kthermos
 */
class PointsCommand extends CConsoleCommand {
    
    public function actionRedeemInvitePoints() {
        $transactions = PointTransaction::model()->disableDefaultScope()->findAll('reason = :reason', array(':reason' => PointTransactionReason::INVITATION));
        foreach ($transactions as $transaction) {
            $invitations = UserInvite::model()->findAllByAttributes(array(
                'user_id' => $transaction->user_id,
                'status' => InviteStatus::REGISTERED,
            ));
            foreach ($invitations as $invite) {
                $user = User::model()->findByAttributes(array('email' => $invite->invited_email));
                if ($user) {
                    if ($user->totalSales() > 20) {
                        $invite->sender->details->points += $transaction->points;
                        $invite->sender->details->save(false);
                        
                        $transaction->are_redeemable = true;
                        $transaction->save(false);
                        
                        $invite->status = InviteStatus::BOUGHT;
                        $invite->save(false);
                    }
                }
            }
        }
    }
    
    public function actionCancelExpiredPoints() {
        $transactions = PointTransaction::model()->findAll('points > 0 AND are_redeemable AND NOW() > transaction_date + INTERVAL 8 MONTH');
        foreach ($transactions as $transaction) {
            $transaction->are_redeemable = false;
            $transaction->save(false);
            
            $transaction->user->details->points -= $transaction->points;
            $transaction->user->details->save(false);
        }
    }
    
}