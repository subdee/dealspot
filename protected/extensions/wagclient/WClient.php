<?php

class WClient {

    public $key;
    public $secret;
    public $url = false;
    private $client;

    public function init() {
        Yii::import('ext.wagclient.client.wrapagift-client.*');
        $this->client = new WagClient($this->key, $this->secret, $this->url);
    }

    public function getPapers() {
        return $this->client->getPapers();
    }
    
    public function getTooltip() {
        return $this->client->getTooltip();
    }

    public function setGift($gift) {
        return new WagGift($gift);
    }

    public function sendGift(WagGift $gift) {
        $sent = $this->client->saveGift($gift);
        if (!$sent) {
            Yii::log(_app()->session['client-error'], CLogger::LEVEL_ERROR, 'wrapagift');
        } else {
            return $sent;
        }
        return false;
    }

    public function getStatus($gift) {
        return $this->client->getStatus($gift);
    }

}