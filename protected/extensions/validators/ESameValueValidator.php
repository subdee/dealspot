<?php

/**
 * Description of CPastValidator
 *
 * @author kthermos
 */
class ESameValueValidator extends CValidator {

    protected function validateAttribute($object, $attribute) {
        if ($attribute == 'password') {
            if ($object->password != $object->password2)
                $this->addError($object, 'password2', _t('user', 'passwords dont match'));
        }
        if ($attribute == 'email') {
            if ($object->email !== $object->email2)
                $this->addError($object, 'email2', _t('user', 'emails dont match'));
        }
    }

}