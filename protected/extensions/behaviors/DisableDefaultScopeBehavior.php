<?php
class DisableDefaultScopeBehavior extends CActiveRecordBehavior
{
    private $_defaultScopeDisabled = false;

    public function disableDefaultScope()
    {
          $this->_defaultScopeDisabled = true;
          return $this->Owner;
    }

    public function getDefaultScopeDisabled() {
        return $this->_defaultScopeDisabled;
    }

}
